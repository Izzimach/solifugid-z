import Lean
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM

open Lean Elab Command Term Meta PrettyPrinter

open FSM Basic

-- generate program graphs and convert into transition systems

structure ProgramTransition (Loc : Type) (Action : Type) (Var : Type) where
    (guard : Option (Var → Bool)) (action : Action) (effect : Var → Var) (next : Loc)

--
-- A Program Graph contains nodes and edges like a normal graph with some changes:
-- - each node is a program location (maybe a line number in some cases)
-- - The Program Graph has an associated state "s", which here is represented as a Lean
--   structure containing one fields for each variable.
-- - A node has 0 or more outgoing edges we'll call "transitions"
-- - Each transition has:
--   1. A guard (s → Bool) which determines if this transition can taken (enabled) given a specific state value.
--   2. An action which is sometimes used for synchromous concurrent modeling and/or as a debugging aid to
--      produce more human-readable errors.
--   3. A function to produce the new state.
--   4. A destination location.
-- - If a node has more than one enabled transition they are taken nondeterministically.
-- - A set of possible starting locations and state variables are also provided.
--
-- The Program Graph can be "unfolded" into a transition system where each possible value of (location × state)
-- is represented as a node. If the state has a high cardinality this could produce quite a large set of nodes!
-- Thus it's important to keep the size of the state down. In general the state contains Bool and (Fin n) for small
-- values of n.
structure ProgramGraph (Location : Type) (Action : Type) (StateVariables : Type) [Ord Location] where
    (transitions : RBMap Location (List (ProgramTransition Location Action StateVariables)) compare)
    (locationLabels : RBMap Location (List String) compare)
    (starts : List Location)


namespace ProgramGraph

-- StateCardinality is used to indicate how many possible states there are in the ProgramGraph,
-- since we need to track the overall cardinality of the unfolded ProgramGraph. It also
-- generates a default instance used when starting a program.
class StateCardinality (X : Type) where
    sSize : Nat
    genState : Fin sSize → X

instance : StateCardinality Unit where
    sSize := 1
    genState := fun _ => ()

instance : StateCardinality Bool where
    sSize := 2
    genState := fun b => if b.val == 0 then True else False

instance : StateCardinality (Fin n) where
    sSize := n
    genState := id

def allLocations [BEq Loc] [Ord Loc] (p : ProgramGraph Loc Act S) : List Loc :=
    let addIfMissing := fun m v => if m.contains v then m else m.cons v
    let transitionLocations :=
        p.transitions.fold
            (fun m v ts =>
                let m := addIfMissing m v
                ts.foldl (fun m t => addIfMissing m t.4) m)
            []
    let labelLocations := p.locationLabels.toList.map Prod.fst
    List.eraseDups (transitionLocations ++ labelLocations ++ p.starts)

def addTransition [Ord Loc] (p : ProgramGraph Loc Act S) (l₁ l₂ : Loc) (guard : S → Bool) (action : Act) (effect : S → S) : ProgramGraph Loc Act S :=
    let newTransition := ProgramTransition.mk guard action effect l₂
    let tr := match p.transitions.find? l₁ with
              | Option.none => p.transitions.insert l₁ [newTransition]
              | Option.some ts => p.transitions.insert l₁ (newTransition :: ts)
    { p with transitions := tr }

def addStart [Ord Loc] (p : ProgramGraph Loc Act S) (l : Loc) : ProgramGraph Loc Act S :=
    {p with starts := l :: p.starts}



def interleaveProgramGraphs [Ord Loc] [BEq S] [BEq Act] (pg₁ pg₂ : ProgramGraph Loc Act S) (syncActions : Option (Act → Bool)) : ProgramGraph (Basic.OProd Loc Loc) Act S :=
    let syncCheck : ProgramTransition Loc Act S → Bool :=
        fun pt => (Option.getD syncActions (λ_=>false)) pt.action
    -- have to combine all locations
    let tr₁ := pg₁.transitions.toList
    let tr₂ := pg₂.transitions.toList
    let tr := Basic.listProduct tr₁ tr₂ <|
        fun ⟨loc₁,es₁⟩ ⟨loc₂,es₂⟩ =>
            -- pull out sync/no sync edge elements
            let ⟨el₁, en₁⟩ := es₁.partition syncCheck
            let ⟨el₂, en₂⟩ := es₂.partition syncCheck
            -- "no sync" elements are just swizzled
            let en₁ := en₁.map (fun tr => ProgramTransition.mk tr.guard tr.action tr.effect (OProd.mk tr.next loc₂))
            let en₂ := en₂.map (fun tr => ProgramTransition.mk tr.guard tr.action tr.effect (OProd.mk loc₁ tr.next))            -- "should sync" elements are kept only if labels in the two lists match
            -- for sync edges, only keep those that match
            let matchingEdges : List (ProgramTransition (OProd Loc Loc) Act S) :=
                Basic.listProductFilter el₁ el₂ <|
                    fun tr₁ tr₂ =>
                        if tr₁.action == tr₂.action 
                        then let combinedGuard := match tr₁.guard, tr₂.guard with
                                                    | .none, .none => .none
                                                    | .some g, .none => .some g
                                                    | .none, .some g => .some g
                                                    | .some g₁, .some g₂ => .some (λs => g₁ s && g₂ s)
                             Option.some <| ProgramTransition.mk combinedGuard tr₁.action (λs => tr₁.effect (tr₂.effect s)) ⟨tr₁.next,tr₂.next⟩
                        else Option.none
            ⟨⟨loc₁,loc₂⟩, en₁ ++ en₂ ++ matchingEdges⟩
    let newLabels := Basic.listProductFilter tr₁ tr₂
        (fun ⟨loc₁,_⟩ ⟨loc₂,_⟩ => match Prod.mk (pg₁.locationLabels.find? loc₁) (pg₂.locationLabels.find? loc₂) with
            | ⟨.none,.none⟩ => .none
            | ⟨.some a,.none⟩ => .some ⟨⟨loc₁,loc₂⟩,a⟩
            | ⟨.none, .some b⟩ => .some ⟨⟨loc₁,loc₂⟩,b⟩
            | ⟨.some a, .some b⟩ => .some ⟨⟨loc₁,loc₂⟩,a ++ b⟩
            )
    let newStarts := Basic.listProduct pg₁.starts pg₂.starts OProd.mk
    ProgramGraph.mk (RBMap.fromList tr _) (RBMap.fromList newLabels _) newStarts


-- "Either" as an Action. InfoText is for human-readable text that has no functional use. Signal is an action
-- used for synchronizing concurrent processes.
inductive ActionLabel (SignalIndex : Type) where
| InfoText : String → ActionLabel SignalIndex
| Signal : SignalIndex → ActionLabel SignalIndex
    deriving Repr

instance : Inhabited (ActionLabel a) where
    default := .InfoText ""

instance [BEq SignalIndex] : BEq (ActionLabel SignalIndex) where
    beq
    | .InfoText a, .InfoText b => a == b
    | .Signal a, .Signal b => a == b
    | _,_ => false

instance [Ord SignalIndex] : Ord (ActionLabel SignalIndex) where
    compare
    | .InfoText a, .InfoText b => compare a b
    | .InfoText _, .Signal _ => Ordering.lt
    | .Signal a, .Signal b => compare a b
    | .Signal _, .InfoText _ => Ordering.gt

instance [ToString SignalIndex] : ToString (ActionLabel SignalIndex) where
    toString
    | .InfoText s => "\"" ++ s ++ "\""
    | .Signal si => "<<" ++ toString si ++ ">>"

instance [ToString SignalIndex] : GraphVizLabel (ActionLabel SignalIndex) where
    toGraphVizLabel
    | .InfoText s => "." ++ s ++ "."
    | .Signal si => "{{" ++ toString si ++ "}}"

def isSignal : ActionLabel a → Bool
    | .InfoText _ => false
    | .Signal _ => true

--
-- Simple program statements used when writing/building a program. This gets transformed into a Program Graph.
-- Note you probably don't want ot build a ProgramSequence directly, typically you would use macros (see below)
-- or code to generate a ProgramSequence.
--
inductive ProgramSequence (A : Type) (S : Type) where
    | Statement : (S → Bool) → A → (S → S) → ProgramSequence A S
    | Bind : ProgramSequence A S → ProgramSequence A S → ProgramSequence A S
    | DoUntil : ProgramSequence A S → A → (S → Bool) → ProgramSequence A S
    | Wait : A → (S → Bool) → (S → S) → ProgramSequence A S
    | IfThen : A → (S → Bool) → ProgramSequence A S → ProgramSequence A S
    | NondetBranch : List (ProgramSequence A S) → ProgramSequence A S
    | EndProgram : String → ProgramSequence A S
    | Label : String → ProgramSequence A S → ProgramSequence A S



structure ProgramBuilderState (Act S : Type) where
    (pGraph : ProgramGraph Nat Act S)
    (nextLoc : Nat)

def ProgramBuilder (A S : Type) (α : Type) : Type := StateT (ProgramBuilderState A S) Id α

instance : Monad (ProgramBuilder A S) where
    bind := StateT.bind
    pure := StateT.pure

instance : MonadStateOf (ProgramBuilderState A S) (ProgramBuilder A S) where
    get := StateT.get
    set := StateT.set
    modifyGet := StateT.modifyGet

def newLoc : ProgramBuilder Action S Nat := modifyGet <| fun s => ⟨s.nextLoc, {s with nextLoc := s.nextLoc + 1}⟩
    
def newTransition {S : Type} (loc₁ : Nat) (guard : S → Bool) (action : Action) (eff : S → S) (loc₂ : Nat) : ProgramBuilder Action S Unit := do
    modifyGet <| fun s =>
        let newGraph := addTransition s.pGraph loc₁ loc₂ guard action eff
        ⟨(), {s with pGraph := newGraph }⟩

def newLabel (loc : Nat) (label : String) : ProgramBuilder Action S Unit := do
    modifyGet <| fun s =>
        let newGraph := {s.pGraph with locationLabels := s.pGraph.locationLabels.insert loc [label] }
        ⟨(), {s with pGraph := newGraph}⟩


def compileSequenceAux [Inhabited Signal] (p : ProgramSequence (ActionLabel Signal) S) (startLoc : Option Nat) (stopLoc : Option Nat) : ProgramBuilder (ActionLabel Signal) S Unit := do
    let startLoc ← match startLoc with
                    | Option.some n => pure n
                    | Option.none => newLoc
    let stopLoc ← match stopLoc with
                  | Option.some n => pure n
                  | Option.none => newLoc
    match p with
    | .Statement guard action f => do
        newTransition startLoc guard action f stopLoc
    | .Bind s₁ s₂ => do
        let midLoc ← newLoc
        compileSequenceAux s₁ startLoc midLoc 
        compileSequenceAux s₂ midLoc stopLoc
    | .DoUntil s exitAction u => do
        let midLoc ← newLoc
        compileSequenceAux s (Option.some startLoc) (Option.some midLoc)
        newTransition midLoc (fun s => not (u s)) (.InfoText "doloop") id startLoc
        newTransition midLoc u exitAction id stopLoc
    | .Wait a u t => do
        newTransition startLoc u a t stopLoc
    | .IfThen a b t => do
        let thenLoc ← newLoc
        newTransition startLoc b a id thenLoc
        compileSequenceAux t (Option.some thenLoc) (Option.some stopLoc)
        newTransition startLoc (fun x => not (b x)) (.InfoText "else") id stopLoc
    | .NondetBranch ns =>
        match ns with
        | List.cons h tail => do
            compileSequenceAux h startLoc stopLoc
            compileSequenceAux (.NondetBranch tail) startLoc stopLoc
        | List.nil => pure ()
    | .EndProgram l => do
        let trapLoc ← newLoc
        newTransition startLoc (fun _ => true) (.InfoText "end") id trapLoc
        newTransition trapLoc (fun _ => true) (.InfoText "endloop") id trapLoc
        newLabel trapLoc l
    | .Label l s => do
        compileSequenceAux s startLoc stopLoc
        newLabel startLoc l
    termination_by compileSequenceAux p _ _ => p
  
-- Given a particular ProgramSequence this compiles it into a ProgramGraph that can then be
-- combined with other ProgramGraphs or unfolded into a transition system
def compileSequence [Inhabited Signal] [Inhabited S] : ProgramSequence (ActionLabel Signal) S → ProgramGraph Nat (ActionLabel Signal) S := fun p => 
    let pg0 := {transitions := RBMap.empty, locationLabels := RBMap.empty, starts := [0]}
    let x : Prod Unit (ProgramBuilderState (ActionLabel Signal) S) :=
        Id.run <| StateT.run (do let startLoc ← newLoc; compileSequenceAux p startLoc Option.none) { pGraph := pg0, nextLoc := 0}
    x.2.pGraph

-- modify the given ProgramGraph so that it starts at the location(s) indicates by the label(s)
def startProgramAt [Inhabited S] (ps : ProgramGraph Nat (ActionLabel Signal) S) (startLabels : List String) : ProgramGraph Nat (ActionLabel Signal) S :=
    let locList := ps.locationLabels.toList
    let findLabeledLocations : String → List Nat :=
        fun lbl => locList.filterMap (fun ⟨loc,lbls⟩ => if lbls.contains lbl then Option.some loc else Option.none)
    let labelLocs := List.join <| List.eraseDups <| startLabels.map findLabeledLocations
    {ps with starts := labelLocs}

--
-- simple DSL syntax to build ProgramSequences
--
-- we drop down into the elaborator so that we can embed pretty-printed text of the tests and
-- state updates into the graph
--

elab " <[ " x:term " ]> " : term => do
    let infoTxt ← Lean.Syntax.mkStrLit <$> toString <$> ppTerm x
    let fullSyntax ← `(@ProgramSequence.Statement (ActionLabel _) _ (fun _ => true) (.InfoText $infoTxt) ($x))
    elabTerm fullSyntax .none

--elab " LABEL " lbl:term "//" x:term : term => do
    --let fullSyntax ← `(@ProgramSequence.Label _ _ $lbl $x)
    --elabTerm fullSyntax .none

elab " IF? " b:term " THEN " th:term " ..IF " : term => do
    let ifTxt ← Lean.Syntax.mkStrLit <$> toString <$> ppTerm b
    let fullSyntax ← `(ProgramSequence.IfThen (.InfoText $ifTxt) $b $th)
    elabTerm fullSyntax .none

elab " WAIT " b:term " THEN " t:term " ..WAIT " : term => do
    let waitTxt ← Lean.Syntax.mkStrLit <$> toString <$> ppTerm b
    let fullSyntax ← `(ProgramSequence.Wait (.InfoText $waitTxt) $b $t)
    elabTerm fullSyntax .none

elab " DO " d:term " UNTIL " u:term "..DO " : term => do
    let untilTxt ← Lean.Syntax.mkStrLit <$> toString <$> ppTerm u
    let fullSyntax ← `(ProgramSequence.DoUntil ($d) (.InfoText $untilTxt) ($u))
    elabTerm fullSyntax .none

    
syntax term " //> " term : term
syntax " SYNC " term " ..SYNC " : term
syntax " END " term " ..END ": term
syntax " LABEL " term " // " term : term

macro_rules
| `($n //> $m) => `(ProgramSequence.Bind $n $m)
| `(SYNC $s ..SYNC) => `(@ProgramSequence.Statement (ActionLabel _) _ (fun _ => true) (.Signal $s) id)
| `(END $s ..END) => `(ProgramSequence.EndProgram $s)
| `(LABEL $l // $x) => `(ProgramSequence.Label $l $x)


--
-- "compile" a Program Graph into a Transition System/FSM without state variables.
--
def unfoldProgramGraph {Loc Act State VL EL : Type} [BEq State] [BEq Loc] [BEq EL] [Ord Loc] [Inhabited VL] [StateCardinality State]
    (pg : ProgramGraph Loc Act State)
    (nodeLabel : Loc → State → List String → VL)
    (edgeLabel : Act → EL)
    (startStates : List State)
    : FSM (Basic.OProd Loc State) VL EL :=
    let locations := pg.allLocations.toArray
    if hlocz : locations.size <= 0
    then FSM.mk LabeledGraph.empty [] []
    else if hstz : StateCardinality.sSize State <= 0
    then FSM.mk LabeledGraph.empty [] []
    else
    let vertexCount := locations.size * StateCardinality.sSize State
    let vertexFor := fun v =>
        let v₁ := v.val % locations.size
        let hlocz' : locations.size > 0 := by apply Nat.gt_of_not_le; exact hlocz
        let vh₁ : v₁ < locations.size := by apply Nat.mod_lt v.val hlocz'
        let v₂ := (v.val / locations.size) % StateCardinality.sSize State
        let hstz' : StateCardinality.sSize State > 0 := by apply Nat.gt_of_not_le; exact hstz
        let vh₂ : v₂ < StateCardinality.sSize State := by apply Nat.mod_lt (v.val/locations.size) hstz'
        ⟨(locations.get (Fin.mk v₁ vh₁)), StateCardinality.genState (Fin.mk v₂ vh₂)⟩
    let checkGuard := fun guard s => match guard with
                                     | Option.none => true
                                     | Option.some b => b s
    let edges := fun ⟨loc,st⟩ => match pg.transitions.find? loc with
        | Option.none => VertexInfo.mk (nodeLabel loc st (pg.locationLabels.findD loc [])) []
        | Option.some es =>
            let newLabel := nodeLabel loc st (pg.locationLabels.findD loc [])
            let edges := List.eraseDups <| es.filterMap (fun tr => if checkGuard tr.guard st then Option.some ⟨⟨tr.next,tr.effect st⟩,edgeLabel tr.action⟩ else Option.none)
            VertexInfo.mk newLabel edges
    let oprodStarts := Basic.listProduct pg.starts startStates OProd.mk
    FSM.mk (LabeledGraph.mk vertexCount vertexFor edges) oprodStarts []


def readableNodeLabel {Loc State : Type} [ToString Loc] [ToString State] : Loc → State → List String → String :=
    fun l st lbls =>
        let lblX := if lbls.length == 0
                    then ""
                    else String.intercalate "/" (lbls.map toString) ++ "_"
        lblX ++ "(" ++ toString l ++ "×" ++ toString st ++ ")"

--
-- test/check stuff
--

-- "run" the give programGraph, collecting states that satify a certain predicate
def findProgramStates  {Loc Act State : Type} [Ord Loc] [BEq Loc] [BEq State] [StateCardinality State]
    (pg : ProgramGraph Loc Act State)
    (startStates : List State)
    (collect? : Loc → State → List String → Bool)
    : List State :=
    let unfolded := unfoldProgramGraph pg collect? (fun _ => ()) startStates
    let reachable := List.eraseDups <| List.join <| unfolded.startStates.map (fun v => LabeledGraph.reachableVertices unfolded.toLabeledGraph v)
    List.eraseDups <|
        reachable.filterMap
            (fun vi => if (unfolded.edgesFor vi).1
                       then Option.some vi.2
                       else Option.none)

-- for findProgramStates; matches program locations that contain a specific label
def hasLabel {Loc State : Type} (s : String) : Loc → State → List String → Bool :=
    fun _ _ labels => labels.contains s

def hasAllLabels {Loc State : Type} (ss : List String) : Loc → State → List String → Bool :=
    fun _ _ labels => ss.all labels.contains

def hasAnyLabel {Loc State : Type} (ss : List String) : Loc → State → List String → Bool :=
    fun _ _ labels => ss.any labels.contains







def vizLocations {Loc Action S : Type} [BEq Loc] [Ord Loc] [ToString Loc] (pg : ProgramGraph Loc Action S) : String :=
    let isStart : Loc → Bool := fun l => pg.starts.contains l
    pg.transitions.fold
        (fun acc loc _ =>
            let label := match pg.locationLabels.find? loc with
                         | Option.none => []
                         | Option.some lbls => ["label=\"" ++ String.intercalate "/" lbls ++ "\""]
            let shape := if isStart loc then ["shape=square"] else ["shape=circle"]
            let nodeAttributes : List String := label ++ shape
            acc ++ " " ++ toString loc ++ " [" ++ String.intercalate "," nodeAttributes ++ "];")
        ""

open FSM.GraphVizLabel in
def vizTransitions {Loc Action S : Type} [Ord Loc] [ToString Loc] [GraphVizLabel Action] (pg : ProgramGraph Loc Action S) : String :=
    String.join <|
        pg.transitions.fold
            (fun acc loc ts => acc ++ ts.map
                (fun tr => 
                    let label := if String.length (toGraphVizLabel tr.action) == 0 then "" else " [label=\"" ++ toGraphVizLabel tr.action ++ "\"]"
                    " " ++ toString loc ++ " -> " ++ toString tr.next ++ label))
            []

def vizProgramGraph {Loc Action S : Type} [BEq Loc] [Ord Loc] [ToString Loc] [GraphVizLabel Action] (pg : ProgramGraph Loc Action S) : String  :=
    "digraph {"
      ++
      vizLocations pg
      ++
      vizTransitions pg
      ++
      "}"


end ProgramGraph
