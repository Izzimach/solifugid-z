
-- simple useful utility functions

import Lean

namespace Basic

-- for graph combining we need a product and sum type that implement Ord and BEq
structure OProd (a : Type) (b : Type) := (v1 : a) (v2 : b)
    deriving Repr

instance [Ord a] [Ord b] : Ord (OProd a b) where
    compare := fun (OProd.mk x₁ x₂) (OProd.mk y₁ y₂) =>
                   match compare x₁ y₁ with
                   | .eq => compare x₂ y₂
                   | .gt => .gt
                   | .lt => .lt

instance [BEq a] [BEq b] : BEq (OProd a b) where
    beq := fun (OProd.mk x₁ x₂) (OProd.mk y₁ y₂) => x₁ == y₁ && x₂ == y₂

instance [ToString a] [ToString b] : ToString (OProd a b) where
   toString := fun (OProd.mk a b) => "(" ++ toString a ++ "," ++ toString b ++ ")"


inductive OSum (a : Type) (b : Type) where
    | Left : a → OSum a b
    | Right : b → OSum a b
    deriving Repr

instance [Ord a] [Ord b] : Ord (OSum a b) where
    compare := fun x y => match x,y with
                          | .Left _,  .Right _ => .lt
                          | .Left a,  .Left b  => compare a b
                          | .Right _, .Left _  => .gt
                          | .Right a, .Right b => compare a b

instance [BEq a] [BEq b] : BEq (OSum a b) where
    beq := fun x y => match x,y with
                      | .Left a, .Left b => a == b
                      | .Right a, .Right b => a == b
                      | _, _ => false

instance [ToString a] [ToString b] : ToString (OSum a b) where
   toString := fun x => match x with
                        | .Left a => "Left " ++ toString a
                        | .Right b => "Right " ++ toString b                          


-- orderable Option
inductive OOption (a : Type) where
    | None : OOption a
    | Some : a → OOption a
    deriving Repr


instance [BEq a] : BEq (OOption a) where
    beq := fun x y => match x,y with
                      | .None, .None => true
                      | .Some a, .Some b => a == b
                      | .Some _, .None => false
                      | .None, .Some _ => false

instance [Ord a] : Ord (OOption a) where
    compare := fun x y => match x,y with
                          | .None, .None => .eq
                          | .Some a, .Some b => compare a b
                          -- we'll have none be greater than some
                          | .None, .Some _ => .gt
                          | .Some _, .None => .lt

instance : Inhabited (OOption a) where
    default := .None

instance [ToString a] : ToString (OOption a) where
    toString := fun x => match x with
                         | .None => "OOption.none"
                         | .Some a => "OOption.some " ++ toString a

namespace OOption

def getD (v : OOption a) (defaultVal : a) : a :=
    match v with
    | .None => defaultVal
    | .Some x => x 

end OOption

def listProduct {D₁ D₂ X : Type} (l₁ : List D₁) (l₂ : List D₂) (f : D₁ → D₂ → X) : List X :=
   List.join <| l₁.map (fun d₁ => l₂.map (fun d₂ => f d₁ d₂))
   
def listProductFilter {D₁ D₂ X : Type} (l₁ : List D₁) (l₂ : List D₂) (f : D₁ → D₂ → Option X) : List X :=
   List.join <| l₁.map (fun d₁ => l₂.filterMap (fun d₂ => f d₁ d₂))
   

def listRangeFinAux (n : Nat) (nz : n > 0) (sub : Nat) : List (Fin n) :=
    if sz : sub = 0
    then List.nil
    else
        let sgz : sub > 0 := Nat.zero_lt_of_ne_zero sz
        (Fin.mk (n - sub) (by apply @Nat.sub_lt n sub nz sgz)) :: listRangeFinAux n nz (Nat.pred sub)

def listRangeFin (n : Nat) : List (Fin n) :=
    if nz : n = 0
    then List.nil
    else listRangeFinAux n (by apply Nat.zero_lt_of_ne_zero nz) n

def foldFinAux {A : Type u} {n : Nat} (f : A → Fin n → A) (acc : A) (ix : Fin n): A :=
    if ixz : ix.val = 0
    then f acc ix
    else
        let pred := ix.val - 1
        let predix : pred < ix.val := Nat.pred_lt ixz
        let predLt : pred < n := by apply Nat.lt_trans predix _
                                    apply ix.isLt
        f (foldFinAux f acc ⟨pred,predLt⟩) ix
    termination_by foldFinAux _ _ ix => ix.val
    
-- fold over a range of nats bounded by (Fin n). counts down from (n-1) to 0. Includes 0
def foldFin {A : Type u} (n : Nat) (f : A → Fin n → A) (accumulator : A) : A :=
    if nz : n = 0
    then accumulator
    else
        foldFinAux f accumulator ⟨n-1,Nat.pred_lt nz⟩


def foldFinMAux {A : Type u} {n : Nat} [Monad m] (f : A → Fin n → m A) (acc : A) (ix : Fin n): m A :=
    if ixz : ix.val = 0
    then f acc ix
    else do
        let pred := ix.val - 1
        let predix : pred < ix.val := Nat.pred_lt ixz
        let predLt : pred < n := by apply Nat.lt_trans predix _
                                    apply ix.isLt
        let tail ← foldFinMAux f acc ⟨pred,predLt⟩
        f tail ix
    termination_by foldFinMAux _ _ ix => ix.val

-- monadic fold over a range of nats bounded by n. counts down from (n-1) to 0. Includes 0
def foldFinM {A : Type u} [Monad m] (n : Nat) (f : A → Fin n → m A) (accumulator : A) : m A :=
    if nz : n = 0
    then pure accumulator
    else
        foldFinMAux f accumulator ⟨n-1,Nat.pred_lt nz⟩

end Basic
