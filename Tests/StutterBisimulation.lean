import Lean
import LSpec

import SolifugidZ.LabeledGraph
import SolifugidZ.GraphUtilities
import SolifugidZ.Bisimulation
import SolifugidZ.StutterBisimulation
import SolifugidZ.CTL

import Examples.TrafficLight
import Examples.CriticalSection

import SolifugidZ.VizGraph

open Lean Widget
open LSpec
open LabeledGraph GraphUtilities Bisimulation StutterBisimulation CTL

def trafficLightCycleBP := bisimulationPartition trafficLightCycle.toLabeledGraph compare
def trafficLightCycleBQ := FSM.mk (quotientByPartition trafficLightCycle.toLabeledGraph trafficLightCycleBP true) [0] []

def tlViz {aps : List String} (g : FSM Nat (APBits aps) x) :=
    VizGraph.toVizFSM
        g
        (fun a => String.intercalate "/" (List.map toString (@decodeAPBits String _ _ a)))
        (fun _ => "")
        "start"
        "end"
        []

/-
def nonCycles :=
  FSM.mk
    (Bisimulation.quotientByPartition trafficLightCycle.toLabeledGraph (findStutterCycles trafficLightCycle.toLabeledGraph compare) false)
    [0]
    []

def divergenceG :=
    FSM.mk
        (LabeledGraph.mapLabels (LabeledGraph.compactGraph <| addDivergenceNode nonCycles.toLabeledGraph) (fun l => Basic.OOption.getD l default))
        [5]
        []

--#eval StutterBisimulation.stutterPartition trafficLightCycle.toLabeledGraph compare
--#widget VizGraph.vizGraph (toJson (tlViz trafficLightCycle))
--#widget VizGraph.vizGraph (toJson (tlViz nonCycles))
--#widget VizGraph.vizGraph (toJson (tlViz divergenceG))
-/

def trafficLightStutterQuotient :=
    FSM.mk
        (stutterBisimulationDivQuotient trafficLightCycle.toLabeledGraph compare)
        [0]
        []

--#widget VizGraph.vizGraph (toJson (tlViz trafficLightStutterQuotient))

-- critical section system with only the "crit1" and "crit2" labels retained.
def csSystem := criticalSectionSystem.mapLabels (fun bits => bits &&& (@listToAPBits _ CriticalSectionAPList _ ["crit1","crit2"]))

-- a stutter-bisimulation quotient of `csSystem` should only have three states, none self-looping
def csSBSQuotient :=
    FSM.mk
        (stutterBisimulationDivQuotient csSystem.toLabeledGraph compare)
        [0]
        []
  
--#widget VizGraph.vizGraph (toJson (tlViz csSystem))
--#widget VizGraph.vizGraph (toJson (tlViz csSBSQuotient))

--
--
-- An important property of stutter-bisimulation divergence quotienting is that
-- a CTL expression (as long as it doesn't use ◯) gives equivalent satisfiability on both
-- the original system and it's quotient.  So we check that.
--
--

-- these CTL expressions should be true for both the cycle traffic light and it's stutter-bisimulation quotient
def redToYellow := [ctl ∀□(<+"red"+> → ∀◇<+"yellow"+>)]
def yellowLoop := [ctl ∀◇∃◇<+"yellow"+>]

--#eval checkUsingCTL trafficLightCycle yellowLoop

-- the critical section system model always eventually reaches crit1 or crit2. This should
-- be true for both `csSystem` and the stutter quotient of it `csSBSQuotient`
def eventuallyCrit := [ctl ∀(◇<+"crit1"+>∨<+"crit2"+>)]


def testStutterBisimulation : TestSeq :=
    group "Stutter bisimulation" 
        <| test "cycle traffic light always eventually move from red to yellow" (checkUsingCTL trafficLightCycle redToYellow)
        <| test "cycle traffic light can always reach a yellow loop" (checkUsingCTL trafficLightCycle yellowLoop)
        <| test "stutter quotient traffic light has 3 states" (trafficLightStutterQuotient.toLabeledGraph.vertexCount == 3)
        <| test "stutter quotient traffic light always eventually move from red to tellow" (checkUsingCTL trafficLightStutterQuotient redToYellow)
        <| test "stutter quotient traffic light can always reach a yellow loop" (checkUsingCTL trafficLightCycle yellowLoop)
        <| test "critical section system with only crit1/crit2 always eventually reaches crit1 or crit2" (checkUsingCTL csSystem eventuallyCrit)
        <| test "stutter quotient critical system always evenutally reaches crit1 or crit2" (checkUsingCTL csSBSQuotient eventuallyCrit)

--#lspec testStutterBisimulation

def main :=
    lspecIO testStutterBisimulation

