# solifugid-z

Data structures and algorithms of finite state machines and transition systems used for model-checking.

This is basically an implementation of most topics in the first six chapters of the textbook
"Principles of Model Checking" by Christel Baier and Joost-Pieter Katoen, third edition.

![Screenshot of transition system visualized in Lean preview pane](VizExample.png)

Includes
--------

 - Directed graphs for representing transition systems and other things:
     * Finite State Machines (FSM)
     * Nondeterministic Finite Automata (NFA)
     * Nondeterministic Büchi Automata (NBA)
 - A *ProgramGraph* instance for building simple stateful programs and unfolding them into transition systems.
 - Using NFAs and NBAs for checking finite and infinite words
 - Depth-first Search (DFS) and nested DFS algorithms which are used to check transition systems with an NFA or NBA.
 - Simple DSLs for:
    * Generating regular expressions and ω-regular expression (to convert into an NFA or NBA)
    * A pseudo-script language for generating simple program graphs with state.
    * Writing LTL expressions and converting them into GNBA/NBAs
    * Writing CTL expressions for use with a CTL checker
 - Checking a transition system with LTL by using a Büchi automata
 - Checking a transition system with CTL using satisfiability sets (including fairness)
 - (experimental) Bisimulation and Stutter-Bisimulation quotienting
 - (experimental) Visualization of graphs in Lean infoview

