import Lean
import LSpec

import SolifugidZ.LabeledGraph
import SolifugidZ.GraphUtilities

import Examples.TrafficLight
import Examples.CriticalSection
import Examples.PetersonExclusion

open LSpec
open LabeledGraph GraphUtilities

def testG :LabeledGraph Nat (List String) (Option String) :=
  fromExplicit <|
      compileGraph
          []
          [⟨0,1,.none⟩,⟨1,2,.none⟩,⟨2,0,.none⟩,⟨2,3,.none⟩,⟨3,4,.none⟩, ⟨4,5,.none⟩,⟨5,5,.none⟩]

-- testG with all edges from node 3 removed
def testGNo3 := filterEdges testG (fun v₁ e v₂ => v₁ != 3)

-- checks if a given graph is one single Strongly Connected Component
def isGraphStronglyConnected [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) : Bool :=
  if h : 0 < g.vertexCount
  then
      let scc := findSCCAt g (g.vertexFor ⟨0,h⟩)
      -- there is one SCC and it has all the vertices of the original FSM
      scc.length == 1 && (List.length scc.join) == g.vertexCount
  else
      False

def isFSMStronglyConnected [BEq VI] [Ord VI] (f : FSM VI VL EL) : Bool :=
  isGraphStronglyConnected f.toLabeledGraph

def testGraphUtilities : TestSeq :=
    group "Graph utilities" 
        <| test "graph testG has four SCCs" (List.length (findSCCAt testG 0) == 4)
        <| test "graph testG has two nontrivial SCCs" (List.length (findNontrivialSCCAt testG 0) == 2)
        <| test "can find node 5 from node 0 in test graph" (Option.isSome <| findPath testG 0 [5] )
        <| test "can't find node 5 from node 0 in filtered graph (removed edges from 3)" (Option.isNone <| findPath testGNo3 0 [5] )
        <| test "can't find node 0 from node 5 in test graph" (Option.isNone <| findPath testG 5 [0] )
        <| test "traffic light is strongly connected" (isFSMStronglyConnected trafficLightRedGreenYellow)
        <| test "peterson system is strongly connected" (isFSMStronglyConnected petersonSystem)

--#lspec testGraphUtilities

def main :=
    lspecIO testGraphUtilities

