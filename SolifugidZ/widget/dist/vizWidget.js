const global = window

import * as React from 'react';
import React__default, { useState } from 'react';

const useLatest = current => {
  const storedValue = React.useRef(current);
  React.useEffect(() => {
    storedValue.current = current;
  });
  return storedValue;
};

const useDebounceCallback = (callback, wait = 100, leading = false) => {
  const storedCallback = useLatest(callback);
  const timeout = React.useRef();
  const deps = [wait, leading, storedCallback]; // Cleans up pending timeouts when the deps change

  function _ref() {
    timeout.current && clearTimeout(timeout.current);
    timeout.current = void 0;
  }

  React.useEffect(() => _ref, deps);

  function _ref2() {
    timeout.current = void 0;
  }

  return React.useCallback(function () {
    // eslint-disable-next-line prefer-rest-params
    const args = arguments;
    const {
      current
    } = timeout; // Calls on leading edge

    if (current === void 0 && leading) {
      timeout.current = setTimeout(_ref2, wait); // eslint-disable-next-line prefer-spread

      return storedCallback.current.apply(null, args);
    } // Clear the timeout every call and start waiting again


    current && clearTimeout(current); // Waits for `wait` before invoking the callback

    timeout.current = setTimeout(() => {
      timeout.current = void 0;
      storedCallback.current.apply(null, args);
    }, wait);
  }, deps);
};
const useDebounce = (initialState, wait, leading) => {
  const state = React.useState(initialState);
  return [state[0], useDebounceCallback(state[1], wait, leading)];
};

function useEvent(target, type, listener, cleanup) {
  const storedListener = React.useRef(listener);
  const storedCleanup = React.useRef(cleanup);
  React.useEffect(() => {
    storedListener.current = listener;
    storedCleanup.current = cleanup;
  });
  React.useEffect(() => {
    const targetEl = target && 'current' in target ? target.current : target;
    if (!targetEl) return;
    let didUnsubscribe = 0;

    function listener(...args) {
      if (didUnsubscribe) return;
      storedListener.current.apply(this, args);
    }

    targetEl.addEventListener(type, listener);
    const cleanup = storedCleanup.current;
    return () => {
      didUnsubscribe = 1;
      targetEl.removeEventListener(type, listener);
      cleanup && cleanup();
    }; // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [target, type]);
}

const emptyObj = {};
const win = window;
const wv = win && typeof win.visualViewport !== 'undefined' ? win.visualViewport : null;

const getSize = () => [document.documentElement.clientWidth, document.documentElement.clientHeight];

const useWindowSize = function (options) {
  if (options === void 0) {
    options = emptyObj;
  }

  const {
    wait,
    leading,
    initialWidth = 0,
    initialHeight = 0
  } = options;
  const [size, setDebouncedSize] = useDebounce(
  /* istanbul ignore next */
  typeof document === 'undefined' ? [initialWidth, initialHeight] : getSize, wait, leading);

  const setSize = () => setDebouncedSize(getSize);

  useEvent(win, 'resize', setSize); // @ts-expect-error

  useEvent(wv, 'resize', setSize);
  useEvent(win, 'orientationchange', setSize);
  return size;
};

var Vector2 = (function () {
    function Vector2(values) {
        this._values = new Float32Array(2);
        if (values) {
            this.xy = values;
        }
    }
    Object.defineProperty(Vector2, "zero", {
        /**
         * Retrieves a new instance of the vector (0, 0)
         * @returns {Vector2} The zero vector
         */
        get: function () {
            return new Vector2([0, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "x", {
        /**
         * @returns {number} The x-component of the vector
         */
        get: function () {
            return this._values[0];
        },
        /**
         * @param {number} value The new x-component of the vector
         */
        set: function (value) {
            this._values[0] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "y", {
        /**
         * @returns {number} The y-component of the vectory
         */
        get: function () {
            return this._values[1];
        },
        /**
         * @param {number} value The new y-component of the vector
         */
        set: function (value) {
            this._values[1] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector2.prototype, "xy", {
        /**
         * @returns {number[]} An array containing the x-component and y-component of the vector
         */
        get: function () {
            return [this._values[0], this._values[1]];
        },
        /**
         * @param {number[]} values An array containing the new x-component and y-component of the vector
         */
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
        },
        enumerable: true,
        configurable: true
    });
    Vector2.cross = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        var x = vector.x;
        var y = vector.y;
        var x2 = vector2.x;
        var y2 = vector2.y;
        var z = x * y2 - y * x2;
        dest.xyz = [0, 0, z];
        return dest;
    };
    /**
     * Calculates the dot product of two vectors
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @returns {number} The dot product of the two vectors
     */
    Vector2.dot = function (vector, vector2) {
        return vector.x * vector2.x + vector.y * vector2.y;
    };
    /**
     * Calculates the distance between two vectors
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @returns {number} The distance between the two vectors
     */
    Vector2.distance = function (vector, vector2) {
        return Math.sqrt(this.squaredDistance(vector, vector2));
    };
    /**
     * Calculates the distance between two vectors squared
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @returns {number} The distance between the two vectors
     */
    Vector2.squaredDistance = function (vector, vector2) {
        var x = vector2.x - vector.x;
        var y = vector2.y - vector.y;
        return x * x + y * y;
    };
    /**
     * Calculates a normalized vector representing the direction from one vector to another.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.direction = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector2();
        var x = vector.x - vector2.x;
        var y = vector.y - vector2.y;
        var length = Math.sqrt(x * x + y * y);
        if (length === 0) {
            dest.reset();
            return dest;
        }
        length = 1.0 / length;
        dest.x = x * length;
        dest.y = y * length;
        return dest;
    };
    /**
     * Performs a linear interpolation over two vectors.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} a
     * @param {Vector2} b
     * @param {number} t
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.lerp = function (a, b, t, dest) {
        if (!dest)
            dest = new Vector2();
        dest.x = a.x + t * (b.x - a.x);
        dest.y = a.y + t * (b.y - a.y);
        return dest;
    };
    /**
     * Adds two vectors.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.sum = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector2();
        dest.x = vector.x + vector2.x;
        dest.y = vector.y + vector2.y;
        return dest;
    };
    /**
     * Subtracts two vectors.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.difference = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector2();
        dest.x = vector.x - vector2.x;
        dest.y = vector.y - vector2.y;
        return dest;
    };
    /**
     * Multiplies two vectors piecewise.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.product = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector2();
        dest.x = vector.x * vector2.x;
        dest.y = vector.y * vector2.y;
        return dest;
    };
    /**
     * Divides two vectors piecewise.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} vector
     * @param {Vector2} vector2
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.quotient = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector2();
        dest.x = vector.x / vector2.x;
        dest.y = vector.y / vector2.y;
        return dest;
    };
    /**
     * Retrieves the x-component or y-component of the vector.
     * @param {number} index
     * @returns {number}
     */
    Vector2.prototype.at = function (index) {
        return this._values[index];
    };
    /**
     * Sets both the x- and y-components of the vector to 0.
     */
    Vector2.prototype.reset = function () {
        this.x = 0;
        this.y = 0;
    };
    /**
     * Copies the x- and y-components from one vector to another.
     * If no dest vector is specified, a new vector is instantiated.
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.copy = function (dest) {
        if (!dest)
            dest = new Vector2();
        dest.xy = this.xy;
        return dest;
    };
    /**
     * Multiplies both the x- and y-components of a vector by -1.
     * If no dest vector is specified, the operation is performed in-place.
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.negate = function (dest) {
        if (!dest)
            dest = this;
        dest.x = -this.x;
        dest.y = -this.y;
        return dest;
    };
    /**
     * Checks if two vectors are equal, using a threshold to avoid floating-point precision errors.
     * @param {Vector2} other
     * @param {number} threshold
     * @returns {boolean}
     */
    Vector2.prototype.equals = function (other, threshold) {
        if (threshold === void 0) { threshold = EPSILON; }
        if (Math.abs(this.x - other.x) > threshold) {
            return false;
        }
        if (Math.abs(this.y - other.y) > threshold) {
            return false;
        }
        return true;
    };
    /**
     * Returns the distance from the vector to the origin.
     * @returns {number}
     */
    Vector2.prototype.length = function () {
        return Math.sqrt(this.squaredLength());
    };
    /**
     * Returns the distance from the vector to the origin, squared.
     * @returns {number}
     */
    Vector2.prototype.squaredLength = function () {
        var x = this.x;
        var y = this.y;
        return x * x + y * y;
    };
    /**
     * Adds two vectors together.
     * If no dest vector is specified, the operation is performed in-place.
     * @param {Vector2} vector
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.add = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x + vector.x;
        dest.y = this.y + vector.y;
        return dest;
    };
    /**
     * Subtracts one vector from another.
     * If no dest vector is specified, the operation is performed in-place.
     * @param {Vector2} vector
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.subtract = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x - vector.x;
        dest.y = this.y - vector.y;
        return dest;
    };
    /**
     * Multiplies two vectors together piecewise.
     * If no dest vector is specified, the operation is performed in-place.
     * @param {Vector2} vector
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.multiply = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x * vector.x;
        dest.y = this.y * vector.y;
        return dest;
    };
    /**
     * Divides two vectors piecewise.
     * @param {Vector2} vector
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.divide = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x / vector.x;
        dest.y = this.y / vector.y;
        return dest;
    };
    /**
     * Scales a vector by a scalar parameter.
     * If no dest vector is specified, the operation is performed in-place.
     * @param {number} value
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.scale = function (value, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x * value;
        dest.y = this.y * value;
        return dest;
    };
    /**
     * Normalizes a vector.
     * If no dest vector is specified, the operation is performed in-place.
     * @param {Vector2} dest
     * @returns {Vector2}
     */
    Vector2.prototype.normalize = function (dest) {
        if (!dest)
            dest = this;
        dest.xy = this.xy;
        var length = dest.length();
        if (length === 1) {
            return dest;
        }
        if (length === 0) {
            dest.reset();
            return dest;
        }
        length = 1.0 / length;
        dest.x *= length;
        dest.y *= length;
        return dest;
    };
    Vector2.prototype.toString = function () {
        return '(' + this.x + ', ' + this.y + ')';
    };
    return Vector2;
}());

var Vector3 = (function () {
    function Vector3(values) {
        this._values = new Float32Array(3);
        if (values) {
            this.xyz = values;
        }
    }
    Object.defineProperty(Vector3, "zero", {
        get: function () {
            return new Vector3([0, 0, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "one", {
        get: function () {
            return new Vector3([1, 1, 1]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "up", {
        get: function () {
            return new Vector3([0, 1, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "down", {
        get: function () {
            return new Vector3([0, -1, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "right", {
        get: function () {
            return new Vector3([1, 0, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "left", {
        get: function () {
            return new Vector3([-1, 0, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "forward", {
        get: function () {
            return new Vector3([0, 0, 1]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3, "backward", {
        get: function () {
            return new Vector3([0, 0, -1]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3.prototype, "x", {
        get: function () {
            return this._values[0];
        },
        set: function (value) {
            this._values[0] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3.prototype, "y", {
        get: function () {
            return this._values[1];
        },
        set: function (value) {
            this._values[1] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3.prototype, "z", {
        get: function () {
            return this._values[2];
        },
        set: function (value) {
            this._values[2] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3.prototype, "xy", {
        get: function () {
            return [this._values[0], this._values[1]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector3.prototype, "xyz", {
        get: function () {
            return [this._values[0], this._values[1], this._values[2]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
            this._values[2] = values[2];
        },
        enumerable: true,
        configurable: true
    });
    Vector3.cross = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        var x = vector.x;
        var y = vector.y;
        var z = vector.z;
        var x2 = vector2.x;
        var y2 = vector2.y;
        var z2 = vector2.z;
        dest.x = y * z2 - z * y2;
        dest.y = z * x2 - x * z2;
        dest.z = x * y2 - y * x2;
        return dest;
    };
    Vector3.dot = function (vector, vector2) {
        var x = vector.x;
        var y = vector.y;
        var z = vector.z;
        var x2 = vector2.x;
        var y2 = vector2.y;
        var z2 = vector2.z;
        return x * x2 + y * y2 + z * z2;
    };
    Vector3.distance = function (vector, vector2) {
        return Math.sqrt(this.squaredDistance(vector, vector2));
    };
    Vector3.squaredDistance = function (vector, vector2) {
        var x = vector2.x - vector.x;
        var y = vector2.y - vector.y;
        var z = vector2.z - vector.z;
        return x * x + y * y + z * z;
    };
    Vector3.direction = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        var x = vector.x - vector2.x;
        var y = vector.y - vector2.y;
        var z = vector.z - vector2.z;
        var length = Math.sqrt(x * x + y * y + z * z);
        if (length === 0) {
            dest.x = 0;
            dest.y = 0;
            dest.z = 0;
            return dest;
        }
        length = 1 / length;
        dest.x = x * length;
        dest.y = y * length;
        dest.z = z * length;
        return dest;
    };
    Vector3.lerp = function (a, b, t, dest) {
        if (!dest)
            dest = new Vector3();
        dest.x = a.x + t * (b.x - a.x);
        dest.y = a.y + t * (b.y - a.y);
        dest.z = a.z + t * (b.z - a.z);
        return dest;
    };
    Vector3.sum = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        dest.x = vector.x + vector2.x;
        dest.y = vector.y + vector2.y;
        dest.z = vector.z + vector2.z;
        return dest;
    };
    Vector3.difference = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        dest.x = vector.x - vector2.x;
        dest.y = vector.y - vector2.y;
        dest.z = vector.z - vector2.z;
        return dest;
    };
    Vector3.product = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        dest.x = vector.x * vector2.x;
        dest.y = vector.y * vector2.y;
        dest.z = vector.z * vector2.z;
        return dest;
    };
    Vector3.quotient = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector3();
        dest.x = vector.x / vector2.x;
        dest.y = vector.y / vector2.y;
        dest.z = vector.z / vector2.z;
        return dest;
    };
    Vector3.prototype.at = function (index) {
        return this._values[index];
    };
    Vector3.prototype.reset = function () {
        this.xyz = [0, 0, 0];
    };
    Vector3.prototype.copy = function (dest) {
        if (!dest)
            dest = new Vector3();
        dest.xyz = this.xyz;
        return dest;
    };
    Vector3.prototype.negate = function (dest) {
        if (!dest)
            dest = this;
        dest.x = -this.x;
        dest.y = -this.y;
        dest.z = -this.z;
        return dest;
    };
    Vector3.prototype.equals = function (other, threshold) {
        if (threshold === void 0) { threshold = EPSILON; }
        if (Math.abs(this.x - other.x) > threshold) {
            return false;
        }
        if (Math.abs(this.y - other.y) > threshold) {
            return false;
        }
        if (Math.abs(this.z - other.z) > threshold) {
            return false;
        }
        return true;
    };
    Vector3.prototype.length = function () {
        return Math.sqrt(this.squaredLength());
    };
    Vector3.prototype.squaredLength = function () {
        var x = this.x;
        var y = this.y;
        var z = this.z;
        return x * x + y * y + z * z;
    };
    Vector3.prototype.add = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x + vector.x;
        dest.y = this.y + vector.y;
        dest.z = this.z + vector.z;
        return dest;
    };
    Vector3.prototype.subtract = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x - vector.x;
        dest.y = this.y - vector.y;
        dest.z = this.z - vector.z;
        return dest;
    };
    Vector3.prototype.multiply = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x * vector.x;
        dest.y = this.y * vector.y;
        dest.z = this.z * vector.z;
        return dest;
    };
    Vector3.prototype.divide = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x / vector.x;
        dest.y = this.y / vector.y;
        dest.z = this.z / vector.z;
        return dest;
    };
    Vector3.prototype.scale = function (value, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x * value;
        dest.y = this.y * value;
        dest.z = this.z * value;
        return dest;
    };
    Vector3.prototype.normalize = function (dest) {
        if (!dest)
            dest = this;
        dest.xyz = this.xyz;
        var length = dest.length();
        if (length === 1) {
            return dest;
        }
        if (length === 0) {
            dest.reset();
            return dest;
        }
        length = 1.0 / length;
        dest.x *= length;
        dest.y *= length;
        dest.z *= length;
        return dest;
    };
    return Vector3;
}());

((function () {
    function Vector4(values) {
        this._values = new Float32Array(4);
        if (values) {
            this.xyzw = values;
        }
    }
    Object.defineProperty(Vector4, "zero", {
        get: function () {
            return new Vector4([0, 0, 0, 0]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4, "one", {
        get: function () {
            return new Vector4([1, 1, 1, 1]);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "x", {
        get: function () {
            return this._values[0];
        },
        set: function (value) {
            this._values[0] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "y", {
        get: function () {
            return this._values[1];
        },
        set: function (value) {
            this._values[1] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "z", {
        get: function () {
            return this._values[2];
        },
        set: function (value) {
            this._values[2] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "w", {
        get: function () {
            return this._values[3];
        },
        set: function (value) {
            this._values[3] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "xy", {
        get: function () {
            return [this._values[0], this._values[1]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "xyz", {
        get: function () {
            return [this._values[0], this._values[1], this._values[2]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
            this._values[2] = values[2];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "xyzw", {
        get: function () {
            return [this._values[0], this._values[1], this._values[2], this._values[3]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
            this._values[2] = values[2];
            this._values[3] = values[3];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "r", {
        get: function () {
            return this._values[0];
        },
        set: function (value) {
            this._values[0] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "g", {
        get: function () {
            return this._values[1];
        },
        set: function (value) {
            this._values[1] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "b", {
        get: function () {
            return this._values[2];
        },
        set: function (value) {
            this._values[2] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "a", {
        get: function () {
            return this._values[3];
        },
        set: function (value) {
            this._values[3] = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "rg", {
        get: function () {
            return [this._values[0], this._values[1]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "rgb", {
        get: function () {
            return [this._values[0], this._values[1], this._values[2]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
            this._values[2] = values[2];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Vector4.prototype, "rgba", {
        get: function () {
            return [this._values[0], this._values[1], this._values[2], this._values[3]];
        },
        set: function (values) {
            this._values[0] = values[0];
            this._values[1] = values[1];
            this._values[2] = values[2];
            this._values[3] = values[3];
        },
        enumerable: true,
        configurable: true
    });
    Vector4.lerp = function (a, b, t, dest) {
        if (!dest)
            dest = new Vector4();
        dest.x = a.x + t * (b.x - a.x);
        dest.y = a.y + t * (b.y - a.y);
        dest.z = a.z + t * (b.z - a.z);
        dest.w = a.w + t * (b.w - a.w);
        return dest;
    };
    Vector4.sum = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector4();
        dest.x = vector.x + vector2.x;
        dest.y = vector.y + vector2.y;
        dest.z = vector.z + vector2.z;
        dest.w = vector.w + vector2.w;
        return dest;
    };
    Vector4.difference = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector4();
        dest.x = vector.x - vector2.x;
        dest.y = vector.y - vector2.y;
        dest.z = vector.z - vector2.z;
        dest.w = vector.w - vector2.w;
        return dest;
    };
    Vector4.product = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector4();
        dest.x = vector.x * vector2.x;
        dest.y = vector.y * vector2.y;
        dest.z = vector.z * vector2.z;
        dest.w = vector.w * vector2.w;
        return dest;
    };
    Vector4.quotient = function (vector, vector2, dest) {
        if (!dest)
            dest = new Vector4();
        dest.x = vector.x / vector2.x;
        dest.y = vector.y / vector2.y;
        dest.z = vector.z / vector2.z;
        dest.w = vector.w / vector2.w;
        return dest;
    };
    Vector4.prototype.at = function (index) {
        return this._values[index];
    };
    Vector4.prototype.reset = function () {
        this.xyzw = [0, 0, 0, 0];
    };
    Vector4.prototype.copy = function (dest) {
        if (!dest)
            dest = new Vector4();
        dest.xyzw = this.xyzw;
        return dest;
    };
    Vector4.prototype.negate = function (dest) {
        if (!dest)
            dest = this;
        dest.x = -this.x;
        dest.y = -this.y;
        dest.z = -this.z;
        dest.w = -this.w;
        return dest;
    };
    Vector4.prototype.equals = function (vector, threshold) {
        if (threshold === void 0) { threshold = EPSILON; }
        if (Math.abs(this.x - vector.x) > threshold) {
            return false;
        }
        if (Math.abs(this.y - vector.y) > threshold) {
            return false;
        }
        if (Math.abs(this.z - vector.z) > threshold) {
            return false;
        }
        if (Math.abs(this.w - vector.w) > threshold) {
            return false;
        }
        return true;
    };
    Vector4.prototype.length = function () {
        return Math.sqrt(this.squaredLength());
    };
    Vector4.prototype.squaredLength = function () {
        var x = this.x;
        var y = this.y;
        var z = this.z;
        var w = this.w;
        return x * x + y * y + z * z + w * w;
    };
    Vector4.prototype.add = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x + vector.x;
        dest.y = this.y + vector.y;
        dest.z = this.z + vector.z;
        dest.w = this.w + vector.w;
        return dest;
    };
    Vector4.prototype.subtract = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x - vector.x;
        dest.y = this.y - vector.y;
        dest.z = this.z - vector.z;
        dest.w = this.w - vector.w;
        return dest;
    };
    Vector4.prototype.multiply = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x * vector.x;
        dest.y = this.y * vector.y;
        dest.z = this.z * vector.z;
        dest.w = this.w * vector.w;
        return dest;
    };
    Vector4.prototype.divide = function (vector, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x / vector.x;
        dest.y = this.y / vector.y;
        dest.z = this.z / vector.z;
        dest.w = this.w / vector.w;
        return dest;
    };
    Vector4.prototype.scale = function (value, dest) {
        if (!dest)
            dest = this;
        dest.x = this.x * value;
        dest.y = this.y * value;
        dest.z = this.z * value;
        dest.w = this.w * value;
        return dest;
    };
    Vector4.prototype.normalize = function (dest) {
        if (!dest)
            dest = this;
        dest.xyzw = this.xyzw;
        var length = dest.length();
        if (length === 1) {
            return dest;
        }
        if (length === 0) {
            dest.x *= 0;
            dest.y *= 0;
            dest.z *= 0;
            dest.w *= 0;
            return dest;
        }
        length = 1.0 / length;
        dest.x *= length;
        dest.y *= length;
        dest.z *= length;
        dest.w *= length;
        return dest;
    };
    return Vector4;
})());

((function () {
    function Matrix2(values) {
        this.mul = this.multiply;
        this.sub = this.subtract;
        this._values = new Float32Array(4);
        if (values) {
            this.init(values);
        }
        else {
            this.init([1, 0, 0, 1]);
        }
    }
    Object.defineProperty(Matrix2, "identity", {
        get: function () {
            return new Matrix2();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Creates a Matrix2 from a given angle
     * This is equivalent to (but much faster than):
     *     m = Matrix2.identity;
     *     m.rotate(radians, m);
     *
     * @param {number} radians the angle to rotate the matrix by
     * @param {Matrix2} dest the receiving matrix
     * @returns {Matrix2} dest
     */
    Matrix2.fromRotation = function (radians, dest) {
        if (!dest)
            dest = new Matrix2();
        var s = Math.sin(radians);
        var c = Math.cos(radians);
        dest._values[0] = c;
        dest._values[1] = s;
        dest._values[2] = -s;
        dest._values[3] = c;
        return dest;
    };
    /**
     * Creates a matrix from a vector scaling
     * This is equivalent to (but much faster than):
     *     m = Matrix2.identity;
     *     m.scale(v, m);
     *
     * @param {Vector2} v the scaling vector
     * @param {Matrix2} dest the receiving matrix
     * @returns {Matrix2} dest
     */
    Matrix2.fromScaling = function (v, dest) {
        if (!dest)
            dest = new Matrix2();
        dest._values[0] = v.x;
        dest._values[1] = 0;
        dest._values[2] = 0;
        dest._values[3] = v.y;
        return dest;
    };
    Matrix2.prototype.at = function (index) {
        return this._values[index];
    };
    Matrix2.prototype.init = function (values) {
        for (var i = 0; i < 4; i++) {
            this._values[i] = values[i];
        }
        return this;
    };
    Matrix2.prototype.copy = function (dest) {
        if (!dest)
            dest = new Matrix2();
        for (var i = 0; i < 4; i++) {
            dest._values[i] = this._values[i];
        }
        return dest;
    };
    Matrix2.prototype.all = function () {
        var data = [];
        for (var i = 0; i < 4; i++) {
            data[i] = this._values[i];
        }
        return data;
    };
    Matrix2.prototype.row = function (index) {
        return [this._values[index * 2 + 0], this._values[index * 2 + 1]];
    };
    Matrix2.prototype.col = function (index) {
        return [this._values[index], this._values[index + 2]];
    };
    Matrix2.prototype.equals = function (matrix, threshold) {
        if (threshold === void 0) { threshold = EPSILON; }
        for (var i = 0; i < 4; i++) {
            if (!equals(this.at(i), matrix.at(i), threshold)) {
                return false;
            }
        }
        return true;
    };
    Matrix2.prototype.determinant = function () {
        return this._values[0] * this._values[3] - this._values[2] * this._values[1];
    };
    Matrix2.prototype.setIdentity = function () {
        this._values[0] = 1;
        this._values[1] = 0;
        this._values[2] = 0;
        this._values[3] = 1;
        return this;
    };
    Matrix2.prototype.transpose = function (dest) {
        if (!dest)
            dest = this;
        this.copy(dest);
        var temp = this._values[1];
        dest._values[1] = this._values[2];
        dest._values[2] = temp;
        return dest;
    };
    Matrix2.prototype.inverse = function (dest) {
        if (!dest)
            dest = this;
        var a0 = this._values[0];
        var a1 = this._values[1];
        var a2 = this._values[2];
        var a3 = this._values[3];
        var det = this.determinant();
        if (!det)
            return;
        det = 1.0 / det;
        dest._values[0] = det * a3;
        dest._values[1] = det * -a1;
        dest._values[2] = det * -a2;
        dest._values[3] = det * a0;
        return dest;
    };
    /**
     * Calculates the adjugate of a Matrix2
     * @param {Matrix2} dest the receiving matrix
     * @returns {Matrix2} dest
     */
    Matrix2.prototype.adjoint = function (dest) {
        if (!dest)
            dest = this;
        var a0 = this._values[0];
        dest._values[0] = this._values[3];
        dest._values[1] = -this._values[1];
        dest._values[2] = -this._values[2];
        dest._values[3] = a0;
        return dest;
    };
    Matrix2.prototype.multiply = function (matrix, dest) {
        if (!dest)
            dest = this;
        var a0 = this._values[0];
        var a1 = this._values[1];
        var a2 = this._values[2];
        var a3 = this._values[3];
        var b0 = matrix._values[0];
        var b1 = matrix._values[1];
        var b2 = matrix._values[2];
        var b3 = matrix._values[3];
        dest._values[0] = a0 * b0 + a2 * b1;
        dest._values[1] = a1 * b0 + a3 * b1;
        dest._values[2] = a0 * b2 + a2 * b3;
        dest._values[3] = a1 * b2 + a3 * b3;
        return dest;
    };
    /**
     * Rotates a Matrix2 by the given angle
     * @param {number} radians the angle to rotate the matrix by
     * @param {Matrix2} dest the receiving matrix
     * @returns {Matrix2} dest
     */
    Matrix2.prototype.rotate = function (radians, dest) {
        if (!dest)
            dest = this;
        var a0 = this._values[0];
        var a1 = this._values[1];
        var a2 = this._values[2];
        var a3 = this._values[3];
        var s = Math.sin(radians);
        var c = Math.cos(radians);
        dest._values[0] = a0 * c + a2 * s;
        dest._values[1] = a1 * c + a3 * s;
        dest._values[2] = a0 * -s + a2 * c;
        dest._values[3] = a1 * -s + a3 * c;
        return dest;
    };
    /**
     * Scales a Matrix2 by the dimensions in the given Vector2
     * @param {Vector2} v the Vector2 to scale the matrix by
     * @param {Matrix2} dest the receiving matrix
     * @returns {Matrix2} dest
     */
    Matrix2.prototype.scale = function (v, dest) {
        if (!dest)
            dest = this;
        var a0 = this._values[0];
        var a1 = this._values[1];
        var a2 = this._values[2];
        var a3 = this._values[3];
        dest._values[0] = a0 * v.x;
        dest._values[1] = a1 * v.x;
        dest._values[2] = a2 * v.y;
        dest._values[3] = a3 * v.y;
        return dest;
    };
    Matrix2.prototype.toString = function () {
        return ('mat2(' +
            this._values[0] +
            ', ' +
            this._values[1] +
            ', ' +
            this._values[2] +
            ', ' +
            this._values[3] +
            ')');
    };
    /**
     * Returns the Frobenius norm of a Matrix2
     * @returns {number} Frobenius norm
     */
    Matrix2.prototype.frobenius = function () {
        var a0 = this._values[0];
        var a1 = this._values[1];
        var a2 = this._values[2];
        var a3 = this._values[3];
        return Math.sqrt(Math.pow(a0, 2) + Math.pow(a1, 2) + Math.pow(a2, 2) + Math.pow(a3, 2));
    };
    // TODO: LDU decomposition
    Matrix2.prototype.add = function (matrix, dest) {
        if (!dest)
            dest = this;
        dest._values[0] = this._values[0] + matrix._values[0];
        dest._values[1] = this._values[1] + matrix._values[1];
        dest._values[2] = this._values[2] + matrix._values[2];
        dest._values[3] = this._values[3] + matrix._values[3];
        return dest;
    };
    Matrix2.prototype.subtract = function (matrix, dest) {
        if (!dest)
            dest = this;
        dest._values[0] = this._values[0] - matrix._values[0];
        dest._values[1] = this._values[1] - matrix._values[1];
        dest._values[2] = this._values[2] - matrix._values[2];
        dest._values[3] = this._values[3] - matrix._values[3];
        return dest;
    };
    Matrix2.prototype.exactEquals = function (matrix) {
        for (var i = 0; i < 4; i++) {
            if (this.at(i) !== matrix.at(i))
                return false;
        }
        return true;
    };
    Matrix2.prototype.multiplyScalar = function (scalar, dest) {
        if (!dest)
            dest = this;
        dest._values[0] = this._values[0] * scalar;
        dest._values[1] = this._values[1] * scalar;
        dest._values[2] = this._values[2] * scalar;
        dest._values[3] = this._values[3] * scalar;
        return dest;
    };
    return Matrix2;
})());

var EPSILON = 0.00001;
/**
 * Tests whether or not the arguments have approximately the same value, within an absolute
 * or relative tolerance (an absolute tolerance is used for values less  than or equal to
 * 1.0, and a relative tolerance is used for larger values)
 * @param {number} a The first number to test
 * @param {number} b The second number to test
 * @param {number} tolerance The tolerance to test for
 * @returns {boolean} True if the numbers are approximately equal, false otherwise
 */
function equals(a, b, tolerance) {
    if (tolerance === void 0) { tolerance = EPSILON; }
    return Math.abs(a - b) <= tolerance * Math.max(1.0, Math.abs(a), Math.abs(b));
}

var propTypes = {exports: {}};

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var ReactPropTypesSecret$1 = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

var ReactPropTypesSecret_1 = ReactPropTypesSecret$1;

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var ReactPropTypesSecret = ReactPropTypesSecret_1;

function emptyFunction() {}
function emptyFunctionWithReset() {}
emptyFunctionWithReset.resetWarningCache = emptyFunction;

var factoryWithThrowingShims = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret) {
      // It is still safe when called from React.
      return;
    }
    var err = new Error(
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
    err.name = 'Invariant Violation';
    throw err;
  }  shim.isRequired = shim;
  function getShim() {
    return shim;
  }  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bigint: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    elementType: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim,

    checkPropTypes: emptyFunctionWithReset,
    resetWarningCache: emptyFunction
  };

  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

{
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  propTypes.exports = factoryWithThrowingShims();
}

/**
 * Calculate a point transformed with an affine matrix
 * @param matrix {Matrix} Affine Matrix
 * @param  point {Point} Point
 * @returns {Point} Point
 */
function applyToPoint (matrix, point) {
  return Array.isArray(point)
    ? [
        matrix.a * point[0] + matrix.c * point[1] + matrix.e,
        matrix.b * point[0] + matrix.d * point[1] + matrix.f
      ]
    : {
        x: matrix.a * point.x + matrix.c * point.y + matrix.e,
        y: matrix.b * point.x + matrix.d * point.y + matrix.f
      }
}

/**
 * Calculate an array of points transformed with an affine matrix
 * @param matrix {Matrix} Affine Matrix
 * @param points {Point[]} Array of point
 * @returns {Point[]} Array of point
 */
function applyToPoints (matrix, points) {
  return points.map(point => applyToPoint(matrix, point))
}

/**
 * Extract an affine matrix from an object that contains a,b,c,d,e,f keys
 * Any value could be a float or a string that contains a float
 * @param object {Object} Object that contains a,b,c,d,e,f keys
 * @return {Matrix} Affine Matrix
 */
function fromObject (object) {
  return {
    a: parseFloat(object.a),
    b: parseFloat(object.b),
    c: parseFloat(object.c),
    d: parseFloat(object.d),
    e: parseFloat(object.e),
    f: parseFloat(object.f)
  }
}

/**
 * Identity matrix
 * @returns {Matrix} Affine Matrix
 */
function identity () {
  return {
    a: 1,
    c: 0,
    e: 0,
    b: 0,
    d: 1,
    f: 0
  }
}

/**
 * Calculate a matrix that is the inverse of the provided matrix
 * @param matrix {Matrix} Affine Matrix
 * @returns {Matrix} Inverted Affine Matrix
 */
function inverse (matrix) {
  // http://www.wolframalpha.com/input/?i=Inverse+%5B%7B%7Ba,c,e%7D,%7Bb,d,f%7D,%7B0,0,1%7D%7D%5D

  const { a, b, c, d, e, f } = matrix;

  const denom = a * d - b * c;

  return {
    a: d / denom,
    b: b / -denom,
    c: c / -denom,
    d: a / denom,
    e: (d * e - c * f) / -denom,
    f: (b * e - a * f) / denom
  }
}

function isUndefined (val) {
  return typeof val === 'undefined'
}

/**
 * Calculate a translate matrix
 * @param tx {number} Translation on axis x
 * @param [ty = 0] {number} Translation on axis y
 * @returns {Matrix} Affine Matrix
 */
function translate (tx, ty = 0) {
  return {
    a: 1,
    c: 0,
    e: tx,
    b: 0,
    d: 1,
    f: ty
  }
}

/**
 * Merge multiple matrices into one
 * @param matrices {...Matrix | Matrix[]} Matrices listed as separate parameters or in an array
 * @returns {Matrix} Affine Matrix
 */
function transform (...matrices) {
  matrices = Array.isArray(matrices[0]) ? matrices[0] : matrices;

  const multiply = (m1, m2) => {
    return {
      a: m1.a * m2.a + m1.c * m2.b,
      c: m1.a * m2.c + m1.c * m2.d,
      e: m1.a * m2.e + m1.c * m2.f + m1.e,
      b: m1.b * m2.a + m1.d * m2.b,
      d: m1.b * m2.c + m1.d * m2.d,
      f: m1.b * m2.e + m1.d * m2.f + m1.f
    }
  };

  switch (matrices.length) {
    case 0:
      throw new Error('no matrices provided')

    case 1:
      return matrices[0]

    case 2:
      return multiply(matrices[0], matrices[1])

    default: {
      const [m1, m2, ...rest] = matrices;
      const m = multiply(m1, m2);
      return transform(m, ...rest)
    }
  }
}

/**
 * Calculate a scaling matrix
 * @param sx {number} Scaling on axis x
 * @param [sy = sx] {number} Scaling on axis y (default sx)
 * @param [cx] {number} If (cx,cy) are supplied the scaling is about this point
 * @param [cy] {number} If (cx,cy) are supplied the scaling is about this point
 * @returns {Matrix} Affine Matrix
 */
function scale (sx, sy = undefined, cx = undefined, cy = undefined) {
  if (isUndefined(sy)) sy = sx;

  const scaleMatrix = {
    a: sx,
    c: 0,
    e: 0,
    b: 0,
    d: sy,
    f: 0
  };

  if (isUndefined(cx) || isUndefined(cy)) {
    return scaleMatrix
  }

  return transform([
    translate(cx, cy),
    scaleMatrix,
    translate(-cx, -cy)
  ])
}

/**
 * Serialize an affine matrix to a string that can be used with CSS or SVG
 * @param matrix {Matrix} Affine Matrix
 * @returns {string} String that contains an affine matrix formatted as matrix(a,b,c,d,e,f)
 */

/**
 * Serialize an affine matrix to a string that can be used with CSS or SVG
 * @param matrix {Matrix} Affine Matrix
 * @returns {string} String that contains an affine matrix formatted as matrix(a,b,c,d,e,f)
 */
function toSVG (matrix) {
  return toString(matrix)
}

/**
 * Serialize an affine matrix to a string that can be used with CSS or SVG
 * @param matrix {Matrix} Affine Matrix
 * @returns {string} String that contains an affine matrix formatted as matrix(a,b,c,d,e,f)
 */
function toString (matrix) {
  return `matrix(${matrix.a},${matrix.b},${matrix.c},${matrix.d},${matrix.e},${matrix.f})`
}

// Generated by Peggy 3.0.0.
//
// https://peggyjs.org/

function peg$subclass(child, parent) {
  function C() { this.constructor = child; }
  C.prototype = parent.prototype;
  child.prototype = new C();
}

function peg$SyntaxError(message, expected, found, location) {
  var self = Error.call(this, message);
  // istanbul ignore next Check is a necessary evil to support older environments
  if (Object.setPrototypeOf) {
    Object.setPrototypeOf(self, peg$SyntaxError.prototype);
  }
  self.expected = expected;
  self.found = found;
  self.location = location;
  self.name = "SyntaxError";
  return self;
}

peg$subclass(peg$SyntaxError, Error);

function peg$padEnd(str, targetLength, padString) {
  padString = padString || " ";
  if (str.length > targetLength) { return str; }
  targetLength -= str.length;
  padString += padString.repeat(targetLength);
  return str + padString.slice(0, targetLength);
}

peg$SyntaxError.prototype.format = function(sources) {
  var str = "Error: " + this.message;
  if (this.location) {
    var src = null;
    var k;
    for (k = 0; k < sources.length; k++) {
      if (sources[k].source === this.location.source) {
        src = sources[k].text.split(/\r\n|\n|\r/g);
        break;
      }
    }
    var s = this.location.start;
    var offset_s = (this.location.source && (typeof this.location.source.offset === "function"))
      ? this.location.source.offset(s)
      : s;
    var loc = this.location.source + ":" + offset_s.line + ":" + offset_s.column;
    if (src) {
      var e = this.location.end;
      var filler = peg$padEnd("", offset_s.line.toString().length, ' ');
      var line = src[s.line - 1];
      var last = s.line === e.line ? e.column : line.length + 1;
      var hatLen = (last - s.column) || 1;
      str += "\n --> " + loc + "\n"
          + filler + " |\n"
          + offset_s.line + " | " + line + "\n"
          + filler + " | " + peg$padEnd("", s.column - 1, ' ')
          + peg$padEnd("", hatLen, "^");
    } else {
      str += "\n at " + loc;
    }
  }
  return str;
};

peg$SyntaxError.buildMessage = function(expected, found) {
  var DESCRIBE_EXPECTATION_FNS = {
    literal: function(expectation) {
      return "\"" + literalEscape(expectation.text) + "\"";
    },

    class: function(expectation) {
      var escapedParts = expectation.parts.map(function(part) {
        return Array.isArray(part)
          ? classEscape(part[0]) + "-" + classEscape(part[1])
          : classEscape(part);
      });

      return "[" + (expectation.inverted ? "^" : "") + escapedParts.join("") + "]";
    },

    any: function() {
      return "any character";
    },

    end: function() {
      return "end of input";
    },

    other: function(expectation) {
      return expectation.description;
    }
  };

  function hex(ch) {
    return ch.charCodeAt(0).toString(16).toUpperCase();
  }

  function literalEscape(s) {
    return s
      .replace(/\\/g, "\\\\")
      .replace(/"/g,  "\\\"")
      .replace(/\0/g, "\\0")
      .replace(/\t/g, "\\t")
      .replace(/\n/g, "\\n")
      .replace(/\r/g, "\\r")
      .replace(/[\x00-\x0F]/g,          function(ch) { return "\\x0" + hex(ch); })
      .replace(/[\x10-\x1F\x7F-\x9F]/g, function(ch) { return "\\x"  + hex(ch); });
  }

  function classEscape(s) {
    return s
      .replace(/\\/g, "\\\\")
      .replace(/\]/g, "\\]")
      .replace(/\^/g, "\\^")
      .replace(/-/g,  "\\-")
      .replace(/\0/g, "\\0")
      .replace(/\t/g, "\\t")
      .replace(/\n/g, "\\n")
      .replace(/\r/g, "\\r")
      .replace(/[\x00-\x0F]/g,          function(ch) { return "\\x0" + hex(ch); })
      .replace(/[\x10-\x1F\x7F-\x9F]/g, function(ch) { return "\\x"  + hex(ch); });
  }

  function describeExpectation(expectation) {
    return DESCRIBE_EXPECTATION_FNS[expectation.type](expectation);
  }

  function describeExpected(expected) {
    var descriptions = expected.map(describeExpectation);
    var i, j;

    descriptions.sort();

    if (descriptions.length > 0) {
      for (i = 1, j = 1; i < descriptions.length; i++) {
        if (descriptions[i - 1] !== descriptions[i]) {
          descriptions[j] = descriptions[i];
          j++;
        }
      }
      descriptions.length = j;
    }

    switch (descriptions.length) {
      case 1:
        return descriptions[0];

      case 2:
        return descriptions[0] + " or " + descriptions[1];

      default:
        return descriptions.slice(0, -1).join(", ")
          + ", or "
          + descriptions[descriptions.length - 1];
    }
  }

  function describeFound(found) {
    return found ? "\"" + literalEscape(found) + "\"" : "end of input";
  }

  return "Expected " + describeExpected(expected) + " but " + describeFound(found) + " found.";
};

var MODE_IDLE = 'idle';
var MODE_PANNING = 'panning';
var MODE_ZOOMING = 'zooming';
var TOOL_AUTO = 'auto';
var TOOL_NONE = 'none';
var TOOL_PAN = 'pan';
var TOOL_ZOOM_IN = 'zoom-in';
var TOOL_ZOOM_OUT = 'zoom-out';
var POSITION_NONE = 'none';
var POSITION_TOP = 'top';
var POSITION_RIGHT = 'right';
var POSITION_BOTTOM = 'bottom';
var POSITION_LEFT = 'left';
var ACTION_ZOOM = 'zoom';
var ACTION_PAN = 'pan';
var ALIGN_CENTER = 'center';
var ALIGN_LEFT = 'left';
var ALIGN_RIGHT = 'right';
var ALIGN_TOP = 'top';
var ALIGN_BOTTOM = 'bottom';
var ALIGN_COVER = 'cover';

function _typeof$c(obj) { "@babel/helpers - typeof"; return _typeof$c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$c(obj); }
function ownKeys$5(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread$5(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys$5(Object(source), !0).forEach(function (key) { _defineProperty$7(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys$5(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty$7(obj, key, value) { key = _toPropertyKey$c(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey$c(arg) { var key = _toPrimitive$c(arg, "string"); return _typeof$c(key) === "symbol" ? key : String(key); }
function _toPrimitive$c(input, hint) { if (_typeof$c(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$c(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var VERSION = 3;
var DEFAULT_MODE = MODE_IDLE;

/**
 * Obtain default value
 * @returns {Object}
 */
function getDefaultValue(viewerWidth, viewerHeight, SVGMinX, SVGMinY, SVGWidth, SVGHeight) {
  var scaleFactorMin = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : null;
  var scaleFactorMax = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : null;
  return set({}, _objectSpread$5(_objectSpread$5({}, identity()), {}, {
    version: VERSION,
    mode: DEFAULT_MODE,
    focus: false,
    pinchPointDistance: null,
    prePinchMode: null,
    viewerWidth: viewerWidth,
    viewerHeight: viewerHeight,
    SVGMinX: SVGMinX,
    SVGMinY: SVGMinY,
    SVGWidth: SVGWidth,
    SVGHeight: SVGHeight,
    scaleFactorMin: scaleFactorMin,
    scaleFactorMax: scaleFactorMax,
    startX: null,
    startY: null,
    endX: null,
    endY: null,
    miniatureOpen: true,
    lastAction: null
  }));
}

/**
 * Change value
 * @param value
 * @param patch
 * @param action
 * @returns {Object}
 */
function set(value, patch) {
  var action = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  value = Object.assign({}, value, patch, {
    lastAction: action
  });
  return Object.freeze(value);
}

/**
 * value valid check
 * @param value
 */
function isValueValid(value) {
  return value !== null && _typeof$c(value) === 'object' && value.hasOwnProperty('version') && value.version === VERSION;
}

/**
 * Export x,y coords relative to SVG
 * @param value
 * @param viewerX
 * @param viewerY
 * @returns {*|{x, y}|{x: number, y: number}}
 */
function getSVGPoint(value, viewerX, viewerY) {
  var matrix = fromObject(value);
  var inverseMatrix = inverse(matrix);
  return applyToPoint(inverseMatrix, {
    x: viewerX,
    y: viewerY
  });
}

/**
 * Decompose matrix from value
 * @param value
 * @returns {{scaleFactor: number, translationX: number, translationY: number}}
 */
function decompose(value) {
  var matrix = fromObject(value);
  return {
    scaleFactor: matrix.a,
    translationX: matrix.e,
    translationY: matrix.f
  };
}

/**
 *
 * @param value
 * @param focus
 * @returns {Object}
 */
function setFocus(value, focus) {
  return set(value, {
    focus: focus
  });
}

/**
 *
 * @param value
 * @param viewerWidth
 * @param viewerHeight
 * @returns {Object}
 */
function setViewerSize(value, viewerWidth, viewerHeight) {
  return set(value, {
    viewerWidth: viewerWidth,
    viewerHeight: viewerHeight
  });
}

/**
 *
 * @param value
 * @param SVGMinX
 * @param SVGMinY
 * @param SVGWidth
 * @param SVGHeight
 * @returns {Object}
 */
function setSVGViewBox(value, SVGMinX, SVGMinY, SVGWidth, SVGHeight) {
  return set(value, {
    SVGMinX: SVGMinX,
    SVGMinY: SVGMinY,
    SVGWidth: SVGWidth,
    SVGHeight: SVGHeight
  });
}

/**
 *
 * @param value
 * @param scaleFactorMin
 * @param scaleFactorMax
 * @returns {Object}
 */
//TODO rename to setZoomLimits
function setZoomLevels(value, scaleFactorMin, scaleFactorMax) {
  return set(value, {
    scaleFactorMin: scaleFactorMin,
    scaleFactorMax: scaleFactorMax
  });
}

/**
 *
 * @param value
 * @param SVGPointX
 * @param SVGPointY
 * @param zoomLevel
 * @returns {Object}
 */
function setPointOnViewerCenter(value, SVGPointX, SVGPointY, zoomLevel) {
  var viewerWidth = value.viewerWidth,
    viewerHeight = value.viewerHeight;
  var matrix = transform(translate(-SVGPointX + viewerWidth / 2, -SVGPointY + viewerHeight / 2),
  //4
  translate(SVGPointX, SVGPointY),
  //3
  scale(zoomLevel, zoomLevel),
  //2
  translate(-SVGPointX, -SVGPointY) //1
  );

  return set(value, _objectSpread$5({
    mode: MODE_IDLE
  }, matrix));
}

/**
 *
 * @param value
 * @returns {Object}
 */
function reset(value) {
  return set(value, _objectSpread$5({
    mode: MODE_IDLE
  }, identity()));
}

/**
 *
 * @param value
 * @returns {Object}
 */
function resetMode(value) {
  return set(value, {
    mode: DEFAULT_MODE,
    startX: null,
    startY: null,
    endX: null,
    endY: null
  });
}

function _typeof$b(obj) { "@babel/helpers - typeof"; return _typeof$b = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$b(obj); }
function _classCallCheck$6(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties$6(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey$b(descriptor.key), descriptor); } }
function _createClass$6(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties$6(Constructor.prototype, protoProps); if (staticProps) _defineProperties$6(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey$b(arg) { var key = _toPrimitive$b(arg, "string"); return _typeof$b(key) === "symbol" ? key : String(key); }
function _toPrimitive$b(input, hint) { if (_typeof$b(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$b(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var ViewerEvent = /*#__PURE__*/function () {
  function ViewerEvent(originalEvent, value, SVGViewer) {
    _classCallCheck$6(this, ViewerEvent);
    this.originalEvent = originalEvent;
    this.value = value;
    this.SVGViewer = SVGViewer;
  }
  _createClass$6(ViewerEvent, [{
    key: "scaleFactor",
    get: function get() {
      this._cacheDecomposedValue = this._cacheDecomposedValue || decompose(this.value);
      return this._cacheDecomposedValue.scaleFactor;
    }
  }, {
    key: "translationX",
    get: function get() {
      this._cacheDecomposedValue = this._cacheDecomposedValue || decompose(this.value);
      return this._cacheDecomposedValue.translationX;
    }
  }, {
    key: "translationY",
    get: function get() {
      this._cacheDecomposedValue = this._cacheDecomposedValue || decompose(this.value);
      return this._cacheDecomposedValue.translationY;
    }
  }, {
    key: "preventDefault",
    value: function preventDefault() {
      this.originalEvent.preventDefault();
    }
  }, {
    key: "stopPropagation",
    value: function stopPropagation() {
      this.originalEvent.stopPropagation();
    }
  }]);
  return ViewerEvent;
}();

function _typeof$a(obj) { "@babel/helpers - typeof"; return _typeof$a = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$a(obj); }
function ownKeys$4(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread$4(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys$4(Object(source), !0).forEach(function (key) { _defineProperty$6(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys$4(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty$6(obj, key, value) { key = _toPropertyKey$a(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey$a(arg) { var key = _toPrimitive$a(arg, "string"); return _typeof$a(key) === "symbol" ? key : String(key); }
function _toPrimitive$a(input, hint) { if (_typeof$a(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$a(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _slicedToArray$2(arr, i) { return _arrayWithHoles$2(arr) || _iterableToArrayLimit$2(arr, i) || _unsupportedIterableToArray$2(arr, i) || _nonIterableRest$2(); }
function _nonIterableRest$2() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray$2(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray$2(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$2(o, minLen); }
function _arrayLikeToArray$2(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit$2(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles$2(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Atomic pan operation
 * @param value
 * @param SVGDeltaX drag movement
 * @param SVGDeltaY drag movement
 * @param panLimit forces the image to keep at least x pixel inside the viewer
 * @returns {Object}
 */
function pan(value, SVGDeltaX, SVGDeltaY) {
  var panLimit = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
  var matrix = transform(fromObject(value),
  //2
  translate(SVGDeltaX, SVGDeltaY) //1
  );

  // apply pan limits
  if (panLimit) {
    var _applyToPoints = applyToPoints(matrix, [{
        x: value.SVGMinX + panLimit,
        y: value.SVGMinY + panLimit
      }, {
        x: value.SVGMinX + value.SVGWidth - panLimit,
        y: value.SVGMinY + value.SVGHeight - panLimit
      }]),
      _applyToPoints2 = _slicedToArray$2(_applyToPoints, 2),
      _applyToPoints2$ = _applyToPoints2[0],
      x1 = _applyToPoints2$.x,
      y1 = _applyToPoints2$.y,
      _applyToPoints2$2 = _applyToPoints2[1],
      x2 = _applyToPoints2$2.x,
      y2 = _applyToPoints2$2.y;

    //x limit
    var moveX = 0;
    if (value.viewerWidth - x1 < 0) moveX = value.viewerWidth - x1;else if (x2 < 0) moveX = -x2;

    //y limit
    var moveY = 0;
    if (value.viewerHeight - y1 < 0) moveY = value.viewerHeight - y1;else if (y2 < 0) moveY = -y2;

    //apply limits
    matrix = transform(translate(moveX, moveY), matrix);
  }
  return set(value, _objectSpread$4({
    mode: MODE_IDLE
  }, matrix), ACTION_PAN);
}

/**
 * Start pan operation lifecycle
 * @param value
 * @param viewerX
 * @param viewerY
 * @return {ReadonlyArray<unknown>}
 */
function startPanning(value, viewerX, viewerY) {
  return set(value, {
    mode: MODE_PANNING,
    startX: viewerX,
    startY: viewerY,
    endX: viewerX,
    endY: viewerY
  }, ACTION_PAN);
}

/**
 * Continue pan operation lifecycle
 * @param value
 * @param viewerX
 * @param viewerY
 * @param panLimit
 * @return {ReadonlyArray<unknown>}
 */
function updatePanning(value, viewerX, viewerY, panLimit) {
  if (value.mode !== MODE_PANNING) throw new Error('update pan not allowed in this mode ' + value.mode);
  var endX = value.endX,
    endY = value.endY;
  var start = getSVGPoint(value, endX, endY);
  var end = getSVGPoint(value, viewerX, viewerY);
  var deltaX = end.x - start.x;
  var deltaY = end.y - start.y;
  var nextValue = pan(value, deltaX, deltaY, panLimit);
  return set(nextValue, {
    mode: MODE_PANNING,
    endX: viewerX,
    endY: viewerY
  }, ACTION_PAN);
}

/**
 * Stop pan operation lifecycle
 * @param value
 * @return {ReadonlyArray<unknown>}
 */
function stopPanning(value) {
  return set(value, {
    mode: MODE_IDLE,
    startX: null,
    startY: null,
    endX: null,
    endY: null
  }, ACTION_PAN);
}

/**
 * when pointer is on viewer edge -> pan image
 * @param value
 * @param viewerX
 * @param viewerY
 * @return {ReadonlyArray<any>}
 */
function autoPanIfNeeded(value, viewerX, viewerY) {
  var deltaX = 0;
  var deltaY = 0;
  if (viewerY <= 20) deltaY = 2;
  if (value.viewerWidth - viewerX <= 20) deltaX = -2;
  if (value.viewerHeight - viewerY <= 20) deltaY = -2;
  if (viewerX <= 20) deltaX = 2;
  deltaX = deltaX / value.d;
  deltaY = deltaY / value.d;
  return deltaX === 0 && deltaY === 0 ? value : pan(value, deltaX, deltaY);
}

function calculateBox(start, end) {
  if (start.x <= end.x && start.y <= end.y) {
    return {
      x: start.x,
      y: start.y,
      width: end.x - start.x,
      height: end.y - start.y
    };
  } else if (start.x >= end.x && start.y <= end.y) {
    return {
      x: end.x,
      y: start.y,
      width: start.x - end.x,
      height: end.y - start.y
    };
  } else if (start.x >= end.x && start.y >= end.y) {
    return {
      x: end.x,
      y: end.y,
      width: start.x - end.x,
      height: start.y - end.y
    };
  } else if (start.x <= end.x && start.y >= end.y) {
    return {
      x: start.x,
      y: end.y,
      width: end.x - start.x,
      height: start.y - end.y
    };
  }
}

function _typeof$9(obj) { "@babel/helpers - typeof"; return _typeof$9 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$9(obj); }
function ownKeys$3(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread$3(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys$3(Object(source), !0).forEach(function (key) { _defineProperty$5(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys$3(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty$5(obj, key, value) { key = _toPropertyKey$9(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey$9(arg) { var key = _toPrimitive$9(arg, "string"); return _typeof$9(key) === "symbol" ? key : String(key); }
function _toPrimitive$9(input, hint) { if (_typeof$9(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$9(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function isZoomLevelGoingOutOfBounds(value, scaleFactor) {
  var _decompose = decompose(value),
    curScaleFactor = _decompose.scaleFactor;
  var lessThanScaleFactorMin = value.scaleFactorMin && curScaleFactor * scaleFactor < value.scaleFactorMin;
  var moreThanScaleFactorMax = value.scaleFactorMax && curScaleFactor * scaleFactor > value.scaleFactorMax;
  return lessThanScaleFactorMin && scaleFactor < 1 || moreThanScaleFactorMax && scaleFactor > 1;
}
function limitZoomLevel(value, matrix) {
  var scaleLevel = matrix.a;
  if (value.scaleFactorMin != null) {
    // limit minimum zoom
    scaleLevel = Math.max(scaleLevel, value.scaleFactorMin);
  }
  if (value.scaleFactorMax != null) {
    // limit maximum zoom
    scaleLevel = Math.min(scaleLevel, value.scaleFactorMax);
  }
  return set(matrix, {
    a: scaleLevel,
    d: scaleLevel
  });
}
function zoom(value, SVGPointX, SVGPointY, scaleFactor) {
  if (isZoomLevelGoingOutOfBounds(value, scaleFactor)) {
    // Do not change translation and scale of value
    return value;
  }
  var matrix = transform(fromObject(value), translate(SVGPointX, SVGPointY), scale(scaleFactor, scaleFactor), translate(-SVGPointX, -SVGPointY));
  return set(value, _objectSpread$3(_objectSpread$3({
    mode: MODE_IDLE
  }, matrix), {}, {
    startX: null,
    startY: null,
    endX: null,
    endY: null
  }), ACTION_ZOOM);
}

//ENHANCEMENT: add ability to control alignment
//ENHANCEMENT: add ability to selectively fit image inside viewer
//ENHANCEMENT: refactor some logic in order to merge with fitToViewer function
function fitSelection(value, selectionSVGPointX, selectionSVGPointY, selectionWidth, selectionHeight) {
  var viewerWidth = value.viewerWidth,
    viewerHeight = value.viewerHeight;
  var scaleX = viewerWidth / selectionWidth;
  var scaleY = viewerHeight / selectionHeight;
  var scaleLevel = Math.min(scaleX, scaleY);
  var matrix = transform(scale(scaleLevel, scaleLevel),
  //2
  translate(-selectionSVGPointX, -selectionSVGPointY) //1
  );

  if (isZoomLevelGoingOutOfBounds(value, scaleLevel / value.d)) {
    // Do not allow scale and translation
    return set(value, {
      mode: MODE_IDLE,
      startX: null,
      startY: null,
      endX: null,
      endY: null
    });
  }
  return set(value, _objectSpread$3(_objectSpread$3({
    mode: MODE_IDLE
  }, limitZoomLevel(value, matrix)), {}, {
    startX: null,
    startY: null,
    endX: null,
    endY: null
  }), ACTION_ZOOM);
}
function fitToViewer(value) {
  var SVGAlignX = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ALIGN_LEFT;
  var SVGAlignY = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ALIGN_TOP;
  var viewerWidth = value.viewerWidth,
    viewerHeight = value.viewerHeight,
    SVGMinX = value.SVGMinX,
    SVGMinY = value.SVGMinY,
    SVGWidth = value.SVGWidth,
    SVGHeight = value.SVGHeight;
  var scaleX = viewerWidth / SVGWidth;
  var scaleY = viewerHeight / SVGHeight;
  var scaleLevel = Math.min(scaleX, scaleY);
  var scaleMatrix = scale(scaleLevel, scaleLevel);
  var translateX = -SVGMinX * scaleX;
  var translateY = -SVGMinY * scaleY;

  // after fitting, SVG and the viewer will match in width (1) or in height (2) or SVG will cover the container with preserving aspect ratio (0)
  if (scaleX < scaleY) {
    var remainderY = viewerHeight - scaleX * SVGHeight;

    //(1) match in width, meaning scaled SVGHeight <= viewerHeight
    switch (SVGAlignY) {
      case ALIGN_TOP:
        translateY = -SVGMinY * scaleLevel;
        break;
      case ALIGN_CENTER:
        translateY = Math.round(remainderY / 2) - SVGMinY * scaleLevel;
        break;
      case ALIGN_BOTTOM:
        translateY = remainderY - SVGMinY * scaleLevel;
        break;
      case ALIGN_COVER:
        scaleMatrix = scale(scaleY, scaleY); // (0) we must now match to short edge, in this case - height
        var remainderX = viewerWidth - scaleY * SVGWidth; // calculate remainder in the other scale

        translateX = SVGMinX + Math.round(remainderX / 2); // center by the long edge
        break;
      //no op
    }
  } else {
    var _remainderX = viewerWidth - scaleY * SVGWidth;

    //(2) match in height, meaning scaled SVGWidth <= viewerWidth
    switch (SVGAlignX) {
      case ALIGN_LEFT:
        translateX = -SVGMinX * scaleLevel;
        break;
      case ALIGN_CENTER:
        translateX = Math.round(_remainderX / 2) - SVGMinX * scaleLevel;
        break;
      case ALIGN_RIGHT:
        translateX = _remainderX - SVGMinX * scaleLevel;
        break;
      case ALIGN_COVER:
        scaleMatrix = scale(scaleX, scaleX); // (0) we must now match to short edge, in this case - width
        var _remainderY = viewerHeight - scaleX * SVGHeight; // calculate remainder in the other scale

        translateY = SVGMinY + Math.round(_remainderY / 2); // center by the long edge
        break;
      //no op
    }
  }

  var translationMatrix = translate(translateX, translateY);
  var matrix = transform(translationMatrix,
  //2
  scaleMatrix //1
  );

  if (isZoomLevelGoingOutOfBounds(value, scaleLevel / value.d)) {
    // Do not allow scale and translation
    return set(value, {
      mode: MODE_IDLE,
      startX: null,
      startY: null,
      endX: null,
      endY: null
    });
  }
  return set(value, _objectSpread$3(_objectSpread$3({
    mode: MODE_IDLE
  }, limitZoomLevel(value, matrix)), {}, {
    startX: null,
    startY: null,
    endX: null,
    endY: null
  }), ACTION_ZOOM);
}
function zoomOnViewerCenter(value, scaleFactor) {
  var viewerWidth = value.viewerWidth,
    viewerHeight = value.viewerHeight;
  var SVGPoint = getSVGPoint(value, viewerWidth / 2, viewerHeight / 2);
  return zoom(value, SVGPoint.x, SVGPoint.y, scaleFactor);
}
function startZooming(value, viewerX, viewerY) {
  return set(value, {
    mode: MODE_ZOOMING,
    startX: viewerX,
    startY: viewerY,
    endX: viewerX,
    endY: viewerY
  });
}
function updateZooming(value, viewerX, viewerY) {
  if (value.mode !== MODE_ZOOMING) throw new Error('update selection not allowed in this mode ' + value.mode);
  return set(value, {
    endX: viewerX,
    endY: viewerY
  });
}
function stopZooming(value, viewerX, viewerY, scaleFactor) {
  var TOLERATED_DISTANCE = 7; //minimum distance to choose if area selection or drill down on point
  var startX = value.startX,
    startY = value.startY;
  var start = getSVGPoint(value, startX, startY);
  var end = getSVGPoint(value, viewerX, viewerY);
  if (Math.abs(startX - viewerX) > TOLERATED_DISTANCE && Math.abs(startY - viewerY) > TOLERATED_DISTANCE) {
    var box = calculateBox(start, end);
    return fitSelection(value, box.x, box.y, box.width, box.height);
  } else {
    var SVGPoint = getSVGPoint(value, viewerX, viewerY);
    return zoom(value, SVGPoint.x, SVGPoint.y, scaleFactor);
  }
}

/**
 * Convert (re-map) an input value range into a destination range.
 * @param value
 * @param sourceStart
 * @param sourceEnd
 * @param targetStart
 * @param targetEnd
 * @return number
 */

function mapRange(value, sourceStart, sourceEnd, targetStart, targetEnd) {
  return targetStart + (targetEnd - targetStart) * (value - sourceStart) / (sourceEnd - sourceStart);
}

function getMousePosition(event, ViewerDOM) {
  var _ViewerDOM$getBoundin = ViewerDOM.getBoundingClientRect(),
    left = _ViewerDOM$getBoundin.left,
    top = _ViewerDOM$getBoundin.top;
  var x = event.clientX - Math.round(left);
  var y = event.clientY - Math.round(top);
  return {
    x: x,
    y: y
  };
}
function onMouseDown(event, ViewerDOM, tool, value, props) {
  var coords = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;
  var _ref = coords || getMousePosition(event, ViewerDOM),
    x = _ref.x,
    y = _ref.y;
  var nextValue = value;
  switch (tool) {
    case TOOL_ZOOM_OUT:
      var SVGPoint = getSVGPoint(value, x, y);
      nextValue = zoom(value, SVGPoint.x, SVGPoint.y, 1 / props.scaleFactor);
      break;
    case TOOL_ZOOM_IN:
      nextValue = startZooming(value, x, y);
      break;
    case TOOL_AUTO:
    case TOOL_PAN:
      nextValue = startPanning(value, x, y);
      break;
    default:
      return value;
  }
  event.preventDefault();
  return nextValue;
}
function onMouseMove(event, ViewerDOM, tool, value, props) {
  var coords = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;
  var _ref2 = coords || getMousePosition(event, ViewerDOM),
    x = _ref2.x,
    y = _ref2.y;
  var forceExit = event.buttons === 0; //the mouse exited and reentered into svg
  var nextValue = value;
  switch (tool) {
    case TOOL_ZOOM_IN:
      if (value.mode === MODE_ZOOMING) nextValue = forceExit ? stopZooming(value, x, y, props.scaleFactor) : updateZooming(value, x, y);
      break;
    case TOOL_AUTO:
    case TOOL_PAN:
      if (value.mode === MODE_PANNING) nextValue = forceExit ? stopPanning(value) : updatePanning(value, x, y, props.preventPanOutside ? 20 : undefined);
      break;
    default:
      return value;
  }
  event.preventDefault();
  return nextValue;
}
function onMouseUp(event, ViewerDOM, tool, value, props) {
  var coords = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;
  var _ref3 = coords || getMousePosition(event, ViewerDOM),
    x = _ref3.x,
    y = _ref3.y;
  var nextValue = value;
  switch (tool) {
    case TOOL_ZOOM_OUT:
      if (value.mode === MODE_ZOOMING) nextValue = stopZooming(value, x, y, 1 / props.scaleFactor);
      break;
    case TOOL_ZOOM_IN:
      if (value.mode === MODE_ZOOMING) nextValue = stopZooming(value, x, y, props.scaleFactor);
      break;
    case TOOL_AUTO:
    case TOOL_PAN:
      if (value.mode === MODE_PANNING) nextValue = stopPanning(value);
      break;
    default:
      return value;
  }
  event.preventDefault();
  return nextValue;
}
function onDoubleClick(event, ViewerDOM, tool, value, props) {
  var coords = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;
  var _ref4 = coords || getMousePosition(event, ViewerDOM),
    x = _ref4.x,
    y = _ref4.y;
  var nextValue = value;
  if (tool === TOOL_AUTO && !props.disableDoubleClickZoomWithToolAuto) {
    var _props$modifierKeys = props.modifierKeys,
      modifierKeys = _props$modifierKeys === void 0 ? [] : _props$modifierKeys;
    var SVGPoint = getSVGPoint(value, x, y);
    var modifierKeysReducer = function modifierKeysReducer(current, modifierKey) {
      return current || event.getModifierState(modifierKey);
    };
    var modifierKeyActive = modifierKeys.reduce(modifierKeysReducer, false);
    var scaleFactor = modifierKeyActive ? 1 / props.scaleFactor : props.scaleFactor;
    nextValue = zoom(value, SVGPoint.x, SVGPoint.y, scaleFactor);
  }
  event.preventDefault();
  return nextValue;
}
function onWheel(event, ViewerDOM, tool, value, props) {
  var coords = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;
  var _ref5 = coords || getMousePosition(event, ViewerDOM),
    x = _ref5.x,
    y = _ref5.y;
  if (!props.detectWheel) return value;
  var delta = Math.max(-1, Math.min(1, event.deltaY));
  var scaleFactor = mapRange(delta, -1, 1, props.scaleFactorOnWheel, 1 / props.scaleFactorOnWheel);
  var SVGPoint = getSVGPoint(value, x, y);
  var nextValue = zoom(value, SVGPoint.x, SVGPoint.y, scaleFactor);
  event.preventDefault();
  return nextValue;
}
function onMouseEnterOrLeave(event, ViewerDOM, tool, value, props) {
  var nextValue = setFocus(value, event.type === 'mouseenter');
  event.preventDefault();
  return nextValue;
}
function onInterval(event, ViewerDOM, tool, value, props) {
  var coords = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;
  var x = coords.x,
    y = coords.y;
  if (!([TOOL_NONE, TOOL_AUTO].indexOf(tool) >= 0)) return value;
  if (!props.detectAutoPan) return value;
  if (!value.focus) return value;
  return autoPanIfNeeded(value, x, y);
}

function _typeof$8(obj) { "@babel/helpers - typeof"; return _typeof$8 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$8(obj); }
function _classCallCheck$5(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties$5(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey$8(descriptor.key), descriptor); } }
function _createClass$5(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties$5(Constructor.prototype, protoProps); if (staticProps) _defineProperties$5(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey$8(arg) { var key = _toPrimitive$8(arg, "string"); return _typeof$8(key) === "symbol" ? key : String(key); }
function _toPrimitive$8(input, hint) { if (_typeof$8(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$8(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _inherits$5(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf$5(subClass, superClass); }
function _setPrototypeOf$5(o, p) { _setPrototypeOf$5 = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf$5(o, p); }
function _createSuper$5(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$5(); return function _createSuperInternal() { var Super = _getPrototypeOf$5(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf$5(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn$5(this, result); }; }
function _possibleConstructorReturn$5(self, call) { if (call && (_typeof$8(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized$5(self); }
function _assertThisInitialized$5(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct$5() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf$5(o) { _getPrototypeOf$5 = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf$5(o); }
var ViewerMouseEvent = /*#__PURE__*/function (_ViewerEvent) {
  _inherits$5(ViewerMouseEvent, _ViewerEvent);
  var _super = _createSuper$5(ViewerMouseEvent);
  function ViewerMouseEvent() {
    _classCallCheck$5(this, ViewerMouseEvent);
    return _super.apply(this, arguments);
  }
  _createClass$5(ViewerMouseEvent, [{
    key: "point",
    get: function get() {
      if (!this._cachePoint) {
        var event = this.originalEvent,
          SVGViewer = this.SVGViewer,
          value = this.value;
        var mousePosition = getMousePosition(event, SVGViewer);
        this._cachePoint = getSVGPoint(value, mousePosition.x, mousePosition.y);
      }
      return this._cachePoint;
    }
  }, {
    key: "x",
    get: function get() {
      return this.point.x;
    }
  }, {
    key: "y",
    get: function get() {
      return this.point.y;
    }
  }]);
  return ViewerMouseEvent;
}(ViewerEvent);

function _typeof$7(obj) { "@babel/helpers - typeof"; return _typeof$7 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$7(obj); }
function ownKeys$2(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread$2(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys$2(Object(source), !0).forEach(function (key) { _defineProperty$4(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys$2(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty$4(obj, key, value) { key = _toPropertyKey$7(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _classCallCheck$4(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties$4(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey$7(descriptor.key), descriptor); } }
function _createClass$4(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties$4(Constructor.prototype, protoProps); if (staticProps) _defineProperties$4(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey$7(arg) { var key = _toPrimitive$7(arg, "string"); return _typeof$7(key) === "symbol" ? key : String(key); }
function _toPrimitive$7(input, hint) { if (_typeof$7(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$7(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _inherits$4(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf$4(subClass, superClass); }
function _setPrototypeOf$4(o, p) { _setPrototypeOf$4 = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf$4(o, p); }
function _createSuper$4(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$4(); return function _createSuperInternal() { var Super = _getPrototypeOf$4(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf$4(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn$4(this, result); }; }
function _possibleConstructorReturn$4(self, call) { if (call && (_typeof$7(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized$4(self); }
function _assertThisInitialized$4(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct$4() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf$4(o) { _getPrototypeOf$4 = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf$4(o); }
var ViewerTouchEvent = /*#__PURE__*/function (_ViewerEvent) {
  _inherits$4(ViewerTouchEvent, _ViewerEvent);
  var _super = _createSuper$4(ViewerTouchEvent);
  function ViewerTouchEvent() {
    _classCallCheck$4(this, ViewerTouchEvent);
    return _super.apply(this, arguments);
  }
  _createClass$4(ViewerTouchEvent, [{
    key: "points",
    get: function get() {
      if (!this._cachePoints) this._cachePoints = ViewerTouchEvent.touchesToPoints(this.originalEvent.touches, this.SVGViewer, this.value);
      return this._cachePoints;
    }
  }, {
    key: "changedPoints",
    get: function get() {
      if (!this._cacheChangedPoints) this._cacheChangedPoints = ViewerTouchEvent.touchesToPoints(this.originalEvent.changedTouches, this.SVGViewer, this.value);
      return this._cacheChangedPoints;
    }
  }], [{
    key: "touchesToPoints",
    value: function touchesToPoints(touches, SVGViewer, value) {
      var points = [];
      for (var i = 0; i < touches.length; i++) {
        var touch = touches[i];
        var rect = SVGViewer.getBoundingClientRect();
        var x = touch.clientX - Math.round(rect.left);
        var y = touch.clientY - Math.round(rect.top);
        var point = getSVGPoint(value, x, y);
        points.push(_objectSpread$2(_objectSpread$2({}, point), {}, {
          identifier: touch.identifier
        }));
      }
      return points;
    }
  }]);
  return ViewerTouchEvent;
}(ViewerEvent);

function eventFactory(originalEvent, value, SVGViewer) {
  var eventType = originalEvent.type;
  switch (eventType) {
    case "mousemove":
    case "mouseup":
    case "mousedown":
    case "click":
    case "dblclick":
      return new ViewerMouseEvent(originalEvent, value, SVGViewer);
    case "touchstart":
    case "touchmove":
    case "touchend":
    case "touchcancel":
      return new ViewerTouchEvent(originalEvent, value, SVGViewer);
    default:
      throw new Error("".concat(eventType, " not supported"));
  }
}

function parseViewBox(viewBoxString) {
  // viewBox specs: https://www.w3.org/TR/SVG/coords.html#ViewBoxAttribute
  return viewBoxString.split(/[ ,]/) // split optional comma
  .filter(Boolean) // remove empty strings
  .map(Number); // cast to Number
}

function _typeof$6(obj) { "@babel/helpers - typeof"; return _typeof$6 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$6(obj); }
function ownKeys$1(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread$1(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys$1(Object(source), !0).forEach(function (key) { _defineProperty$3(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys$1(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty$3(obj, key, value) { key = _toPropertyKey$6(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey$6(arg) { var key = _toPrimitive$6(arg, "string"); return _typeof$6(key) === "symbol" ? key : String(key); }
function _toPrimitive$6(input, hint) { if (_typeof$6(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$6(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function getTouchPosition(touch, ViewerDOM) {
  var _ViewerDOM$getBoundin = ViewerDOM.getBoundingClientRect(),
    left = _ViewerDOM$getBoundin.left,
    top = _ViewerDOM$getBoundin.top;
  var x = touch.clientX - Math.round(left);
  var y = touch.clientY - Math.round(top);
  return {
    x: x,
    y: y
  };
}
function onTouchStart(event, ViewerDOM, tool, value, props) {
  if (isMultiTouch(event, props)) {
    return onMultiTouch(event, ViewerDOM, tool, value);
  }
  if (event.touches.length !== 1) {
    if ([MODE_PANNING, MODE_ZOOMING].indexOf(value.mode) >= 0) {
      return resetMode(value);
    } else if ([MODE_IDLE].indexOf(value.mode) >= 0) {
      return value;
    }
  }
  return onSingleTouch(event, ViewerDOM, tool, value, props, onMouseDown);
}
function onTouchMove(event, ViewerDOM, tool, value, props) {
  if (isMultiTouch(event, props)) {
    return onMultiTouch(event, ViewerDOM, tool, value);
  }
  if (!([MODE_PANNING, MODE_ZOOMING].indexOf(value.mode) >= 0)) {
    return value;
  }
  return onSingleTouch(event, ViewerDOM, tool, value, props, onMouseMove);
}
function onTouchEnd(event, ViewerDOM, tool, value, props) {
  if (!([MODE_PANNING, MODE_ZOOMING].indexOf(value.mode) >= 0)) {
    return value;
  }
  var nextValue = shouldResetPinchPointDistance(event, value, props) ? set(value, {
    pinchPointDistance: null
  }) : value;
  if (event.touches.length > 0) {
    return nextValue;
  }
  return onSingleTouch(event, ViewerDOM, tool, nextValue, props, onMouseUp);
}
function onTouchCancel(event, ViewerDOM, tool, value, props) {
  event.stopPropagation();
  event.preventDefault();
  return resetMode(value);
}
function hasPinchPointDistance(value) {
  return typeof value.pinchPointDistance === 'number';
}
function shouldResetPinchPointDistance(event, value, props) {
  return props.detectPinchGesture && hasPinchPointDistance(value) && event.touches.length < 2;
}
function isMultiTouch(event, props) {
  return props.detectPinchGesture && event.touches.length > 1;
}
function onSingleTouch(event, ViewerDOM, tool, value, props, nextValueFn) {
  var nextValue = event.touches.length === 0 ? set(value, {
    mode: value.prePinchMode ? MODE_IDLE : value.mode,
    prePinchMode: null
  }) : value;
  var touch = event.touches.length > 0 ? event.touches[0] : event.changedTouches[0];
  var touchPosition = getTouchPosition(touch, ViewerDOM);
  switch (tool) {
    case TOOL_ZOOM_OUT:
    case TOOL_ZOOM_IN:
    case TOOL_AUTO:
    case TOOL_PAN:
      event.stopPropagation();
      event.preventDefault();
      return nextValueFn(event, ViewerDOM, tool, nextValue, props, touchPosition);
    default:
      return nextValue;
  }
}
function onMultiTouch(event, ViewerDOM, tool, value, props) {
  var _ViewerDOM$getBoundin2 = ViewerDOM.getBoundingClientRect(),
    left = _ViewerDOM$getBoundin2.left,
    top = _ViewerDOM$getBoundin2.top;
  var x1 = event.touches[0].clientX - Math.round(left);
  var y1 = event.touches[0].clientY - Math.round(top);
  var x2 = event.touches[1].clientX - Math.round(left);
  var y2 = event.touches[1].clientY - Math.round(top);
  var pinchPointDistance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
  var previousPointDistance = hasPinchPointDistance(value) ? value.pinchPointDistance : pinchPointDistance;
  var svgPoint = getSVGPoint(value, (x1 + x2) / 2, (y1 + y2) / 2);
  var distanceFactor = pinchPointDistance / previousPointDistance;
  if (isZoomLevelGoingOutOfBounds(value, distanceFactor)) {
    // Do not change translation and scale of value
    return value;
  }
  if (event.cancelable) {
    event.preventDefault();
  }
  var matrix = transform(fromObject(value), translate(svgPoint.x, svgPoint.y), scale(distanceFactor, distanceFactor), translate(-svgPoint.x, -svgPoint.y));
  return set(value, set(_objectSpread$1(_objectSpread$1({
    mode: MODE_ZOOMING
  }, limitZoomLevel(value, matrix)), {}, {
    startX: null,
    startY: null,
    endX: null,
    endY: null,
    prePinchMode: value.prePinchMode ? value.prePinchMode : value.mode,
    pinchPointDistance: pinchPointDistance
  })));
}

function openMiniature(value) {
  return set(value, {
    miniatureOpen: true
  });
}
function closeMiniature(value) {
  return set(value, {
    miniatureOpen: false
  });
}

//specs: https://developer.mozilla.org/en-US/docs/Web/CSS/cursor

var needPrefix = function needPrefix(cursor) {
  return ['zoom-in', 'zoom-out', 'grab', 'grabbing'].indexOf(cursor) > -1;
};
var userAgent = function userAgent() {
  return navigator.userAgent.toLowerCase();
};
var isFirefox = function isFirefox() {
  return userAgent().indexOf('firefox') > -1;
};
var isWebkit = function isWebkit() {
  return userAgent().indexOf('webkit') > -1;
};
function cursorPolyfill (cursor) {
  if (!needPrefix(cursor)) return cursor;
  if (isFirefox()) return "-moz-".concat(cursor);
  if (isWebkit()) return "-webkit-".concat(cursor);
}

//https://facebook.github.io/react/docs/higher-order-components.html#convention-wrap-the-display-name-for-easy-debugging
function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

function _typeof$5(obj) { "@babel/helpers - typeof"; return _typeof$5 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$5(obj); }
function _extends$2() { _extends$2 = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$2.apply(this, arguments); }
function _classCallCheck$3(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties$3(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey$5(descriptor.key), descriptor); } }
function _createClass$3(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties$3(Constructor.prototype, protoProps); if (staticProps) _defineProperties$3(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey$5(arg) { var key = _toPrimitive$5(arg, "string"); return _typeof$5(key) === "symbol" ? key : String(key); }
function _toPrimitive$5(input, hint) { if (_typeof$5(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$5(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _inherits$3(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf$3(subClass, superClass); }
function _setPrototypeOf$3(o, p) { _setPrototypeOf$3 = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf$3(o, p); }
function _createSuper$3(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$3(); return function _createSuperInternal() { var Super = _getPrototypeOf$3(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf$3(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn$3(this, result); }; }
function _possibleConstructorReturn$3(self, call) { if (call && (_typeof$5(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized$3(self); }
function _assertThisInitialized$3(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct$3() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf$3(o) { _getPrototypeOf$3 = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf$3(o); }
var uid = 1;
var nextUID = function nextUID() {
  return "uid".concat(uid++);
};
function RandomUID(WrappedComponent) {
  var RandomUID = /*#__PURE__*/function (_React$Component) {
    _inherits$3(RandomUID, _React$Component);
    var _super = _createSuper$3(RandomUID);
    function RandomUID(props) {
      var _this;
      _classCallCheck$3(this, RandomUID);
      _this = _super.call(this, props);
      _this.state = {
        uid: nextUID()
      };
      return _this;
    }
    _createClass$3(RandomUID, [{
      key: "render",
      value: function render() {
        return /*#__PURE__*/React__default.createElement(WrappedComponent, _extends$2({
          _uid: this.state.uid
        }, this.props));
      }
    }]);
    return RandomUID;
  }(React__default.Component);
  RandomUID.displayName = "RandomUID(".concat(getDisplayName(WrappedComponent), ")");
  return RandomUID;
}

var prefixID$1 = 'react-svg-pan-zoom_border_gradient';
function BorderGradient(_ref) {
  var direction = _ref.direction,
    width = _ref.width,
    height = _ref.height,
    _uid = _ref._uid;
  var transform;
  switch (direction) {
    case POSITION_TOP:
      transform = "translate(".concat(width, ", 0) rotate(90)");
      break;
    case POSITION_RIGHT:
      transform = "translate(".concat(width, ", ").concat(height, ") rotate(180)");
      break;
    case POSITION_BOTTOM:
      transform = "translate(0, ".concat(height, ") rotate(270)");
      break;
    case POSITION_LEFT:
    default:
      transform = " ";
      break;
  }
  var gradientID = "".concat(prefixID$1, "_gradient_").concat(_uid);
  var maskID = "".concat(prefixID$1, "_mask_").concat(_uid);
  return /*#__PURE__*/React__default.createElement("g", null, /*#__PURE__*/React__default.createElement("defs", null, /*#__PURE__*/React__default.createElement("linearGradient", {
    id: gradientID,
    x1: "0%",
    y1: "0%",
    x2: "100%",
    y2: "0%",
    spreadMethod: "pad"
  }, /*#__PURE__*/React__default.createElement("stop", {
    offset: "0%",
    stopColor: "#fff",
    stopOpacity: "0.8"
  }), /*#__PURE__*/React__default.createElement("stop", {
    offset: "100%",
    stopColor: "#000",
    stopOpacity: "0.5"
  })), /*#__PURE__*/React__default.createElement("mask", {
    id: maskID,
    x: "0",
    y: "0",
    width: "20",
    height: Math.max(width, height)
  }, /*#__PURE__*/React__default.createElement("rect", {
    x: "0",
    y: "0",
    width: "20",
    height: Math.max(width, height),
    style: {
      stroke: "none",
      fill: "url(#".concat(gradientID, ")")
    }
  }))), /*#__PURE__*/React__default.createElement("rect", {
    x: "0",
    y: "0",
    width: "20",
    height: Math.max(width, height),
    style: {
      stroke: "none",
      fill: "#000",
      mask: "url(#".concat(maskID, ")")
    },
    transform: transform
  }));
}
BorderGradient.propTypes = {
  direction: propTypes.exports.oneOf([POSITION_TOP, POSITION_RIGHT, POSITION_BOTTOM, POSITION_LEFT]).isRequired,
  width: propTypes.exports.number.isRequired,
  height: propTypes.exports.number.isRequired
};
var BorderGradient$1 = RandomUID(BorderGradient);

function Selection(_ref) {
  var startX = _ref.startX,
    startY = _ref.startY,
    endX = _ref.endX,
    endY = _ref.endY;
  if (!startX || !startY || !endX || !endY) return null;
  var box = calculateBox({
    x: startX,
    y: startY
  }, {
    x: endX,
    y: endY
  });
  return /*#__PURE__*/React__default.createElement("rect", {
    stroke: "#969FFF",
    strokeOpacity: 0.7,
    fill: "#F3F4FF",
    fillOpacity: 0.7,
    x: box.x,
    y: box.y,
    width: box.width,
    height: box.height,
    style: {
      pointerEvents: "none"
    }
  });
}
Selection.propTypes = {
  startX: propTypes.exports.number,
  startY: propTypes.exports.number,
  endX: propTypes.exports.number,
  endY: propTypes.exports.number
};

//credits https://materialdesignicons.com/icon/cursor-default-outline

function IconCursor() {
  return /*#__PURE__*/React__default.createElement("svg", {
    width: 24,
    height: 24,
    stroke: "currentColor"
  }, /*#__PURE__*/React__default.createElement("path", {
    d: "M10.07,14.27C10.57,14.03 11.16,14.25 11.4,14.75L13.7,19.74L15.5,18.89L13.19,13.91C12.95,13.41 13.17,12.81 13.67,12.58L13.95,12.5L16.25,12.05L8,5.12V15.9L9.82,14.43L10.07,14.27M13.64,21.97C13.14,22.21 12.54,22 12.31,21.5L10.13,16.76L7.62,18.78C7.45,18.92 7.24,19 7,19A1,1 0 0,1 6,18V3A1,1 0 0,1 7,2C7.24,2 7.47,2.09 7.64,2.23L7.65,2.22L19.14,11.86C19.57,12.22 19.62,12.85 19.27,13.27C19.12,13.45 18.91,13.57 18.7,13.61L15.54,14.23L17.74,18.96C18,19.46 17.76,20.05 17.26,20.28L13.64,21.97Z"
  }));
}

//https://materialdesignicons.com/icon/cursor-move

function IconPan() {
  return /*#__PURE__*/React__default.createElement("svg", {
    width: 24,
    height: 24,
    stroke: "currentColor"
  }, /*#__PURE__*/React__default.createElement("path", {
    d: "M13,6V11H18V7.75L22.25,12L18,16.25V13H13V18H16.25L12,22.25L7.75,18H11V13H6V16.25L1.75,12L6,7.75V11H11V6H7.75L12,1.75L16.25,6H13Z"
  }));
}

//https://material.io/icons/#ic_zoom_in

function IconZoomIn() {
  return /*#__PURE__*/React__default.createElement("svg", {
    width: 24,
    height: 24,
    stroke: "currentColor"
  }, /*#__PURE__*/React__default.createElement("g", null, /*#__PURE__*/React__default.createElement("path", {
    d: "M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"
  }), /*#__PURE__*/React__default.createElement("path", {
    d: "M12 10h-2v2H9v-2H7V9h2V7h1v2h2v1z"
  })));
}

//https://material.io/icons/#ic_zoom_out

function IconZoomOut() {
  return /*#__PURE__*/React__default.createElement("svg", {
    width: 24,
    height: 24,
    stroke: "currentColor"
  }, /*#__PURE__*/React__default.createElement("path", {
    d: "M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14zM7 9h5v1H7z"
  }));
}

//credits https://materialdesignicons.com/icon/cursor-default-outline

function IconFit() {
  return /*#__PURE__*/React__default.createElement("svg", {
    width: 24,
    height: 24,
    stroke: "currentColor"
  }, /*#__PURE__*/React__default.createElement("path", {
    d: "M15 3l2.3 2.3-2.89 2.87 1.42 1.42L18.7 6.7 21 9V3zM3 9l2.3-2.3 2.87 2.89 1.42-1.42L6.7 5.3 9 3H3zm6 12l-2.3-2.3 2.89-2.87-1.42-1.42L5.3 17.3 3 15v6zm12-6l-2.3 2.3-2.87-2.89-1.42 1.42 2.89 2.87L15 21h6z"
  }));
}

function _typeof$4(obj) { "@babel/helpers - typeof"; return _typeof$4 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$4(obj); }
function _classCallCheck$2(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties$2(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey$4(descriptor.key), descriptor); } }
function _createClass$2(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties$2(Constructor.prototype, protoProps); if (staticProps) _defineProperties$2(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey$4(arg) { var key = _toPrimitive$4(arg, "string"); return _typeof$4(key) === "symbol" ? key : String(key); }
function _toPrimitive$4(input, hint) { if (_typeof$4(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$4(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _inherits$2(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf$2(subClass, superClass); }
function _setPrototypeOf$2(o, p) { _setPrototypeOf$2 = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf$2(o, p); }
function _createSuper$2(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$2(); return function _createSuperInternal() { var Super = _getPrototypeOf$2(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf$2(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn$2(this, result); }; }
function _possibleConstructorReturn$2(self, call) { if (call && (_typeof$4(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized$2(self); }
function _assertThisInitialized$2(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct$2() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf$2(o) { _getPrototypeOf$2 = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf$2(o); }
var ToolbarButton = /*#__PURE__*/function (_React$Component) {
  _inherits$2(ToolbarButton, _React$Component);
  var _super = _createSuper$2(ToolbarButton);
  function ToolbarButton(props) {
    var _this;
    _classCallCheck$2(this, ToolbarButton);
    _this = _super.call(this, props);
    _this.state = {
      hover: false
    };
    return _this;
  }
  _createClass$2(ToolbarButton, [{
    key: "change",
    value: function change(event) {
      event.preventDefault();
      event.stopPropagation();
      switch (event.type) {
        case 'mouseenter':
        case 'touchstart':
          this.setState({
            hover: true
          });
          break;
        case 'mouseleave':
        case 'touchend':
        case 'touchcancel':
          this.setState({
            hover: false
          });
          break;
        //noop
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;
      var style = {
        display: "block",
        width: "24px",
        height: "24px",
        margin: [POSITION_TOP, POSITION_BOTTOM].indexOf(this.props.toolbarPosition) >= 0 ? "2px 1px" : "1px 2px",
        color: this.props.active || this.state.hover ? this.props.activeColor : '#FFF',
        transition: "color 200ms ease",
        background: "none",
        padding: "0px",
        border: "0px",
        outline: "0px",
        cursor: "pointer"
      };
      return /*#__PURE__*/React__default.createElement("button", {
        onMouseEnter: function onMouseEnter(e) {
          return _this2.change(e);
        },
        onMouseLeave: function onMouseLeave(e) {
          return _this2.change(e);
        },
        onTouchStart: function onTouchStart(e) {
          _this2.change(e);
          _this2.props.onClick(e);
        },
        onTouchEnd: function onTouchEnd(e) {
          return _this2.change(e);
        },
        onTouchCancel: function onTouchCancel(e) {
          return _this2.change(e);
        },
        onClick: this.props.onClick,
        style: style,
        title: this.props.title,
        name: this.props.name,
        type: "button"
      }, this.props.children);
    }
  }]);
  return ToolbarButton;
}(React__default.Component);
ToolbarButton.propTypes = {
  title: propTypes.exports.string.isRequired,
  name: propTypes.exports.string.isRequired,
  toolbarPosition: propTypes.exports.string.isRequired,
  activeColor: propTypes.exports.string.isRequired,
  onClick: propTypes.exports.func.isRequired,
  active: propTypes.exports.bool.isRequired
};

function Toolbar(_ref) {
  var tool = _ref.tool,
    value = _ref.value,
    onChangeValue = _ref.onChangeValue,
    onChangeTool = _ref.onChangeTool,
    activeToolColor = _ref.activeToolColor,
    position = _ref.position,
    SVGAlignX = _ref.SVGAlignX,
    SVGAlignY = _ref.SVGAlignY;
  var handleChangeTool = function handleChangeTool(event, tool) {
    onChangeTool(tool);
    event.stopPropagation();
    event.preventDefault();
  };
  var handleFit = function handleFit(event) {
    onChangeValue(fitToViewer(value, SVGAlignX, SVGAlignY));
    event.stopPropagation();
    event.preventDefault();
  };
  var isHorizontal = [POSITION_TOP, POSITION_BOTTOM].indexOf(position) >= 0;
  var style = {
    //position
    position: "absolute",
    transform: [POSITION_TOP, POSITION_BOTTOM].indexOf(position) >= 0 ? "translate(-50%, 0px)" : "none",
    top: [POSITION_LEFT, POSITION_RIGHT, POSITION_TOP].indexOf(position) >= 0 ? "5px" : "unset",
    left: [POSITION_TOP, POSITION_BOTTOM].indexOf(position) >= 0 ? "50%" : POSITION_LEFT === position ? "5px" : "unset",
    right: [POSITION_RIGHT].indexOf(position) >= 0 ? "5px" : "unset",
    bottom: [POSITION_BOTTOM].indexOf(position) >= 0 ? "5px" : "unset",
    //inner styling
    backgroundColor: "rgba(19, 20, 22, 0.90)",
    borderRadius: "2px",
    display: "flex",
    flexDirection: isHorizontal ? "row" : "column",
    padding: isHorizontal ? "1px 2px" : "2px 1px"
  };
  return /*#__PURE__*/React__default.createElement("div", {
    style: style,
    role: "toolbar"
  }, /*#__PURE__*/React__default.createElement(ToolbarButton, {
    toolbarPosition: position,
    active: tool === TOOL_NONE,
    activeColor: activeToolColor,
    name: "unselect-tools",
    title: "Selection",
    onClick: function onClick(event) {
      return handleChangeTool(event, TOOL_NONE);
    }
  }, /*#__PURE__*/React__default.createElement(IconCursor, null)), /*#__PURE__*/React__default.createElement(ToolbarButton, {
    toolbarPosition: position,
    active: tool === TOOL_PAN,
    activeColor: activeToolColor,
    name: "select-tool-pan",
    title: "Pan",
    onClick: function onClick(event) {
      return handleChangeTool(event, TOOL_PAN);
    }
  }, /*#__PURE__*/React__default.createElement(IconPan, null)), /*#__PURE__*/React__default.createElement(ToolbarButton, {
    toolbarPosition: position,
    active: tool === TOOL_ZOOM_IN,
    activeColor: activeToolColor,
    name: "select-tool-zoom-in",
    title: "Zoom in",
    onClick: function onClick(event) {
      return handleChangeTool(event, TOOL_ZOOM_IN);
    }
  }, /*#__PURE__*/React__default.createElement(IconZoomIn, null)), /*#__PURE__*/React__default.createElement(ToolbarButton, {
    toolbarPosition: position,
    active: tool === TOOL_ZOOM_OUT,
    activeColor: activeToolColor,
    name: "select-tool-zoom-out",
    title: "Zoom out",
    onClick: function onClick(event) {
      return handleChangeTool(event, TOOL_ZOOM_OUT);
    }
  }, /*#__PURE__*/React__default.createElement(IconZoomOut, null)), /*#__PURE__*/React__default.createElement(ToolbarButton, {
    toolbarPosition: position,
    active: false,
    activeColor: activeToolColor,
    name: "fit-to-viewer",
    title: "Fit to viewer",
    onClick: function onClick(event) {
      return handleFit(event);
    }
  }, /*#__PURE__*/React__default.createElement(IconFit, null)));
}
Toolbar.propTypes = {
  tool: propTypes.exports.string.isRequired,
  onChangeTool: propTypes.exports.func.isRequired,
  value: propTypes.exports.object.isRequired,
  onChangeValue: propTypes.exports.func.isRequired,
  //customizations
  position: propTypes.exports.oneOf([POSITION_TOP, POSITION_RIGHT, POSITION_BOTTOM, POSITION_LEFT]),
  SVGAlignX: propTypes.exports.oneOf([ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT]),
  SVGAlignY: propTypes.exports.oneOf([ALIGN_CENTER, ALIGN_TOP, ALIGN_BOTTOM]),
  activeToolColor: propTypes.exports.string
};
Toolbar.defaultProps = {
  position: POSITION_RIGHT,
  SVGAlignX: ALIGN_LEFT,
  SVGAlignY: ALIGN_TOP,
  activeToolColor: '#1CA6FC'
};

//http://stackoverflow.com/a/4819886/1398836

function isTouchDevice() {
  return 'ontouchstart' in window // works on most browsers
  || navigator.maxTouchPoints; // works on IE10/11 and Surface
}

//credits https://materialdesignicons.com/icon/chevron-up

function IconArrow(_ref) {
  var open = _ref.open,
    position = _ref.position;
  var transform = 0;
  switch (position) {
    case POSITION_LEFT:
      transform = open ? "rotate(225, 12, 13)" : "rotate(45, 12, 13)";
      break;
    case POSITION_RIGHT:
      transform = transform = open ? "rotate(135, 12, 13)" : "rotate(-45, 12, 13)";
      break;
    //no op
  }

  return /*#__PURE__*/React__default.createElement("svg", {
    width: 24,
    height: 24,
    stroke: "currentColor"
  }, /*#__PURE__*/React__default.createElement("g", {
    transform: transform
  }, /*#__PURE__*/React__default.createElement("path", {
    fill: "#000000",
    d: "M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z"
  })));
}
IconArrow.propTypes = {
  open: propTypes.exports.bool.isRequired,
  position: propTypes.exports.oneOf([POSITION_RIGHT, POSITION_LEFT]).isRequired
};

function _typeof$3(obj) { "@babel/helpers - typeof"; return _typeof$3 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$3(obj); }
function _defineProperty$2(obj, key, value) { key = _toPropertyKey$3(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey$3(arg) { var key = _toPrimitive$3(arg, "string"); return _typeof$3(key) === "symbol" ? key : String(key); }
function _toPrimitive$3(input, hint) { if (_typeof$3(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$3(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function MiniatureToggleButton(_ref) {
  var _style;
  var value = _ref.value,
    onChangeValue = _ref.onChangeValue,
    position = _ref.position;
  var style = (_style = {
    width: "24px",
    height: "24px",
    display: "block",
    position: "absolute",
    bottom: 0
  }, _defineProperty$2(_style, position === POSITION_LEFT ? 'left' : 'right', '0px'), _defineProperty$2(_style, "background", "rgba(19, 20, 22, 0.901961)"), _defineProperty$2(_style, "border", 0), _defineProperty$2(_style, "padding", 0), _defineProperty$2(_style, "outline", 0), _defineProperty$2(_style, "color", "#fff"), _style);
  var action = value.miniatureOpen ? closeMiniature : openMiniature;
  return /*#__PURE__*/React__default.createElement("button", {
    type: "button",
    style: style,
    onClick: function onClick() {
      return onChangeValue(action(value));
    }
  }, /*#__PURE__*/React__default.createElement(IconArrow, {
    open: value.miniatureOpen,
    position: position
  }));
}
MiniatureToggleButton.propTypes = {
  value: propTypes.exports.object.isRequired,
  onChangeValue: propTypes.exports.func.isRequired,
  position: propTypes.exports.oneOf([POSITION_RIGHT, POSITION_LEFT]).isRequired
};

var prefixID = 'react-svg-pan-zoom_miniature';
function MiniatureMask(_ref) {
  var SVGMinX = _ref.SVGMinX,
    SVGMinY = _ref.SVGMinY,
    SVGWidth = _ref.SVGWidth,
    SVGHeight = _ref.SVGHeight,
    x1 = _ref.x1,
    y1 = _ref.y1,
    x2 = _ref.x2,
    y2 = _ref.y2;
    _ref.zoomToFit;
    var _uid = _ref._uid;
  var maskID = "".concat(prefixID, "_mask_").concat(_uid);
  return /*#__PURE__*/React__default.createElement("g", null, /*#__PURE__*/React__default.createElement("defs", null, /*#__PURE__*/React__default.createElement("mask", {
    id: maskID
  }, /*#__PURE__*/React__default.createElement("rect", {
    x: SVGMinX,
    y: SVGMinY,
    width: SVGWidth,
    height: SVGHeight,
    fill: "#ffffff"
  }), /*#__PURE__*/React__default.createElement("rect", {
    x: x1,
    y: y1,
    width: x2 - x1,
    height: y2 - y1
  }))), /*#__PURE__*/React__default.createElement("rect", {
    x: SVGMinX,
    y: SVGMinY,
    width: SVGWidth,
    height: SVGHeight,
    style: {
      stroke: "none",
      fill: "#000",
      mask: "url(#".concat(maskID, ")"),
      opacity: 0.4
    }
  }));
}
MiniatureMask.propTypes = {
  SVGWidth: propTypes.exports.number.isRequired,
  SVGHeight: propTypes.exports.number.isRequired,
  SVGMinX: propTypes.exports.number.isRequired,
  SVGMinY: propTypes.exports.number.isRequired,
  x1: propTypes.exports.number.isRequired,
  y1: propTypes.exports.number.isRequired,
  x2: propTypes.exports.number.isRequired,
  y2: propTypes.exports.number.isRequired,
  zoomToFit: propTypes.exports.number.isRequired
};
var MiniatureMask$1 = RandomUID(MiniatureMask);

function _typeof$2(obj) { "@babel/helpers - typeof"; return _typeof$2 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$2(obj); }
function _defineProperty$1(obj, key, value) { key = _toPropertyKey$2(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey$2(arg) { var key = _toPrimitive$2(arg, "string"); return _typeof$2(key) === "symbol" ? key : String(key); }
function _toPrimitive$2(input, hint) { if (_typeof$2(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$2(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _slicedToArray$1(arr, i) { return _arrayWithHoles$1(arr) || _iterableToArrayLimit$1(arr, i) || _unsupportedIterableToArray$1(arr, i) || _nonIterableRest$1(); }
function _nonIterableRest$1() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray$1(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray$1(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$1(o, minLen); }
function _arrayLikeToArray$1(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit$1(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles$1(arr) { if (Array.isArray(arr)) return arr; }
function Miniature(props) {
  var _style;
  var value = props.value,
    onChangeValue = props.onChangeValue,
    children = props.children,
    position = props.position,
    background = props.background,
    SVGBackground = props.SVGBackground,
    miniatureWidth = props.width,
    miniatureHeight = props.height;
  var SVGMinX = value.SVGMinX,
    SVGMinY = value.SVGMinY,
    SVGWidth = value.SVGWidth,
    SVGHeight = value.SVGHeight,
    viewerWidth = value.viewerWidth,
    viewerHeight = value.viewerHeight;
  var ratio = SVGHeight / SVGWidth;
  var zoomToFit = ratio >= 1 ? miniatureHeight / SVGHeight : miniatureWidth / SVGWidth;
  var _applyToPoints = applyToPoints(inverse(value), [{
      x: 0,
      y: 0
    }, {
      x: viewerWidth,
      y: viewerHeight
    }]),
    _applyToPoints2 = _slicedToArray$1(_applyToPoints, 2),
    _applyToPoints2$ = _applyToPoints2[0],
    x1 = _applyToPoints2$.x,
    y1 = _applyToPoints2$.y,
    _applyToPoints2$2 = _applyToPoints2[1],
    x2 = _applyToPoints2$2.x,
    y2 = _applyToPoints2$2.y;
  var width, height;
  if (value.miniatureOpen) {
    width = miniatureWidth;
    height = miniatureHeight;
  } else {
    width = 24;
    height = 24;
  }
  var style = (_style = {
    position: "absolute",
    overflow: "hidden",
    outline: "1px solid rgba(19, 20, 22, 0.90)",
    transition: "width 200ms ease, height 200ms ease, bottom 200ms ease",
    width: width + "px",
    height: height + "px",
    bottom: "6px"
  }, _defineProperty$1(_style, position === POSITION_LEFT ? 'left' : 'right', "6px"), _defineProperty$1(_style, "background", background), _style);
  var centerTranslation = ratio >= 1 ? "translate(".concat((miniatureWidth - SVGWidth * zoomToFit) / 2 - SVGMinX * zoomToFit, ", ").concat(-SVGMinY * zoomToFit, ")") : "translate(".concat(-SVGMinX * zoomToFit, ", ").concat((miniatureHeight - SVGHeight * zoomToFit) / 2 - SVGMinY * zoomToFit, ")");
  return /*#__PURE__*/React__default.createElement("div", {
    role: "navigation",
    style: style
  }, /*#__PURE__*/React__default.createElement("svg", {
    width: miniatureWidth,
    height: miniatureHeight,
    style: {
      pointerEvents: "none"
    }
  }, /*#__PURE__*/React__default.createElement("g", {
    transform: centerTranslation
  }, /*#__PURE__*/React__default.createElement("g", {
    transform: "scale(".concat(zoomToFit, ", ").concat(zoomToFit, ")")
  }, /*#__PURE__*/React__default.createElement("rect", {
    fill: SVGBackground,
    x: SVGMinX,
    y: SVGMinY,
    width: SVGWidth,
    height: SVGHeight
  }), children, /*#__PURE__*/React__default.createElement(MiniatureMask$1, {
    SVGWidth: SVGWidth,
    SVGHeight: SVGHeight,
    SVGMinX: SVGMinX,
    SVGMinY: SVGMinY,
    x1: x1,
    y1: y1,
    x2: x2,
    y2: y2,
    zoomToFit: zoomToFit
  })))), /*#__PURE__*/React__default.createElement(MiniatureToggleButton, {
    value: value,
    onChangeValue: onChangeValue,
    position: position
  }));
}
Miniature.propTypes = {
  value: propTypes.exports.object.isRequired,
  onChangeValue: propTypes.exports.func.isRequired,
  SVGBackground: propTypes.exports.string.isRequired,
  //customizations
  position: propTypes.exports.oneOf([POSITION_RIGHT, POSITION_LEFT]),
  background: propTypes.exports.string.isRequired,
  width: propTypes.exports.number.isRequired,
  height: propTypes.exports.number.isRequired
};
Miniature.defaultProps = {
  position: POSITION_LEFT,
  background: "#616264",
  width: 100,
  height: 80
};

function _typeof$1(obj) { "@babel/helpers - typeof"; return _typeof$1 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof$1(obj); }
function _extends$1() { _extends$1 = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$1.apply(this, arguments); }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey$1(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i.return && (_r = _i.return(), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function _classCallCheck$1(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties$1(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey$1(descriptor.key), descriptor); } }
function _createClass$1(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties$1(Constructor.prototype, protoProps); if (staticProps) _defineProperties$1(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey$1(arg) { var key = _toPrimitive$1(arg, "string"); return _typeof$1(key) === "symbol" ? key : String(key); }
function _toPrimitive$1(input, hint) { if (_typeof$1(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof$1(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _inherits$1(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf$1(subClass, superClass); }
function _setPrototypeOf$1(o, p) { _setPrototypeOf$1 = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf$1(o, p); }
function _createSuper$1(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$1(); return function _createSuperInternal() { var Super = _getPrototypeOf$1(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf$1(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn$1(this, result); }; }
function _possibleConstructorReturn$1(self, call) { if (call && (_typeof$1(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized$1(self); }
function _assertThisInitialized$1(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct$1() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf$1(o) { _getPrototypeOf$1 = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf$1(o); }
var ReactSVGPanZoom = /*#__PURE__*/function (_React$Component) {
  _inherits$1(ReactSVGPanZoom, _React$Component);
  var _super = _createSuper$1(ReactSVGPanZoom);
  function ReactSVGPanZoom(props, context) {
    var _this;
    _classCallCheck$1(this, ReactSVGPanZoom);
    var viewerWidth = props.width,
      viewerHeight = props.height,
      scaleFactorMin = props.scaleFactorMin,
      scaleFactorMax = props.scaleFactorMax,
      children = props.children;
    var SVGViewBox = children.props.viewBox;
    var defaultValue;
    if (SVGViewBox) {
      var _parseViewBox = parseViewBox(SVGViewBox),
        _parseViewBox2 = _slicedToArray(_parseViewBox, 4),
        SVGMinX = _parseViewBox2[0],
        SVGMinY = _parseViewBox2[1],
        SVGWidth = _parseViewBox2[2],
        SVGHeight = _parseViewBox2[3];
      defaultValue = getDefaultValue(viewerWidth, viewerHeight, SVGMinX, SVGMinY, SVGWidth, SVGHeight, scaleFactorMin, scaleFactorMax);
    } else {
      var _children$props = children.props,
        _SVGWidth = _children$props.width,
        _SVGHeight = _children$props.height;
      defaultValue = getDefaultValue(viewerWidth, viewerHeight, 0, 0, _SVGWidth, _SVGHeight, scaleFactorMin, scaleFactorMax);
    }
    _this = _super.call(this, props, context);
    _this.ViewerDOM = null;
    _this.state = {
      pointerX: null,
      pointerY: null,
      defaultValue: defaultValue
    };
    _this.autoPanLoop = _this.autoPanLoop.bind(_assertThisInitialized$1(_this));
    _this.onWheel = _this.onWheel.bind(_assertThisInitialized$1(_this));
    return _this;
  }

  /** React hooks **/
  _createClass$1(ReactSVGPanZoom, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var value = this.getValue();
      var props = this.props;
      var nextValue = value;
      var needUpdate = false;

      // This block checks the size of the SVG
      var SVGViewBox = props.children.props.viewBox;
      if (SVGViewBox) {
        // if the viewBox prop is specified
        var _parseViewBox3 = parseViewBox(SVGViewBox),
          _parseViewBox4 = _slicedToArray(_parseViewBox3, 4),
          x = _parseViewBox4[0],
          y = _parseViewBox4[1],
          width = _parseViewBox4[2],
          height = _parseViewBox4[3];
        if (value.SVGMinX !== x || value.SVGMinY !== y || value.SVGWidth !== width || value.SVGHeight !== height) {
          nextValue = setSVGViewBox(nextValue, x, y, width, height);
          needUpdate = true;
        }
      } else {
        // if the width and height props are specified
        var _props$children$props = props.children.props,
          SVGWidth = _props$children$props.width,
          SVGHeight = _props$children$props.height;
        if (value.SVGWidth !== SVGWidth || value.SVGHeight !== SVGHeight) {
          nextValue = setSVGViewBox(nextValue, 0, 0, SVGWidth, SVGHeight);
          needUpdate = true;
        }
      }

      // This block checks the size of the viewer
      if (prevProps.width !== props.width || prevProps.height !== props.height) {
        nextValue = setViewerSize(nextValue, props.width, props.height);
        needUpdate = true;
      }

      // This blocks checks the scale factors
      if (prevProps.scaleFactorMin !== props.scaleFactorMin || prevProps.scaleFactorMax !== props.scaleFactorMax) {
        nextValue = setZoomLevels(nextValue, props.scaleFactorMin, props.scaleFactorMax);
        needUpdate = true;
      }
      if (needUpdate) {
        this.setValue(nextValue);
      }
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.autoPanIsRunning = true;
      requestAnimationFrame(this.autoPanLoop);
      this.ViewerDOM.addEventListener('wheel', this.onWheel, false);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.autoPanIsRunning = false;
      this.ViewerDOM.removeEventListener('wheel', this.onWheel);
    }

    /** ReactSVGPanZoom handlers **/
  }, {
    key: "getValue",
    value: function getValue() {
      if (isValueValid(this.props.value)) return this.props.value;
      return this.state.defaultValue;
    }
  }, {
    key: "getTool",
    value: function getTool() {
      if (this.props.tool) return this.props.tool;
      return TOOL_NONE;
    }
  }, {
    key: "setValue",
    value: function setValue(nextValue) {
      var _this$props = this.props,
        onChangeValue = _this$props.onChangeValue,
        onZoom = _this$props.onZoom,
        onPan = _this$props.onPan;
      if (onChangeValue) onChangeValue(nextValue);
      if (nextValue.lastAction) {
        if (onZoom && nextValue.lastAction === ACTION_ZOOM) onZoom(nextValue);
        if (onPan && nextValue.lastAction === ACTION_PAN) onPan(nextValue);
      }
    }

    /** ReactSVGPanZoom methods **/
  }, {
    key: "pan",
    value: function pan$1(SVGDeltaX, SVGDeltaY) {
      var nextValue = pan(this.getValue(), SVGDeltaX, SVGDeltaY);
      this.setValue(nextValue);
    }
  }, {
    key: "zoom",
    value: function zoom$1(SVGPointX, SVGPointY, scaleFactor) {
      var nextValue = zoom(this.getValue(), SVGPointX, SVGPointY, scaleFactor);
      this.setValue(nextValue);
    }
  }, {
    key: "fitSelection",
    value: function fitSelection$1(selectionSVGPointX, selectionSVGPointY, selectionWidth, selectionHeight) {
      var nextValue = fitSelection(this.getValue(), selectionSVGPointX, selectionSVGPointY, selectionWidth, selectionHeight);
      this.setValue(nextValue);
    }
  }, {
    key: "fitToViewer",
    value: function fitToViewer$1() {
      var SVGAlignX = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ALIGN_LEFT;
      var SVGAlignY = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ALIGN_TOP;
      var nextValue = fitToViewer(this.getValue(), SVGAlignX, SVGAlignY);
      this.setValue(nextValue);
    }
  }, {
    key: "zoomOnViewerCenter",
    value: function zoomOnViewerCenter$1(scaleFactor) {
      var nextValue = zoomOnViewerCenter(this.getValue(), scaleFactor);
      this.setValue(nextValue);
    }
  }, {
    key: "setPointOnViewerCenter",
    value: function setPointOnViewerCenter$1(SVGPointX, SVGPointY, zoomLevel) {
      var nextValue = setPointOnViewerCenter(this.getValue(), SVGPointX, SVGPointY, zoomLevel);
      this.setValue(nextValue);
    }
  }, {
    key: "reset",
    value: function reset$1() {
      var nextValue = reset(this.getValue());
      this.setValue(nextValue);
    }
  }, {
    key: "openMiniature",
    value: function openMiniature$1() {
      var nextValue = openMiniature(this.getValue());
      this.setValue(nextValue);
    }
  }, {
    key: "closeMiniature",
    value: function closeMiniature$1() {
      var nextValue = closeMiniature(this.getValue());
      this.setValue(nextValue);
    }

    /** ReactSVGPanZoom internals **/
  }, {
    key: "handleViewerEvent",
    value: function handleViewerEvent(event) {
      var props = this.props,
        ViewerDOM = this.ViewerDOM;
      if (!([TOOL_NONE, TOOL_AUTO].indexOf(this.getTool()) >= 0)) return;
      if (event.target === ViewerDOM) return;
      var eventsHandler = {
        click: props.onClick,
        dblclick: props.onDoubleClick,
        mousemove: props.onMouseMove,
        mouseup: props.onMouseUp,
        mousedown: props.onMouseDown,
        touchstart: props.onTouchStart,
        touchmove: props.onTouchMove,
        touchend: props.onTouchEnd,
        touchcancel: props.onTouchCancel
      };
      var onEventHandler = eventsHandler[event.type];
      if (!onEventHandler) return;
      onEventHandler(eventFactory(event, props.value, ViewerDOM));
    }
  }, {
    key: "autoPanLoop",
    value: function autoPanLoop() {
      var coords = {
        x: this.state.pointerX,
        y: this.state.pointerY
      };
      var nextValue = onInterval(null, this.ViewerDOM, this.getTool(), this.getValue(), this.props, coords);
      if (this.getValue() !== nextValue) {
        this.setValue(nextValue);
      }
      if (this.autoPanIsRunning) {
        requestAnimationFrame(this.autoPanLoop);
      }
    }
  }, {
    key: "onWheel",
    value: function onWheel$1(event) {
      var nextValue = onWheel(event, this.ViewerDOM, this.getTool(), this.getValue(), this.props);
      if (this.getValue() !== nextValue) this.setValue(nextValue);
    }

    /** React renderer **/
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;
      var props = this.props,
        _this$state = this.state,
        pointerX = _this$state.pointerX,
        pointerY = _this$state.pointerY;
      var tool = this.getTool();
      var value = this.getValue();
      var _props$customToolbar = props.customToolbar,
        CustomToolbar = _props$customToolbar === void 0 ? Toolbar : _props$customToolbar,
        _props$customMiniatur = props.customMiniature,
        CustomMiniature = _props$customMiniatur === void 0 ? Miniature : _props$customMiniatur;
      var panningWithToolAuto = tool === TOOL_AUTO && value.mode === MODE_PANNING && value.startX !== value.endX && value.startY !== value.endY;
      var cursor;
      if (tool === TOOL_PAN) cursor = cursorPolyfill(value.mode === MODE_PANNING ? 'grabbing' : 'grab');
      if (tool === TOOL_ZOOM_IN) cursor = cursorPolyfill('zoom-in');
      if (tool === TOOL_ZOOM_OUT) cursor = cursorPolyfill('zoom-out');
      if (panningWithToolAuto) cursor = cursorPolyfill('grabbing');
      var blockChildEvents = [TOOL_PAN, TOOL_ZOOM_IN, TOOL_ZOOM_OUT].indexOf(tool) >= 0;
      blockChildEvents = blockChildEvents || panningWithToolAuto;
      var touchAction = this.props.detectPinchGesture || [TOOL_PAN, TOOL_AUTO].indexOf(this.getTool()) !== -1 ? 'none' : undefined;
      var style = {
        display: 'block',
        cursor: cursor,
        touchAction: touchAction
      };
      return /*#__PURE__*/React__default.createElement("div", {
        style: _objectSpread({
          position: "relative",
          width: value.viewerWidth,
          height: value.viewerHeight
        }, props.style),
        className: this.props.className
      }, /*#__PURE__*/React__default.createElement("svg", {
        ref: function ref(ViewerDOM) {
          return _this2.ViewerDOM = ViewerDOM;
        },
        width: value.viewerWidth,
        height: value.viewerHeight,
        style: style,
        onMouseDown: function onMouseDown$1(event) {
          var nextValue = onMouseDown(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        },
        onMouseMove: function onMouseMove$1(event) {
          var _this2$ViewerDOM$getB = _this2.ViewerDOM.getBoundingClientRect(),
            left = _this2$ViewerDOM$getB.left,
            top = _this2$ViewerDOM$getB.top;
          var x = event.clientX - Math.round(left);
          var y = event.clientY - Math.round(top);
          var nextValue = onMouseMove(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props, {
            x: x,
            y: y
          });
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.setState({
            pointerX: x,
            pointerY: y
          });
          _this2.handleViewerEvent(event);
        },
        onMouseUp: function onMouseUp$1(event) {
          var nextValue = onMouseUp(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        },
        onClick: function onClick(event) {
          _this2.handleViewerEvent(event);
        },
        onDoubleClick: function onDoubleClick$1(event) {
          var nextValue = onDoubleClick(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        },
        onMouseEnter: function onMouseEnter(event) {
          if (isTouchDevice()) return;
          var nextValue = onMouseEnterOrLeave(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
        },
        onMouseLeave: function onMouseLeave(event) {
          var nextValue = onMouseEnterOrLeave(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
        },
        onTouchStart: function onTouchStart$1(event) {
          var nextValue = onTouchStart(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        },
        onTouchMove: function onTouchMove$1(event) {
          var nextValue = onTouchMove(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        },
        onTouchEnd: function onTouchEnd$1(event) {
          var nextValue = onTouchEnd(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        },
        onTouchCancel: function onTouchCancel$1(event) {
          var nextValue = onTouchCancel(event, _this2.ViewerDOM, _this2.getTool(), _this2.getValue(), _this2.props);
          if (_this2.getValue() !== nextValue) _this2.setValue(nextValue);
          _this2.handleViewerEvent(event);
        }
      }, /*#__PURE__*/React__default.createElement("rect", {
        fill: props.background,
        x: 0,
        y: 0,
        width: value.viewerWidth,
        height: value.viewerHeight,
        style: {
          pointerEvents: "none"
        }
      }), /*#__PURE__*/React__default.createElement("g", {
        transform: toSVG(value),
        style: blockChildEvents ? {
          pointerEvents: "none"
        } : {}
      }, /*#__PURE__*/React__default.createElement("rect", {
        fill: this.props.SVGBackground,
        style: this.props.SVGStyle,
        x: value.SVGMinX || 0,
        y: value.SVGMinY || 0,
        width: value.SVGWidth,
        height: value.SVGHeight
      }), /*#__PURE__*/React__default.createElement("g", null, props.children.props.children)), !([TOOL_NONE, TOOL_AUTO].indexOf(tool) >= 0 && props.detectAutoPan && value.focus) ? null : /*#__PURE__*/React__default.createElement("g", {
        style: {
          pointerEvents: "none"
        }
      }, !(pointerY <= 20) ? null : /*#__PURE__*/React__default.createElement(BorderGradient$1, {
        direction: POSITION_TOP,
        width: value.viewerWidth,
        height: value.viewerHeight
      }), !(value.viewerWidth - pointerX <= 20) ? null : /*#__PURE__*/React__default.createElement(BorderGradient$1, {
        direction: POSITION_RIGHT,
        width: value.viewerWidth,
        height: value.viewerHeight
      }), !(value.viewerHeight - pointerY <= 20) ? null : /*#__PURE__*/React__default.createElement(BorderGradient$1, {
        direction: POSITION_BOTTOM,
        width: value.viewerWidth,
        height: value.viewerHeight
      }), !(value.focus && pointerX <= 20) ? null : /*#__PURE__*/React__default.createElement(BorderGradient$1, {
        direction: POSITION_LEFT,
        width: value.viewerWidth,
        height: value.viewerHeight
      })), !(value.mode === MODE_ZOOMING) ? null : /*#__PURE__*/React__default.createElement(Selection, {
        startX: value.startX,
        startY: value.startY,
        endX: value.endX,
        endY: value.endY
      })), props.toolbarProps.position === POSITION_NONE ? null : /*#__PURE__*/React__default.createElement(CustomToolbar, _extends$1({}, this.props.toolbarProps, {
        value: value,
        onChangeValue: function onChangeValue(value) {
          return _this2.setValue(value);
        },
        tool: tool,
        onChangeTool: function onChangeTool(tool) {
          return _this2.props.onChangeTool(tool);
        }
      })), props.miniatureProps.position === POSITION_NONE ? null : /*#__PURE__*/React__default.createElement(CustomMiniature, _extends$1({}, this.props.miniatureProps, {
        value: value,
        onChangeValue: function onChangeValue(value) {
          return _this2.setValue(value);
        },
        SVGBackground: this.props.SVGBackground
      }), props.children.props.children));
    }
  }]);
  return ReactSVGPanZoom;
}(React__default.Component);
ReactSVGPanZoom.propTypes = {
  /**************************************************************************/
  /*  Viewer configuration                                                  */
  /**************************************************************************/

  /**
   *   width of the viewer displayed on screen
   */
  width: propTypes.exports.number.isRequired,
  /**
  * height of the viewer displayed on screen
  */
  height: propTypes.exports.number.isRequired,
  /**
  * value of the viewer (current camera view)
  */
  value: propTypes.exports.oneOfType([propTypes.exports.object, propTypes.exports.shape({
    version: propTypes.exports.oneOf([2]).isRequired,
    mode: propTypes.exports.oneOf([MODE_IDLE, MODE_PANNING, MODE_ZOOMING]).isRequired,
    focus: propTypes.exports.bool.isRequired,
    a: propTypes.exports.number.isRequired,
    b: propTypes.exports.number.isRequired,
    c: propTypes.exports.number.isRequired,
    d: propTypes.exports.number.isRequired,
    e: propTypes.exports.number.isRequired,
    f: propTypes.exports.number.isRequired,
    viewerWidth: propTypes.exports.number.isRequired,
    viewerHeight: propTypes.exports.number.isRequired,
    SVGMinX: propTypes.exports.number.isRequired,
    SVGMinY: propTypes.exports.number.isRequired,
    SVGWidth: propTypes.exports.number.isRequired,
    SVGHeight: propTypes.exports.number.isRequired,
    startX: propTypes.exports.number,
    startY: propTypes.exports.number,
    endX: propTypes.exports.number,
    endY: propTypes.exports.number,
    miniatureOpen: propTypes.exports.bool.isRequired
  })]).isRequired,
  /**
  * handler something changed
  */
  onChangeValue: propTypes.exports.func.isRequired,
  /**
  * current active tool (TOOL_NONE, TOOL_PAN, TOOL_ZOOM_IN, TOOL_ZOOM_OUT)
  */
  tool: propTypes.exports.oneOf([TOOL_AUTO, TOOL_NONE, TOOL_PAN, TOOL_ZOOM_IN, TOOL_ZOOM_OUT]).isRequired,
  /**
  * handler tool changed
  */
  onChangeTool: propTypes.exports.func.isRequired,
  /**************************************************************************/
  /* Customize style                                                        */
  /**************************************************************************/

  /**
  * background of the viewer
  */
  background: propTypes.exports.string,
  /**
  * background of the svg
  */
  SVGBackground: propTypes.exports.string,
  /**
  * style of the svg
  */
  SVGStyle: propTypes.exports.object,
  /**
  * CSS style of the Viewer
  */
  style: propTypes.exports.object,
  /**
  * className of the Viewer
  */
  className: propTypes.exports.string,
  /**************************************************************************/
  /* Detect events                                                          */
  /**************************************************************************/

  /**
  * perform zoom operation on mouse scroll
  */
  detectWheel: propTypes.exports.bool,
  /**
  * perform PAN if the mouse is on viewer border
  */
  detectAutoPan: propTypes.exports.bool,
  /**
  * perform zoom operation on pinch gesture
  */
  detectPinchGesture: propTypes.exports.bool,
  /**
  * handler zoom level changed
  */
  onZoom: propTypes.exports.func,
  /**
  * handler pan action performed
  */
  onPan: propTypes.exports.func,
  /**
  * handler click
  */
  onClick: propTypes.exports.func,
  /**
  * handler double click
  */
  onDoubleClick: propTypes.exports.func,
  /**
  * handler mouseup
  */
  onMouseUp: propTypes.exports.func,
  /**
  * handler mousemove
  */
  onMouseMove: propTypes.exports.func,
  /**
  * handler mousedown
  */
  onMouseDown: propTypes.exports.func,
  /**************************************************************************/
  /* Some advanced configurations                                           */
  /**************************************************************************/

  /**
  * if disabled the user can move the image outside the viewer
  */
  preventPanOutside: propTypes.exports.bool,
  /**
  * how much scale in or out
  */
  scaleFactor: propTypes.exports.number,
  /**
  * how much scale in or out on mouse wheel (requires detectWheel enabled)
  */
  scaleFactorOnWheel: propTypes.exports.number,
  /**
  * maximum amount of scale a user can zoom in to
  */
  scaleFactorMax: propTypes.exports.number,
  /**
  * minimum amount of a scale a user can zoom out of
  */
  scaleFactorMin: propTypes.exports.number,
  /**
  * modifier keys //https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/getModifierState
  */
  modifierKeys: propTypes.exports.array,
  /**
  * Turn off zoom on double click
  */
  disableDoubleClickZoomWithToolAuto: propTypes.exports.bool,
  /**************************************************************************/
  /* Miniature configurations                                                 */
  /**************************************************************************/

  /**
  * override miniature component
  */
  customMiniature: propTypes.exports.oneOfType([propTypes.exports.element, propTypes.exports.func]),
  /**
  * miniature props
  */
  miniatureProps: propTypes.exports.shape({
    position: propTypes.exports.oneOf([POSITION_NONE, POSITION_RIGHT, POSITION_LEFT]),
    background: propTypes.exports.string,
    width: propTypes.exports.number,
    height: propTypes.exports.number
  }),
  /**************************************************************************/
  /* Toolbar configurations                                                 */
  /**************************************************************************/

  /**
  * override toolbar component
  */
  customToolbar: propTypes.exports.oneOfType([propTypes.exports.element, propTypes.exports.func]),
  /**
  * toolbar props
  */
  toolbarProps: propTypes.exports.shape({
    position: propTypes.exports.oneOf([POSITION_NONE, POSITION_TOP, POSITION_RIGHT, POSITION_BOTTOM, POSITION_LEFT]),
    SVGAlignX: propTypes.exports.oneOf([ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT]),
    SVGAlignY: propTypes.exports.oneOf([ALIGN_CENTER, ALIGN_TOP, ALIGN_BOTTOM]),
    activeToolColor: propTypes.exports.string
  }),
  /**************************************************************************/
  /* Children Check                                                         */
  /**************************************************************************/
  /**
  * accept only one node SVG
  */
  children: function children(props, propName, componentName) {
    // Only accept a single child, of the appropriate type
    //credits: http://www.mattzabriskie.com/blog/react-validating-children
    var prop = props[propName];
    var types = ['svg'];
    if (React__default.Children.count(prop) !== 1 || types.indexOf(prop.type) === -1) {
      return new Error('`' + componentName + '` ' + 'should have a single child of the following types: ' + ' `' + types.join('`, `') + '`.');
    }
    if ((!prop.props.hasOwnProperty('width') || !prop.props.hasOwnProperty('height')) && !prop.props.hasOwnProperty('viewBox')) {
      return new Error('SVG should have props `width` and `height` or `viewBox`');
    }
  }
};
ReactSVGPanZoom.defaultProps = {
  style: {},
  background: "#616264",
  SVGBackground: "#fff",
  SVGStyle: {},
  detectWheel: true,
  detectAutoPan: true,
  detectPinchGesture: true,
  modifierKeys: ["Alt", "Shift", "Control"],
  preventPanOutside: true,
  scaleFactor: 1.1,
  scaleFactorOnWheel: 1.06,
  disableZoomWithToolAuto: false,
  onZoom: null,
  onPan: null,
  toolbarProps: {},
  miniatureProps: {}
};

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
var _excluded = ["width", "height", "onChangeTool", "onChangeValue"];
function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }
function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var UncontrolledReactSVGPanZoom = /*#__PURE__*/function (_React$Component) {
  _inherits(UncontrolledReactSVGPanZoom, _React$Component);
  var _super = _createSuper(UncontrolledReactSVGPanZoom);
  function UncontrolledReactSVGPanZoom(props) {
    var _this;
    _classCallCheck(this, UncontrolledReactSVGPanZoom);
    _this = _super.call(this, props);
    _this.state = {
      value: props.defaultValue || {},
      tool: props.defaultTool || TOOL_NONE
    };
    _this.Viewer = null;
    _this.changeTool = _this.changeTool.bind(_assertThisInitialized(_this));
    _this.changeValue = _this.changeValue.bind(_assertThisInitialized(_this));
    return _this;
  }
  _createClass(UncontrolledReactSVGPanZoom, [{
    key: "changeTool",
    value: function changeTool(tool) {
      this.setState({
        tool: tool
      });
    }
  }, {
    key: "changeValue",
    value: function changeValue(value) {
      this.setState({
        value: value
      });
    }
  }, {
    key: "pan",
    value: function pan(SVGDeltaX, SVGDeltaY) {
      this.Viewer.pan(SVGDeltaX, SVGDeltaY);
    }
  }, {
    key: "zoom",
    value: function zoom(SVGPointX, SVGPointY, scaleFactor) {
      this.Viewer.zoom(SVGPointX, SVGPointY, scaleFactor);
    }
  }, {
    key: "fitSelection",
    value: function fitSelection(selectionSVGPointX, selectionSVGPointY, selectionWidth, selectionHeight) {
      this.Viewer.fitSelection(selectionSVGPointX, selectionSVGPointY, selectionWidth, selectionHeight);
    }
  }, {
    key: "fitToViewer",
    value: function fitToViewer(SVGAlignX, SVGAlignY) {
      this.Viewer.fitToViewer(SVGAlignX, SVGAlignY);
    }
  }, {
    key: "zoomOnViewerCenter",
    value: function zoomOnViewerCenter(scaleFactor) {
      this.Viewer.zoomOnViewerCenter(scaleFactor);
    }
  }, {
    key: "setPointOnViewerCenter",
    value: function setPointOnViewerCenter(SVGPointX, SVGPointY, zoomLevel) {
      this.Viewer.setPointOnViewerCenter(SVGPointX, SVGPointY, zoomLevel);
    }
  }, {
    key: "reset",
    value: function reset() {
      this.Viewer.reset();
    }
  }, {
    key: "openMiniature",
    value: function openMiniature() {
      this.Viewer.openMiniature();
    }
  }, {
    key: "closeMiniature",
    value: function closeMiniature() {
      this.Viewer.closeMiniature();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;
      var _this$props = this.props,
        width = _this$props.width,
        height = _this$props.height;
        _this$props.onChangeTool;
        _this$props.onChangeValue;
        var props = _objectWithoutProperties(_this$props, _excluded);
      var _this$state = this.state,
        tool = _this$state.tool,
        value = _this$state.value;
      return /*#__PURE__*/React__default.createElement(ReactSVGPanZoom, _extends({
        width: width,
        height: height,
        tool: tool,
        onChangeTool: this.changeTool,
        value: value,
        onChangeValue: this.changeValue,
        ref: function ref(Viewer) {
          return _this2.Viewer = Viewer;
        }
      }, props));
    }
  }]);
  return UncontrolledReactSVGPanZoom;
}(React__default.Component);
UncontrolledReactSVGPanZoom.propTypes = {
  width: propTypes.exports.number.isRequired,
  height: propTypes.exports.number.isRequired,
  defaultValue: propTypes.exports.object,
  defaultTool: propTypes.exports.string
};

//
// look for start nodes. Usually this will be the elements of "startNodes" but if there aren't any,
// we look for nodes with no incoming edges. Failing that, we just pick a node.
function findStartNodes(g) {
    if (g.startNodes.length > 0) {
        return g.startNodes;
    }
    // look for node with no predecessor - we gotta look at all nodes!
    // If we have a lotta nodes then a better method should be used, but most graphs
    // we visualize are pretty small (<100 nodes) so it's not too bad.
    const hasPredecessor = function (vix) {
        return g.vertices.some(v => v.outgoing.some(e => e.dest === vix));
    };
    const nopreds = g.vertices.filter(v => !hasPredecessor(v.index));
    if (nopreds.length > 0) {
        return nopreds.map(v => v.index);
    }
    // no start nodes and no nodes with 0 predecessors. Just pick a few nodes as a fallback.
    const fallbackCount = g.vertices.length / 10;
    return g.vertices.slice(0, fallbackCount).map(v => v.index);
}
function findSuccessors(curNodes, remainingNodes, g) {
    let accumulator = [];
    curNodes.forEach(n => {
        let vData = g.vertices.find(x => x.index === n);
        if (vData) {
            vData.outgoing.forEach(e => {
                // Before adding a successor we first check that it isn't
                // already recorded in the accumulator to avoid duplicates
                if (!accumulator.some(i => e.dest === i) && remainingNodes.some(i => e.dest === i)) {
                    accumulator.push(e.dest);
                }
            });
        }
    });
    return accumulator;
}
// Given graph data, sort vertices (by index) into groups. Each group is made up of successor nodes of the previous
// group. This is used to determine vertex layout.
function groupLevels(g) {
    // we track which nodes still need to be put in groups
    let remainingNodes = g.vertices.map(v => v.index);
    // as we generate groups of nodes we need to remove them from the array that records remaining nodes
    const contains = function (l, n) { return l.some(x => x === n); };
    const filterOut = function (l, filterOut) { return l.filter(x => !contains(filterOut, x)); };
    // get the initial group of nodes
    const level0 = findStartNodes(g);
    remainingNodes = filterOut(remainingNodes, level0);
    let levels = [level0];
    // go until no nodes left to process
    while (remainingNodes.length > 0) {
        const succNodes = findSuccessors(levels[levels.length - 1], remainingNodes, g);
        // if there are no successor nodes found then whatever is left cannot be reached,
        // so just dump remaining nodes onto a level and quit
        if (succNodes.length == 0) {
            levels.push(remainingNodes);
            break;
        }
        // otherwise process these nodes
        levels.push(succNodes);
        remainingNodes = filterOut(remainingNodes, succNodes);
    }
    return levels;
}
function placeVertices(g, c) {
    const groups = groupLevels(g);
    const markInfo = g.markedNodes[c.markedSetIndex];
    var markedNodes = [];
    if (typeof markInfo !== 'undefined') {
        markedNodes = markInfo.nodes;
    }
    const viz = groups.flatMap((group, level) => {
        return group.map((n, ix) => {
            return {
                index: n,
                highlighted: markedNodes.find(x => n === x) !== undefined,
                position: new Vector2([10 + ix * c.nodeSpacing, 10 + level * c.nodeSpacing])
            };
        });
    });
    return viz;
}
function vertexBounds(vs, cfg) {
    if (vs.length === 0) {
        return { minX: 0, maxX: 0, minY: 0, maxY: 0 };
    }
    const pos0 = vs[0].position;
    const sz = cfg.nodeSize + 4;
    let accumulator = { minX: pos0.x, maxX: pos0.y, minY: pos0.x, maxY: pos0.y };
    vs.forEach(v => {
        const x = v.position.x;
        const y = v.position.y;
        accumulator.minX = Math.min(x - sz, accumulator.minX);
        accumulator.maxX = Math.max(x + sz, accumulator.maxX);
        accumulator.minY = Math.min(y - sz, accumulator.minY);
        accumulator.maxY = Math.max(y + sz, accumulator.maxY);
    });
    return accumulator;
}
function genEdgePath(start, end, label, cfg) {
    const rotateVec2 = (v, r) => { return new Vector2([v.x * Math.cos(r) + v.y * Math.sin(r), -v.x * Math.sin(r) + v.y * Math.cos(r)]); };
    const edgeVector = end.copy().subtract(start);
    var dirForward = edgeVector.copy().normalize();
    var dirBack = dirForward.copy().negate();
    // instead of drawing a path straight between the center of two nodes, we shift it
    // so that the edge curves slightly clockwise. Curve
    let edgeDeflect = 0.3; // in radians
    // for a self-loop we make the deflections larger and opposite so it generates a loop on the node
    if (edgeVector.length() < cfg.nodeSize) {
        dirForward = new Vector2([1, 0]);
        dirBack = new Vector2([1, 0]);
        edgeDeflect = -0.5;
    }
    // We need the paths to start/stop on node edges, but these coords are from
    // node center. So we need to move endpoints closer together by the nodeSize.
    const d1 = rotateVec2(dirForward, edgeDeflect).scale(cfg.nodeSize);
    const d2 = rotateVec2(dirBack, -edgeDeflect).scale(cfg.nodeSize);
    const p1 = start.copy().add(d1);
    const p2 = end.copy().add(d2);
    // generate a bezier slightly curved clockwise or widdershins
    let c1 = p1.copy().add(d1);
    let c2 = p2.copy().add(d2);
    // arrow hooks are N radians off of the main arrow line
    const hookD1 = rotateVec2(d2, 0.4).normalize().scale(4);
    const hookD2 = rotateVec2(d2, -0.4).normalize().scale(4);
    const h1 = p2.copy().add(hookD1);
    const h2 = p2.copy().add(hookD2);
    // place label about 1/10 of the way to the destination
    const labelPos = p1.copy().add(edgeVector.copy().scale(0.1));
    // each path gets a unique ID so we can align text to the path
    const pathId = "path_" + cfg.nextPathId;
    cfg.nextPathId = cfg.nextPathId + 1;
    const pathString = `M ${p1.x} ${p1.y} C ${c1.x} ${c1.y}, ${c2.x} ${c2.y}, ${p2.x} ${p2.y} L ${h1.x} ${h1.y} M ${p2.x} ${p2.y} L ${h2.x} ${h2.y}`;
    return React__default.createElement("g", null,
        React__default.createElement("path", { id: pathId, d: pathString, stroke: "black", fill: "transparent", strokeWidth: "1" }),
        React__default.createElement("text", null,
            React__default.createElement("textPath", { href: "#" + pathId, style: { fontSize: 8 }, x: labelPos.x, y: labelPos.y, "text-anchor": "left", startOffset: "1%", "baseline-shift": "10%" }, label)));
}
function genEdge(d, e, viz, cfg) {
    const destVertex = viz.find(v => v.index === e.dest);
    if (typeof destVertex == 'undefined') {
        return React__default.createElement("path", null);
    }
    else {
        const edgeElement = genEdgePath(d.position, destVertex.position, e.label, cfg);
        return edgeElement;
    }
}
function drawVertex(d, vs, viz, cfg) {
    const strokeColor = d.highlighted ? "red" : "black";
    const vertexDraw = React__default.createElement("circle", { cx: d.position.x, cy: d.position.y, r: cfg.nodeSize, fill: "none", stroke: strokeColor });
    const vInfo = vs.find(x => x.index === d.index);
    if (typeof vInfo == 'undefined') {
        return vertexDraw;
    }
    else {
        const label = React__default.createElement("text", { style: { fontSize: 8 }, x: d.position.x + 8, y: d.position.y, "text-anchor": "middle" },
            " ",
            vInfo.label,
            " ");
        const edges = vInfo.outgoing.map(e => genEdge(d, e, viz, cfg));
        return React__default.createElement("g", null,
            vertexDraw,
            label,
            ...edges);
    }
}
function vizWidget (props) {
    const g = props || [];
    const gr = groupLevels(g);
    const [markIndex, setMarkIndex] = useState(0);
    const newMarkIndex = (m) => { setMarkIndex(Math.max(0, Math.min(m, g.markedNodes.length - 1))); };
    const prevMark = () => { newMarkIndex(markIndex - 1); };
    const nextMark = () => { newMarkIndex(markIndex + 1); };
    const config = {
        nodeSize: 30,
        nodeSpacing: 110,
        nextPathId: 0,
        markedSetIndex: markIndex
    };
    const viz = placeVertices(g, config);
    const bounds = vertexBounds(viz, config);
    const Viewer = React__default.useRef(null);
    React__default.useEffect(() => {
        if (Viewer.current) {
            Viewer.current.fitToViewer();
        }
    }, []);
    // we try to autosize view area to the infoview pane. Note we need to subtract about 50px
    // from width since Lean infoview adds a buffer/gutter on the left size.
    const [width, height] = useWindowSize({ initialWidth: 400, initialHeight: 400 });
    const viewBoxStr = `${bounds.minX} ${bounds.minY} ${bounds.maxX - bounds.minX} ${bounds.maxY - bounds.minY}`;
    var markLabel = "";
    if (g.markedNodes.length > 0) {
        markLabel = (markIndex + 1) + "/" + (g.markedNodes.length) + " marked: " + g.markedNodes[markIndex].markLabel;
    }
    return React__default.createElement("div", null,
        React__default.createElement("div", { hidden: true },
            "Sequence: ",
            JSON.stringify(gr)),
        React__default.createElement("div", null,
            React__default.createElement("button", { onClick: prevMark }, "<"),
            React__default.createElement("button", { onClick: nextMark }, ">"),
            markLabel),
        React__default.createElement(UncontrolledReactSVGPanZoom, { ref: Viewer, width: width - 50, height: height, background: "#919274", defaultTool: "pan" },
            React__default.createElement("svg", { viewBox: viewBoxStr, xmlns: "http://www.w3.org/2000/svg" }, viz.map(d => drawVertex(d, g.vertices, viz, config)))));
}

export { vizWidget as default };
