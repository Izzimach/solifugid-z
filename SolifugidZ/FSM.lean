import Lean

import SolifugidZ.Basic
import SolifugidZ.APBits
import SolifugidZ.LabeledGraph

open Basic
open LabeledGraph

structure FSM (VI : Type) (VL : Type) (EL : Type) extends (LabeledGraph VI VL EL) where
  (startStates : List VI)
  (endStates : List VI)

namespace FSM

def empty [Inhabited VL] : FSM VI VL EL := FSM.mk LabeledGraph.empty [] []


def mergeFSMs {VI VL EL : Type} [Ord VI] [BEq VI] [BEq EL] (f₁ f₂ : FSM VI VL EL) : FSM (OSum VI VI) VL EL :=
    {
        toLabeledGraph := mergeGraphs f₁.toLabeledGraph f₂.toLabeledGraph,
        startStates := (f₁.startStates.map .Left) ++ (f₂.startStates.map .Right)
        endStates := (f₁.endStates.map .Left) ++ (f₂.endStates.map .Right)
    }


-- Connect end states of one FSM to the start states of another. In fact we do this by
-- adding edges from end states to the successor nodes of start states.
def sequenceConnect [Ord VI] [BEq VI] [BEq EL] (vd : LabeledGraph VI VL EL) (endStates: List VI) (nextStartStates : List VI)
    : LabeledGraph VI VL EL :=
    --
    -- To connect the two FSMs sequentially we have to basically overlap the end states of FSM₁ with the start
    -- states of FSM₂. We do this by adding new edges from each end state of FSM₁ which replicate all outgoing edges of the
    -- start states of FSM₂
    --
    -- find all edges leaving the start states of FSM₂
    let startSuccessorEdges := List.join <| List.map (fun ss => (vd.edgesFor ss).2) nextStartStates

    -- now add all these edges to edge vertices in FSM₁.  We do this by changing the "edgesFor" function:
    -- If the caller is requesting edges for an end state we append the new edges to the returned value
    let eCompare := BEq.mk (fun ⟨v₁,ol₁⟩ ⟨v₂,ol₂⟩ => v₁ == v₂ && ol₁ == ol₂)
    
    let newEdgesFor := fun (v : VI) =>
        let ⟨vl,es⟩ := vd.edgesFor v
        if endStates.contains v
        then ⟨vl, @List.eraseDups (VI × EL) eCompare <| es ++ startSuccessorEdges⟩
        else ⟨vl,es⟩

    LabeledGraph.mk vd.vertexCount vd.vertexFor newEdgesFor
    
-- Combine two FSMs into an new FSM that represents the sequencing of FSMs (for example concatanation of DFAs)
def sequenceFSMs [Ord VI] [BEq VI] [BEq EL] (fsm₁ fsm₂ : FSM VI VL EL) : FSM (OSum VI VI) VL EL :=
    let ⟨vd₁, ss₁, es₁⟩ := fsm₁
    let ⟨vd₂, ss₂, es₂⟩ := fsm₂

    let newVD := mergeGraphs vd₁ vd₂

    -- renumber start/end starts to point to vertices in the merged FSM
    let ss₁ := ss₁.map OSum.Left
    let es₁ := es₁.map OSum.Left
    let ss₂ := ss₂.map OSum.Right

    -- as a special case, if any start state in FSM₂ was also an end state, we need to make ALL
    -- end states of FSM₁ into end states
    let es₂ := if fsm₂.startStates.any fsm₂.endStates.contains
               then es₂.map OSum.Right ++ es₁
               else es₂.map OSum.Right

    let newVD := sequenceConnect newVD es₁ ss₂
    FSM.mk newVD ss₁ es₂

-- combine two FSMs into a new FSM that is the cartesian product of the two FSMs.
def productFSMs [Ord VI] [BEq VI] [Inhabited VL] [HAppend VL VL VL] [BEq EL] (fsm₁ : FSM VI VL EL) (fsm₂ : FSM VI VL EL) : FSM (OProd VI VI) VL EL :=
    let ⟨vd₁, ss₁, es₁⟩ := fsm₁
    let ⟨vd₂, ss₂, es₂⟩ := fsm₂
    let vd₃ := graphProduct vd₁ vd₂
    -- the product start/end states are cartesian products
    let listProduct := fun l₁ l₂ => List.join <| List.map (fun e₁ => List.map (fun e₂ => ⟨e₁,e₂⟩) l₂ ) l₁
    let ss₃ := listProduct ss₁ ss₂
    let es₃ := listProduct es₁ es₂
    FSM.mk vd₃ ss₃ es₃

-- synchronous products of two FSMs using the LabeledGraph syncProduct
def syncProductFSMs [Ord VI] [BEq VI] [Inhabited VL] [HAppend VL VL VL] [BEq EL]
    (fsm₁ : FSM VI VL EL) (fsm₂ : FSM VI VL EL) (isSync : EL → Bool): FSM (OProd VI VI) VL EL :=
    let ⟨vd₁, ss₁, es₁⟩ := fsm₁
    let ⟨vd₂, ss₂, es₂⟩ := fsm₂
    let vd₃ := syncProduct vd₁ vd₂ isSync
    -- the product start/end states are cartesian products
    let ss₃ := listProduct ss₁ ss₂ OProd.mk
    let es₃ := listProduct es₁ es₂ OProd.mk
    FSM.mk vd₃ ss₃ es₃

-- Remap the FSM vertex indices onto a range of 0..(n-1), where n is the number of distinct vertices.
def compactFSM [Ord VI] [BEq VI] [Inhabited VL] (fsm : FSM VI VL EL) : FSM Nat VL EL :=
    let ⟨lg, ss, es⟩ := fsm
    let ⟨lg, remapping⟩ := compactGraphWithMapping lg
    -- for the vertices we drop anything that maps to Option.none. We also
    let ss := ss.filterMap remapping
    let es := es.filterMap remapping
    FSM.mk lg ss es

-- A version of compactFSM that also returns the mapping used, in case you need it for
-- meta-FSM information.
def compactFSMWithMapping [Ord VI] [BEq VI] [Inhabited VL] (fsm : FSM VI VL EL) : (FSM Nat VL EL × (VI → Option Nat)):=
    let ⟨lg, ss, es⟩ := fsm
    let ⟨lg, remapping⟩ := compactGraphWithMapping lg
    -- for the vertices we drop anything that maps to Option.none. We also
    let ss := ss.filterMap remapping
    let es := es.filterMap remapping
    ⟨FSM.mk lg ss es, remapping⟩

-- remove FSM vertices that are not reachable from the start states
def onlyReachableFSM [Ord VI] [BEq VI] [Inhabited VL] (fsm : FSM VI VL EL) : FSM VI VL EL :=
    let reachable := List.eraseDups <| List.join <| fsm.startStates.map (fun v => reachableVertices fsm.toLabeledGraph v)
    let lg' := fromExplicit <| keepOnlyVertices fsm.toLabeledGraph reachable
    -- this should be a no-op since all start states should be reachable...
    let ss := fsm.startStates.filter reachable.contains
    let es := fsm.endStates.filter reachable.contains
    FSM.mk lg' ss es

def renumberVerticesFSM [Ord VI] [BEq VI] [Inhabited VL] (fsm : FSM VI VL EL) : FSM VI Nat EL :=
    let lg := renumberVertices fsm.toLabeledGraph
    FSM.mk lg fsm.startStates fsm.endStates

def mapLabels [Ord VI] [BEq VI] (fsm : FSM VI VL₁ EL) (f : VL₁ → VL₂): FSM VI VL₂ EL :=
    let lg := LabeledGraph.mapLabels fsm.toLabeledGraph f
    FSM.mk lg fsm.startStates fsm.endStates

-- Given an FSM, ensures it has no terminating states.  To do this a single new state called a "trap state"
-- is added with a self loop. Then, for any state that has zero outgoing edges we add an edge leading to this
-- new trap state. You can specify the label for the trap state.
def ensureNonterminal (trapLabel : VL) (trapTransitionLabel : EL) (fsm : FSM VI VL EL) : FSM (OOption VI) VL EL :=
    -- we add a single trap state represented by the vertex index Option.none, while all original
    -- nodes are derived from the original indices wrapped in Option.some
    let newCount := fsm.vertexCount + 1
    let newGetVertex : Fin (fsm.vertexCount + 1) → OOption VI :=
        fun i => if hl : i.val < fsm.vertexCount
                 then OOption.Some (fsm.toLabeledGraph.vertexFor (Fin.mk i hl))
                 else OOption.None  -- trap state index
    let newEdgesFor :=
        fun ov => match ov with
                  -- the trap state only loops to itself
                  | OOption.None => ⟨trapLabel,[⟨ov,trapTransitionLabel⟩]⟩
                  | OOption.Some vi =>
                      let ⟨vl,es⟩ := fsm.toLabeledGraph.edgesFor vi
                      -- if this node has no outgoing edges, route it to the trap state
                      if es.length == 0
                      then ⟨vl,[⟨OOption.None,trapTransitionLabel⟩]⟩
                      -- otherwise return the original edges (wrapped in some since they are not the trap state)
                      else ⟨vl,es.map (fun ⟨v,el⟩ => ⟨OOption.Some v, el⟩)⟩
    FSM.mk (LabeledGraph.mk newCount newGetVertex newEdgesFor)
           (fsm.startStates.map .Some)
           (fsm.endStates.map .Some)


--
-- Render FSM in GraphViz
--


class GraphVizLabel (vl : Type) where
    toGraphVizLabel : vl → String

instance : GraphVizLabel String where toGraphVizLabel := id

instance : GraphVizLabel Nat where toGraphVizLabel := toString

instance [BEq e] [GraphVizLabel e] : GraphVizLabel (List e) where
    toGraphVizLabel := fun vls => String.intercalate "_" <| List.map GraphVizLabel.toGraphVizLabel <| List.eraseDups vls

open GraphVizLabel in
instance [GraphVizLabel a] [GraphVizLabel b] : GraphVizLabel (a × b) where
    toGraphVizLabel := fun ⟨a,b⟩ => "_" ++ toGraphVizLabel a ++ "_" ++ toGraphVizLabel b ++ "_"

open GraphVizLabel in
instance [GraphVizLabel a] : GraphVizLabel (Option a) where
    toGraphVizLabel := fun x =>
        match x with
        | Option.none => ""
        | Option.some l => toGraphVizLabel l

open APBits in
instance {A : Type} {apList : List A} [BEq A] [GraphVizLabel A] : GraphVizLabel (APBits apList) where
    toGraphVizLabel :=
        fun bits =>
            let decoded := List.map GraphVizLabel.toGraphVizLabel (@decodeAPBits A apList _ bits)
            "<<" ++ String.intercalate "," decoded ++ ">>"

instance : GraphVizLabel Unit where
    toGraphVizLabel := λ() => ""

def genVizAux [BEq VI] [ToString VI] [GraphVizLabel VL] [GraphVizLabel EL] (g : FSM VI VL EL) (i : Fin g.vertexCount) : List String :=
    let vi := g.vertexFor i
    let ⟨vl,es⟩ := g.edgesFor vi
    let vertexLabel :=
        if (GraphVizLabel.toGraphVizLabel vl).length = 0
        then []
        else ["label=\"" ++ GraphVizLabel.toGraphVizLabel vl ++ "\""]
    let vertexShape :=
        if g.endStates.contains vi
        then "shape=doublecircle"
        else "shape=circle"
    let vertexVizLine := " " ++ toString vi ++ " [" ++ String.intercalate " " (vertexLabel ++ [vertexShape]) ++ "]"
    let extraVertexVizLines :=
        if g.startStates.contains vi
        then let subnodeName :=  "subnode_" ++ toString vi
             [" subgraph {",
              " " ++ subnodeName ++ " -> " ++ toString vi ++ " [arrowhead=veevee,label=start]",
              " " ++ subnodeName ++ " [shape=none,label=< >]",
              "}"]
        else []
    let vizEdge := fun ⟨v₂,ol⟩ =>
        let l := GraphVizLabel.toGraphVizLabel ol
        let labelString :=
            if l.length == 0
            then ""
            else " [label=\"" ++ GraphVizLabel.toGraphVizLabel l ++ "\"] "
        " " ++ toString vi ++ " -> " ++ toString v₂ ++ labelString
    let edgeLines := List.map vizEdge es
    let allVizLines := vertexVizLine :: (extraVertexVizLines ++ edgeLines)
    if hz : i.val = 0
    then allVizLines
    else 
        let ip := Nat.pred i.val
        let iip : ip = Nat.pred i.val := by simp
        let ipLt : ip < g.vertexCount := by rw [iip]; apply Nat.lt_trans (Nat.pred_lt hz) i.isLt
        -- need this to satify Lean well-founded recursion
        have _showWF : Nat.pred i.val + 1 < i.val + 1 := by apply Nat.add_lt_add_right; apply Nat.pred_lt hz
        allVizLines ++ genVizAux g (Fin.mk ip ipLt) 

        
  
-- generate visualization text, suitable to feed into dot from graphviz
-- edge labels are on the edge, vertex labels are joined with "_"
-- normal nodes are circles, end/accept nodes are squares. Start nodes have a double
def graphVizFSM {VI VL EL : Type} [BEq VI] [ToString VI] [GraphVizLabel VL] [GraphVizLabel EL] (g : FSM VI VL EL) : String :=
    if hz : g.vertexCount = 0
    then ""
    else 
        String.join <|
            ["digraph {", " node [label=< >]"]
            ++
            genVizAux g (Fin.mk (Nat.pred g.vertexCount) (Nat.pred_lt hz))
            ++
            ["}"]

def testFSM (n : Nat) : FSM Nat (List String) (Option String) :=
    let nStr := toString n
    {
        toLabeledGraph :=  (fromExplicit <| compileGraph
            [⟨1,["locked" ++ nStr]⟩]
            [⟨1,2,Option.some <| "unlock" ++ nStr⟩,⟨0,1,Option.some <| "lock" ++ nStr⟩]),
        startStates := [0],
        endStates := [2]
    }


def testFSM1 := testFSM 1
def testFSM2 := testFSM 2

--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| sequenceFSMs testFSM1 testFSM2
--#eval productFSMs testFSM1 testFSM2
--#eval compactFSM testFSM1
--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| ensureNonterminal ["deadlock"] (.some "trap") <| (productFSMs testFSM1 testFSM2)

end FSM