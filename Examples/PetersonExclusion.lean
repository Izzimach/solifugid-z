--
-- Example program(s) implementing Peterson's exclusion algorithm, which
-- uses a Nat to guarantee that two processes get fair access to a critical section.
--

import SolifugidZ.Basic
import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.ProgramGraph

open Basic APBits LabeledGraph ProgramGraph FSM

-- Unlike the CriticalSection example, here we use synchronization via the share state.
-- The state has two booleans, one for each process to request access to the critical section.
-- There is also a (Fin 2) which is set to 0 or 1 to indicate which process should go next if
-- both processes request access.

-- State for Peterson's algorithm. We'll need StateCardinality and Ord for this as well.
structure PSemState where
   (lock₁ : Bool) (lock₂ : Bool) (next : Fin 2)
   deriving Repr

instance : ToString PSemState where
    toString := fun s => toString s.lock₁ ++ "/" ++ toString s.lock₂ ++ "," ++ toString s.next

instance : Inhabited PSemState where
    default := PSemState.mk false false (Fin.mk 0 (by simp))

instance : StateCardinality (PSemState) where
    sSize := 2 * 2 * 2
    genState := fun i =>
        let l₁ := if i.val % 2 == 0 then false else true
        let l₂ := if (i.val/2) % 2 == 0 then false else true
        let n := (i.val / 4) % 2
        let hy : (i.val / 4) % 2 < 2 := Nat.mod_lt (i.val / 4) (by simp)
        PSemState.mk l₁ l₂ ⟨n,hy⟩

instance : BEq PSemState := {
    beq := fun s₁ s₂ => s₁.lock₁ == s₂.lock₁ && s₁.lock₂ == s₂.lock₂ && s₁.next == s₂.next
}

instance : Ord PSemState := {
    compare := fun s₁ s₂ =>
        match compare s₁.lock₁ s₂.lock₁ with
        | Ordering.eq => match compare s₁.lock₂ s₂.lock₂ with
                         | Ordering.eq => compare s₁.next s₂.next
                         | v => v
        | v => v
}

--
-- program graph for peterson's algorithm
--
def petProcess1 : ProgramGraph Nat (ActionLabel String) PSemState :=
    compileSequence (
            DO (
                LABEL "noncrit1" // <[λs => {s with lock₁ := true, next := ⟨1,by simp⟩}]>
            //> LABEL "wait1" // WAIT λs => not s.lock₂ || s.next.val == 0 THEN id ..WAIT
            //> LABEL "crit1" // <[λs => {s with lock₁ := false}]>
            )
            UNTIL λ_=> false ..DO
        )

def petProcess2 : ProgramGraph Nat (ActionLabel String) PSemState :=
    compileSequence (
            DO (
                LABEL "noncrit2" // <[λs => {s with lock₂ := true, next := ⟨0,by simp⟩}]>
            //> LABEL "wait2" // WAIT λs => not s.lock₁ || s.next.val == 1 THEN id ..WAIT
            //> LABEL "crit2" // <[λs => {s with lock₂ := false}]>
            )
            UNTIL λ_=> false ..DO
        )

-- A list of the AP in this system.
def PetersonAPList := ["wait1","wait2","crit1","crit2","noncrit1","noncrit2"]

def petersonSystem : FSM (OProd (OProd Nat Nat) PSemState) (APBits PetersonAPList) String :=
    onlyReachableFSM <| unfoldProgramGraph 
        (ProgramGraph.interleaveProgramGraphs petProcess1 petProcess2 .none)
        (λ_ _ l => listToAPBits l)
        toString
        [default]

--#eval FSM.graphVizFSM <| FSM.compactFSM petersonSystem