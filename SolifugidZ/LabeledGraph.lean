import Lean
import Lean.Data.RBMap
import SolifugidZ.Basic

open Lean

open Basic


-- Generic interface for graphs. This is to support multiple underlaying graph representations...
--  1. Full representations where all vertices/edges are stored as large lists/maps
--  2. "as needed" representation where vertices/edges are computed on-the-fly since the whole
--     graph will not fit in memory

structure VertexInfo (VIndex : Type) (VertexLabel : Type) (EdgeLabel : Type) where
    (vl : VertexLabel) (el : List (VIndex × EdgeLabel))

instance [ToString VI] [ToString VL] [ToString EL] : ToString (VertexInfo VI VL EL) where
    toString x := "VInfo (" ++ toString x.vl  ++ "," ++ toString x.el ++ ")"


structure LabeledGraph (VIndex : Type) (VertexLabel : Type) (EdgeLabel : Type) where
    vertexCount : Nat
    vertexFor : Fin vertexCount → VIndex
    edgesFor : VIndex → VertexInfo VIndex VertexLabel EdgeLabel

namespace LabeledGraph

def empty [Inhabited VL]: LabeledGraph VI VL EL := LabeledGraph.mk 0 Fin.elim0 (fun _ => ⟨default, []⟩)

-- LabeledGraph explicitly stored completely in memory. Fast but with the combinatorial
-- explosion of graphs products this can get quite big, where it cannot fit in memory.
def ExplicitGraph (VIndex : Type) [Ord VIndex] (VertexLabel : Type) (EdgeLabel : Type) :=
    RBMap VIndex (VertexInfo VIndex VertexLabel EdgeLabel) compare


-- when taking the product we need a way to combine VertexLabel values from the two
-- graphs. The standard way to do this is make VertexLabel a list and append the lists
class LabelProduct (l : Type) where
   zeroLabel : l
   combineLabels : l → l → l

instance : LabelProduct (List x) where
  zeroLabel := []
  combineLabels := fun l₁ l₂ => l₁ ++ l₂




section

variable {VI VL EL : Type}

-- find all the vertices pointed to by a given vertex
def successorVertices (g : LabeledGraph VI VL EL) (v : VI) : List VI :=
    let ⟨_,es⟩ := g.edgesFor v
    List.map (fun ⟨vi,_⟩ => vi) es


-- Add a given vertex to the ExplicitGraph if it isn't already in there
def addVertex [Ord VI] [Inhabited VL] (g : ExplicitGraph VI VL EL) (v : VI) :=
    match g.find? v with
    | Option.none => g.insert v ⟨default,[]⟩
    | Option.some _ => g

def addVertexLabel [Ord VI] (g : ExplicitGraph VI VL EL) (v : VI) (l : VL) :=
    match g.find? v with
    | Option.none => g.insert v ⟨l,[]⟩
    | Option.some ⟨_,edges⟩ => g.insert v ⟨l, edges⟩

def addEdge [Ord VI] [Inhabited VL] (g : ExplicitGraph VI VL EL) (v₁ v₂ : VI) (l : EL) :=
    match g.find? v₁ with
    | Option.none => g.insert v₁ ⟨default,[⟨v₂,l⟩]⟩
    | Option.some ⟨labels, edges⟩ => g.insert v₁ ⟨labels, ⟨v₂,l⟩ :: edges⟩

-- Compile an ExplicitGraph from a list of edges and labels.
def compileGraph [Ord VI] [Inhabited VL] 
    (vertexLabels : List (VI × VL))
    (edges : List (VI × VI × EL))
    : ExplicitGraph VI VL EL :=
    -- start with an empty RBMap and fold/insert all the vertex and edge info
    let g := List.foldl (fun m ⟨v,l⟩ => addVertexLabel m v l) RBMap.empty vertexLabels
    let g := List.foldl (fun m ⟨v₁,v₂,_⟩ => addVertex (addVertex m v₂) v₁) g edges
    let g := List.foldl (fun m ⟨v₁,v₂,l⟩ => addEdge m v₁ v₂ l) g edges
    g

-- pull vertex indexed by ix out of the LabeledGraph and inserts it into
-- the ExplicitGraph. Then recurses to the next index down
def reifyAux [Ord VI] [Inhabited VL]
    (g: LabeledGraph VI VL EL) (xg : ExplicitGraph VI VL EL) (ix : Fin (g.vertexCount))
    : ExplicitGraph VI VL EL :=
        let vi := g.vertexFor ix
        let ⟨vl,es⟩ := g.edgesFor vi
        let xg := addVertexLabel xg vi vl
        let xg := List.foldl (fun x ⟨v₂,l⟩ => let x := addVertex x v₂; addEdge x vi v₂ l) xg es
        if h : ix.val = 0
        then xg
        else let s := Nat.pred ix.val
             let hs : s < ix.val := Nat.pred_lt h
             let ix' := Fin.mk s (Nat.lt_trans hs ix.isLt)
             have _ht : Nat.pred ix.val + 1 < ix.val + 1 :=
                 by have hss : s = Nat.pred ix.val := by simp
                    have (hx : Nat.pred ix.val < ix.val) := by rw [←hss]; assumption
                    apply Nat.add_lt_add_right hx 1
             reifyAux g xg ix'
    termination_by reifyAux g xg ix => ix


-- Go from LabeledGraph to ExplicitGraph. High upfront cost to build the ExplicitGraph, but then
-- you can use ExplicitGraph which has everything precomputed. A bad idea if the full graph will not
-- fit in memory!
def toExplicit [Ord VI] [Inhabited VL]
    : LabeledGraph VI VL EL → ExplicitGraph VI VL EL := fun g =>
    if hz : g.vertexCount = 0
    then RBMap.empty
    else
        have p : Nat.pred g.vertexCount < g.vertexCount := Nat.pred_lt hz
        reifyAux g RBMap.empty (Fin.mk (Nat.pred g.vertexCount) p)

-- Convert an ExplicitGraph into a LabeledGraph. This will be a bit slower than using the
-- ExplicitGraph directly but allows for combing with other graphs via product or sequence
def fromExplicit [Ord VI] [Inhabited VL] : ExplicitGraph VI VL EL → LabeledGraph VI VL EL := fun x =>
    let eArray := x.toList.toArray
    let fv := fun (i : Fin eArray.size) =>
        let ⟨vi,_,_⟩ := eArray.get i
        vi
    let fe := fun (vi : VI) =>
        match x.find? vi with
        | Option.none => ⟨default,[]⟩
        | Option.some vinf => vinf
    LabeledGraph.mk eArray.size fv fe

-- Merge two graphs. The first component gets vertex indices mapped to (Left vi) and the second gets
-- vertex indices mapped to (Right vi).
def mergeGraphs [Ord VI] (g₁ g₂ : LabeledGraph VI VL EL) : LabeledGraph (OSum VI VI) VL EL :=
    let mergeCount := g₁.vertexCount + g₂.vertexCount
    let fv := fun (Fin.mk i p) =>
        if hl : i < g₁.vertexCount
        then let vi := g₁.vertexFor (Fin.mk i hl)
             OSum.Left vi
        else let i₂ := i - g₁.vertexCount
             let hi : i₂ = i - g₁.vertexCount := by simp
             let he : mergeCount - g₁.vertexCount <= g₂.vertexCount := by simp; rw [Nat.add_comm]; rw [Nat.add_sub_cancel]; simp
             let hex : i₂ < mergeCount - g₁.vertexCount :=
                 by rw [hi]
                    apply Nat.lt_sub_of_add_lt
                    rw [Nat.add_comm, ←Nat.add_sub_assoc]
                    rw [Nat.add_comm, Nat.add_sub_cancel]
                    exact p
                    rw [←Nat.not_lt_eq]
                    exact hl
             let h₂ : i₂ < g₂.vertexCount := by apply Nat.lt_of_lt_of_le hex he
             let vi := g₂.vertexFor (Fin.mk i₂ h₂)
             OSum.Right vi
    let fe : OSum VI VI → VertexInfo (OSum VI VI) VL EL :=
        fun (vi : OSum VI VI) =>
            let remapEdges : (VI → OSum VI VI) → VertexInfo VI VL EL → VertexInfo (OSum VI VI) VL EL :=
                fun rm vinf => ⟨vinf.1, vinf.2.map (fun ⟨vie,el⟩ => ⟨rm vie, el⟩)⟩
            match vi with
                -- the edge target vertices need to be properly mapped to Left/Right values
                | .Left vi₁ => remapEdges OSum.Left <| g₁.edgesFor vi₁
                | .Right vi₂ => remapEdges OSum.Right <| g₂.edgesFor vi₂
    LabeledGraph.mk mergeCount fv fe

--
-- given two Fin x → a maps, combine then into a product map
--
def finProduct (f₁ : Fin n₁ → a) (f₂ : Fin n₂ → b) (hz₁ : n₁ ≠ 0) (hz₂ : n₂ ≠ 0): Fin (n₁ * n₂) → OProd a b :=
    fun i : Fin (n₁ * n₂)  =>
        let i₁ := i % n₁
        let hi₁ : i₁ < n₁ := Nat.mod_lt i (Nat.zero_lt_of_ne_zero hz₁)
        let i₂ := (i / n₁) % n₂
        -- should use div_lt_iff_lt_mul here from mathlib.  Maybe when it's more stable
        let hi₂ : i₂ < n₂ := Nat.mod_lt (i/n₁) (Nat.zero_lt_of_ne_zero hz₂)
        ⟨f₁ (Fin.mk i₁ hi₁), f₂ (Fin.mk i₂ hi₂)⟩

-- Cartesian product of two graphs. Vertex Index is the product of the two component VI's. Labels
-- on an edge ⟨v₁,v₂⟩ to ⟨z₁,z₂⟩ include labels from v₁→z₁ in the first component and v₂→z₂ in the
-- second component graph.
def graphProduct [Ord VI] [Inhabited VL] [HAppend VL VL VL] : LabeledGraph VI VL EL → LabeledGraph VI VL EL → LabeledGraph (OProd VI VI) VL EL :=
  fun g₁ g₂ =>
      -- if either graph is empty the result is empty
      let c₁ := g₁.vertexCount
      let c₂ := g₂.vertexCount
      if hz₁ : c₁ = 0
      then LabeledGraph.empty
      else if hz₂ : c₂ = 0
      then LabeledGraph.empty
      else
        let c := c₁ * c₂
        let getVertex := finProduct g₁.vertexFor g₂.vertexFor hz₁ hz₂
        -- each set of edges has to have their destination properly swizzled
        let getEdgeInfo := fun ⟨vi₁,vi₂⟩ =>
            let ⟨vl₁,es₁⟩ := g₁.edgesFor vi₁
            let ⟨vl₂,es₂⟩ := g₂.edgesFor vi₂
            let es₁ := es₁.map (fun ⟨vi,el⟩ => ⟨⟨vi,vi₂⟩,el⟩)
            let es₂ := es₂.map (fun ⟨vi,el⟩ => ⟨⟨vi₁,vi⟩,el⟩)
            ⟨vl₁ ++ vl₂, es₁ ++ es₂⟩
        LabeledGraph.mk c getVertex getEdgeInfo

-- "Synchronous" cartesian product. This is similar to the normal product except that some edges (specified
-- by from predicate) must be taken by both components at the same time.  If your EL is an Option you can
-- just use Option.isSome as your predicate.
def syncProduct [Ord VI] [Inhabited VL] [HAppend VL VL VL] [BEq EL] : LabeledGraph VI VL EL → LabeledGraph VI VL EL → (EL → Bool) → LabeledGraph (OProd VI VI) VL EL :=
  fun g₁ g₂ isSyncLabel =>
      -- if either graph is empty the result is empty
      let c₁ := g₁.vertexCount
      let c₂ := g₂.vertexCount
      if hz₁ : c₁ = 0
      then LabeledGraph.empty
      else if hz₂ : c₂ = 0
      then LabeledGraph.empty
      else
        let c := c₁ * c₂
        -- vertices are as they were in the normal product
        let getVertex := finProduct g₁.vertexFor g₂.vertexFor hz₁ hz₂
        -- for edges we separate edges between sync/not sync and process them in a special manner
        let syncCheck : VI × EL → Bool := fun ve => isSyncLabel ve.2
        let getEdgeInfo := fun ⟨vi₁,vi₂⟩ =>
            let ⟨vl₁,e₁⟩ := g₁.edgesFor vi₁
            let ⟨vl₂,e₂⟩ := g₂.edgesFor vi₂
            -- pull out sync/no sync edge elements
            let ⟨el₁, en₁⟩ := e₁.partition syncCheck
            let ⟨el₂, en₂⟩ := e₂.partition syncCheck
            -- "no sync" elements are just swizzled
            let en₁ := List.map (fun ⟨vi,el⟩ => ⟨⟨vi,vi₂⟩,el⟩) <| en₁
            let en₂ := List.map (fun ⟨vi,el⟩ => ⟨⟨vi₁,vi⟩,el⟩) <| en₂
            -- "should sync" elements are kept only if labels in the two lists match
            let matchingEdges :List ((OProd VI VI) × EL) :=
                Basic.listProductFilter el₁ el₂ <|
                    fun ⟨v₁,ex₁⟩ ⟨v₂,ex₂⟩ => if ex₁ == ex₂ then Option.some ⟨⟨v₁,v₂⟩,ex₁⟩ else Option.none
            ⟨vl₁ ++ vl₂, en₁ ++ en₂ ++ matchingEdges⟩
        LabeledGraph.mk c getVertex getEdgeInfo

-- After a series of merges or products your vertex index type will be some mess like (OProd (OSum Nat Nat) (OPRod Nat String))
-- or whatever. You can clean this up by renumbering the vertices using Nat's. Returns the renumbered graph and also
-- a function to map the old vertex index values to Option Nat (since you may pass in the index of a vertex that wasn't
-- in the original graph)
def compactGraphWithMapping [Ord VI] [BEq VI] [Inhabited VL] (g : LabeledGraph VI VL EL) : LabeledGraph Nat VL EL × (VI → Option Nat):=
    let eg : ExplicitGraph VI VL EL := toExplicit g
    -- add a mapping from VI → Nat
    let keyList : Array VI := List.toArray <| RBMap.fold (fun acc k _ => k :: acc) [] eg
    let fv := fun (i : Fin keyList.size) => i.val
    let fe := fun (n : Nat) =>
        match keyList.get? n with
        | Option.none => ⟨default,[]⟩
        | Option.some vi =>
            match eg.find? vi with
            | Option.none => ⟨default,[]⟩
            | Option.some ⟨vl,el⟩ =>
                -- on the edge data we need to remap the VI values Nat's
                let remapVI := (fun ⟨vi,el⟩ =>
                    match Array.indexOf? keyList vi with
                    | Option.none => Option.none
                    | Option.some (Fin.mk n _) => Option.some ⟨n,el⟩)
                ⟨vl,List.filterMap remapVI el⟩
    let newGraph := LabeledGraph.mk keyList.size fv fe
    let remapping := fun vi => Option.map Fin.val <| keyList.indexOf? vi
    ⟨newGraph,remapping⟩

-- if you're compacting a graph but don't need the remapping info you can use this
def compactGraph [Ord VI] [BEq VI] [Inhabited VL] (g : LabeledGraph VI VL EL) : LabeledGraph Nat VL EL :=
    (compactGraphWithMapping g).1

-- find vertices up to n steps away from the specified vertex and add them to acc
def reachableVerticesN [BEq VI] (g : LabeledGraph VI VL EL) (v : VI) (acc : List VI) (n : Nat) : List VI :=
    match n with
    | 0 => acc
    | Nat.succ n' => if acc.contains v
                     then acc
                     else let vs := List.map (fun v => v.1) <| (g.edgesFor v).2
                          let acc := v :: acc
                          List.foldl (fun acc' v' => reachableVerticesN g v' acc' n') acc vs

-- find vertices reachable from the given vertex
def reachableVertices [BEq VI] (g : LabeledGraph VI VL EL) (v : VI) : List VI :=
    -- right now we use vertex count to limit recursion and provide termination proof. There
    -- is probably a better way.
    reachableVerticesN g v [] g.vertexCount


def keepOnlyEdges [Ord VI] [BEq VI] (vs : List VI) (es : List (VI × EL)) : List (VI × EL) :=
    let checkEdge := fun vd =>
        if vs.contains vd.1
        then Option.some vd
        else Option.none
    List.filterMap checkEdge es

def keepOnlyAux [Ord VI] [BEq VI] (g : LabeledGraph VI VL EL) (vs : List VI) (e : ExplicitGraph VI VL EL) (i : Fin g.vertexCount) : ExplicitGraph VI VL EL :=
    let vi := g.vertexFor i
    let e := if vs.contains vi
             then 
                 let ⟨vl,es⟩ := g.edgesFor vi
                 e.insert vi ⟨vl, keepOnlyEdges vs es⟩
             else e
    if hz : i.val = 0
    then e
    else 
        let ip := Nat.pred i.val
        let iip : ip = Nat.pred i.val := by simp
        let ipLt : ip < g.vertexCount := by rw [iip]; apply Nat.lt_trans (Nat.pred_lt hz) i.isLt
        -- need this to satify Lean well-founded recursion
        have _showWF : Nat.pred i.val + 1 < i.val + 1 := by apply Nat.add_lt_add_right; apply Nat.pred_lt hz
        keepOnlyAux g vs e (Fin.mk ip ipLt)
    termination_by keepOnlyAux _ _ _ i => i


-- keep a specified list of vertices. other vertices and their labels are removed. edges referencing vertices not in the
-- "to keep" list are removed.
def keepOnlyVertices [Ord VI] [BEq VI] (g : LabeledGraph VI VL EL) (vs : List VI) : ExplicitGraph VI VL EL :=
    if hz : g.vertexCount = 0
    then RBMap.empty
    else
        keepOnlyAux g vs RBMap.empty (Fin.mk (Nat.pred g.vertexCount) (Nat.pred_lt hz))

-- relabel vertices using Nat. Vertex numbering ordering is not defined, only that each vertex gets
-- a unique Nat value
def renumberVertices [Ord VI] [BEq VI] [Inhabited VL] (g : LabeledGraph VI VL EL) : LabeledGraph VI Nat EL :=
    let eg : ExplicitGraph VI VL EL := toExplicit g
    -- add a mapping from VI → Nat
    let keyList : Array VI := List.toArray <| RBMap.fold (fun acc k _ => k :: acc) [] eg
    let newEdgesFor := fun vi => 
        let oldInfo := g.edgesFor vi
        match keyList.indexOf? vi with
        | Option.none => ⟨0,oldInfo.2⟩
        | Option.some n => ⟨n, oldInfo.2⟩
    LabeledGraph.mk g.vertexCount g.vertexFor newEdgesFor
    
def mapLabels [Ord VI] [BEq VI] (g : LabeledGraph VI VL₁ EL) (f : VL₁ → VL₂) : LabeledGraph VI VL₂ EL :=
    let newEdgesFor := fun vi =>
        let oldInfo := g.edgesFor vi
        { oldInfo with vl := f oldInfo.vl }
    LabeledGraph.mk g.vertexCount g.vertexFor newEdgesFor

-- for quick inline experiments
private def x := fromExplicit <| compileGraph [⟨0,"start"⟩] [⟨0,1,Option.some "argh"⟩,⟨1,0,.none⟩]
private def y := fromExplicit <| compileGraph [⟨1,"end"⟩] [⟨0,1,Option.none⟩,⟨1,2,.some "argh"⟩]

--#eval (toExplicit <| graphProduct x y).fold (fun l k v => l ++ [toString k ++ " -=> " ++ toString v]) []
--#eval (toExplicit <| mergeGraphs x y).fold (fun l k v => l ++ [toString k ++ " -=> " ++ toString v]) []
--#eval (toExplicit <| compactGraph <| syncProduct x y Option.isSome).fold (fun l k v => l ++ [toString k ++ " -=> " ++ toString v]) []


end

end LabeledGraph
