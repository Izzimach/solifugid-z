-- handle fairness for CTL. This requires we find the SCC for subgraphs given certain fairness constraints

import Lean

import SolifugidZ.Basic
import SolifugidZ.LabeledGraph
import SolifugidZ.GraphUtilities
import SolifugidZ.FSM
import SolifugidZ.CTL



namespace CTLFair

open Basic
open Lean
open LabeledGraph FSM CTL

open GraphUtilities

-- Find all nontrivial (i.e., has nonzero edges) strongly connected components of the FSM that are reachable from any of the
-- start states.
def fsmSCC {VI VL EL : Type} [BEq VI] [Ord VI] (g : FSM VI VL EL) : (List (List VI)) :=
    let ⟨allSCCs,_⟩ := StateT.run (g.startStates.mapM (dfsSCCm g.toLabeledGraph)) []
    List.filter (isNonTrivialSCC g.toLabeledGraph) allSCCs.join


-- a fairness constraint. We support Weak/Strong/Unconditional fairness as described in the text:
--  - weak fairness : ◇□a → □◇b
--  - strong fairness : □◇a → □◇b
--  - unconditional fairness : □◇a
inductive CTLFairness (A : Type) where
| Weak : A → A →  CTLFairness A
| Strong : A → A → CTLFairness A
| Unconditional : A → CTLFairness A
    deriving Repr, BEq

-- given a list of fairness constraints a CTL formulae, convert them into sets of vertices on the designated graph.
-- we generate a state monad so this can be composed with other ctl processing to re-use the already-computed CTL sat sets
def compileFairness {VI VL EL A : Type} [BEq A] [BEq VI]
        (ts : FSM VI VL EL) 
        (f : List (CTLFairness (CTLFormula A .State))) 
        (getAPSet : FSM VI VL EL → A → List VI) :
        StateM (CTLSubFormulae A (List VI)) (List (CTLFairness (List VI))) :=
    match f with
    | List.nil => pure []
    | List.cons (.Strong a b) t => do
        let aENF := fromCTLtoENF a
        let bENF := fromCTLtoENF b
        let fa ← genSubFormulae aENF ts getAPSet
        let fb ← genSubFormulae bENF ts getAPSet
        let rest ← compileFairness ts t getAPSet
        pure <| (.Strong fa fb) :: rest
    | List.cons (.Weak a b) t => do
        let aENF := fromCTLtoENF a
        let bENF := fromCTLtoENF b
        let fa ← genSubFormulae aENF ts getAPSet
        let fb ← genSubFormulae bENF ts getAPSet
        let rest ← compileFairness ts t getAPSet
        pure <| (.Weak fa fb) :: rest
    | List.cons (.Unconditional b) t => do
        let bENF := fromCTLtoENF b
        let fb ← genSubFormulae bENF ts getAPSet
        let rest ← compileFairness ts t getAPSet
        pure <| (.Unconditional fb) :: rest

-- does this subgraph SCC satisfy the fairness constraints? returns true if it does
def isSCCFair [BEq VI] [Ord VI] [Inhabited VL] (ts : FSM VI VL EL) (f : List (CTLFairness (List VI))) (scc : List VI) : Bool :=
    match f with
    | List.nil => true -- no more constraints
    | List.cons h t =>
        match h with
        | .Unconditional b => 
            -- for unconditional we need b to appear somewhere in the SCC
            if b.any scc.contains
            -- it's here, continue to the next SCC
            then isSCCFair ts t scc
            else false
        | .Strong a b =>
            -- strong is (□◇a) → (□◇b), or equivalently (¬□◇a) ∨ (□◇b)
            -- to prove this for the SCC we need either:
            --   1) if b exists anywhere in the SCC then □◇b is true for the whole SCC since
            --         we can build a cycle visiting b infinitely often
            --      OR
            --   2) if we remove all vertices for which a is true and find a nontrivial SCC in the remains,
            --         this shows ¬□◇a since there are cycles where no a occurs
            -- based on these criteria we either return false or
            -- continue on to prove the next constraint, possibly with a smaller SCC in the case of (2)
            if b.any scc.contains
            -- SCC contains b so this is true, continue on the to the next constraint
            then isSCCFair ts t scc
            else
                -- find SCCs in the subgraph of vertices with ¬a. If any SCC exist then this constraint 
                -- is satisfied and we continue on to the next constraint with the smaller SCC
                -- first build the subgraph or ¬a
                let notAVertices :=scc.filter (fun v => not (a.contains v))
                let fsmNotA : FSM VI VL EL := {
                    toLabeledGraph := fromExplicit <| ts.keepOnlyVertices notAVertices,
                    -- the start states need to include all vertices in the subgraph since the SCC alg only checks
                    -- vertices reachable from start states
                    startStates := notAVertices,
                    endStates := []
                }
                -- does this ¬a subgraph have any SCC?
                let sccNotA := fsmSCC fsmNotA
                if sccNotA.length == 0
                -- if no SCCs without a exist then this constraint is violated for the whole SCC, so return false
                then false
                else
                    -- if any of the SCCs are true for the rest of the constraints then this constraint is true
                    List.any sccNotA (fun sna => isSCCFair ts t sna)
        | .Weak a b => 
            -- weak fairness is (◇□a) → (□◇b) so the process is similar to that of strong fairness:
            --   1) b is anywhere in the SCC
            --   2) ¬a is anywhere in the SCC
            -- in fact we could transform this into strong fairness (□◇¬a) → (□◇b) or (□◇true) → □◇(¬a ∨ b)
            if (scc.any b.contains) || (scc.any (fun x => not (a.contains x)))
            -- SCC contains b or ¬a so this is true, continue on the to the next constraint
            then isSCCFair ts t scc
            else false
        

-- find all vertices in the set "toConsider" that can reach the set "toReach" through only vertices
-- in the set "toConsider".
def reachableIn [BEq VI] (ts : FSM VI VL EL) (toConsider : List VI) (toReach : List VI) : List VI :=
    -- any vertices in toConsider that transition into toReach are allowed
    let ⟨toAdd, remaining⟩ := toConsider.partition <| fun v =>
        let successors := (ts.edgesFor v).el.map (fun ⟨l,_⟩ => l)
        List.any successors toReach.contains
    -- keep repeating until no more vertices are added from the toConsider set
    if List.length remaining < List.length toConsider
    then reachableIn ts remaining (List.eraseDups <| toAdd ++ toReach)
    else toReach
  termination_by reachableIn _ uA _ => List.length uA

-- given a satifiability set 'a', find '∃□a' of that set under fairness assumptions
def satExistsAlwaysFair {VI VL EL : Type} [BEq VI] [Ord VI] [Inhabited VL]
        (ts : FSM VI VL EL) (f : (List (CTLFairness (List VI)))) (subSat : List VI) : (List VI):=
    -- first generate a subgraph containing only the subSat vertices
    let subG := {
        toLabeledGraph := fromExplicit <| keepOnlyVertices ts.toLabeledGraph subSat,
        startStates := subSat,
        endStates := []
    }
    -- find all SCCs of this subgraph
    let subSCCs := fsmSCC subG
    -- keep only SCCs that satisfy fairness constraints
    let satSCCs := subSCCs.filter (isSCCFair ts f)
    -- find all vertices in the subSat that can reach these SCCs that satify fairness
    -- at this point we just flatten the satSCCs into a set of vertices
    reachableIn ts subSat satSCCs.join


-- processing fair CTL is similar to normal CTL in most cases, with some special processing for ∃◯,∃a⋃b, and ∃□

mutual

-- Process a path formula with fairness. Since we're in ENF all paths are existential.
-- For ∃□a we need special code, the other just restrict their satifiability to the fair set.
def genSubFormulaePathFair {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [Inhabited VL] (p : CTLENF A .Path) (ts : FSM VI VL EL) (f : (List (CTLFairness (List VI)))) (fairSat : List VI) (getAPSet : FSM VI VL EL → A → List VI) : StateM (CTLSubFormulae A (List VI)) (List VI) := do
    match lookupSubformula p (← get) with
    | Option.some v => pure v
    | Option.none => do
        let storeAndReturn : List VI → StateM (CTLSubFormulae A (List VI)) (List VI) :=
            fun v => do
                let sf ← get
                set (addSubformula p v sf)
                pure v
        match p with
        | .Next a => do
            -- for ∃◯a we just call the normal ∃◯a with a filtered by the fair satifiability
            let av ← genSubFormulaeFair a ts f fairSat getAPSet
            let avFair := av.filter fairSat.contains
            storeAndReturn <| satExistsNext ts avFair
        | .Always a => do
            -- for ∃□a we use the special version for fairness
            let av ← genSubFormulaeFair a ts f fairSat getAPSet
            storeAndReturn <| satExistsAlwaysFair ts f av
        | .Until a b => do
            -- for ∃a⋃b we filter both a and by the fairness satisfiability set
            let av ← genSubFormulaeFair a ts f fairSat getAPSet
            let avFair := av.filter fairSat.contains
            let bv ← genSubFormulaeFair b ts f fairSat getAPSet
            let bvFair := bv.filter fairSat.contains
            storeAndReturn <| satExistsUntil ts avFair bvFair

-- evaluate a given subformula with fairness. For state formula the computations are unchanged, but we do have to track
-- the fairness constraints and path them onto the path evaluation
def genSubFormulaeFair {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [Inhabited VL] (c : CTLENF A .State) (ts : FSM VI VL EL) (f : (List (CTLFairness (List VI)))) (fairSat : List VI) (getAPSet : FSM VI VL EL → A → List VI) : StateM (CTLSubFormulae A (List VI)) (List VI) := do
    -- is this subformula alrady evaluated? check the state
    match lookupSubformula c (← get) with
    | Option.some v => pure v
    | Option.none => do
        -- use this instead of pure. Stores the result into state so we don't need to compute it again
        let storeAndReturn : List VI → StateM (CTLSubFormulae A (List VI)) (List VI) :=
            fun v => do
                let sf ← get
                set (addSubformula c v sf)
                pure v
        -- generate a value for this
        match c with
        -- true satisfies all vertices of the graph
        | .True => do
            let v := List.map ts.vertexFor (listRangeFin ts.vertexCount)
            storeAndReturn v
        -- use the provided function to map from AP to a satifiability set
        | .AP a => do
            storeAndReturn <| getAPSet ts a
        | .And a b => do
            let av ← genSubFormulaeFair a ts f fairSat getAPSet
            let bv ← genSubFormulaeFair b ts f fairSat getAPSet
            storeAndReturn <| av.filter bv.contains   -- a ∩ b
        | .Not a => do
            let av ← genSubFormulaeFair a ts f fairSat getAPSet
            -- the complement is all vertices not in av
            let notav := List.filterMap 
                          (fun i => let vi := ts.vertexFor i
                                    if av.contains vi then Option.none else Option.some vi)
                          (listRangeFin ts.vertexCount)
            storeAndReturn notav
        | .Exists p => genSubFormulaePathFair p ts f fairSat getAPSet
end
  termination_by genSubFormulaeFair c _ _ _ _ => sizeOf c
                 genSubFormulaePathFair p _ _ _ _ => sizeOf p

-- find the satifiability set of a given CTLformula with fairness
def satCTLFair {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [Inhabited VL] (c : CTLFormula A .State) (ts : FSM VI VL EL) (f : (List (CTLFairness (CTLFormula A .State)))) (getAPSet : FSM VI VL EL → A → List VI) : List VI :=
    let initialStore : CTLSubFormulae A (List VI) := {stateF := [], pathF := []}
    let cENF := fromCTLtoENF c
    let satSet := Id.run <| StateT.run'
        (do
            let constraints ← compileFairness ts f getAPSet
            let fairSat := satExistsAlwaysFair ts constraints (List.map ts.vertexFor (listRangeFin ts.vertexCount))
            genSubFormulaeFair cENF ts constraints fairSat getAPSet)
        initialStore
    satSet

-- run satCTLFair and also return the computed subformulae and the fairness sat set
def satCTLFairSub {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [Inhabited VL] (c : CTLFormula A .State) (ts : FSM VI VL EL) (f : (List (CTLFairness (CTLFormula A .State)))) (getAPSet : FSM VI VL EL → A → List VI) : (List VI × List VI × CTLSubFormulae A (List VI)):=
    let initialStore : CTLSubFormulae A (List VI) := {stateF := [], pathF := []}
    let cENF := fromCTLtoENF c
    let ⟨⟨x,y⟩, s⟩ := Id.run <| StateT.run
        (do
            let constraints ← compileFairness ts f getAPSet
            let fairSat := satExistsAlwaysFair ts constraints (List.map ts.vertexFor (listRangeFin ts.vertexCount))
            let ctlResult ← genSubFormulaeFair cENF ts constraints fairSat getAPSet
            pure <| Prod.mk ctlResult fairSat)
        initialStore
    ⟨x,y,s⟩

-- check a transitionn system using CTL with a given function to generate satifiability sets from a specific AP  of type A.
def checkCTLAPFair {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [Inhabited VL] (ts : FSM VI VL EL) (c : CTLFormula A .State) (f : (List (CTLFairness (CTLFormula A .State)))) (getAPSet : FSM VI VL EL → A → List VI) : Bool :=
    let satSet := satCTLFair c ts f getAPSet
    -- all of the start states need to be in the satisfiability set
    ts.startStates.all satSet.contains

-- check a transition system expressed using APBits.  If you have your own representation of atomic propositions you can
-- use checkCTLAP
def checkUsingCTLFair {A VI EL : Type} {apList :List A} [BEq A] [BEq VI] [Ord VI] (ts : FSM VI (APBits apList) EL) (c : CTLFormula A .State) (f : (List (CTLFairness (CTLFormula A .State)))): Bool  :=
    checkCTLAPFair ts c f satAPBits


-- generate counter example from state `s` for a CTL formula of the form `a ⋃ b`
def genUntilCounterExampleFairAt {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [ToString A] [Inhabited VL]
        (a b : CTLFormula A .State) 
        (fairnessConstraints : List (CTLFairness (List VI)))
        (fairSat : List VI)
        (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) (s : VI) 
        : StateM (CTLSubFormulae A (List VI)) (CTLResult VI) := do
    -- for Until we generate a reduced graph, only keeping edges from vertices that satisfy (a ∧ ¬b)
    -- and then look for a nontrivial SCC or a terminal node that is (¬a ∧ ¬b)
    let eENF := fromCTLtoENF (.And a (.Not b))
    let satSet ← genSubFormulae eENF ts getAPSet
    let filteredGraph := GraphUtilities.filterEdges ts.toLabeledGraph (fun v₁ _ _ => satSet.contains v₁)
    -- try to find a path to one of the nontrivial fair SCCs of this reduced graph
    let sccs := GraphUtilities.findNontrivialSCCAt filteredGraph s
    -- keep only SCCs that satisfy fairness constraints
    let sccs := sccs.filter (isSCCFair ts fairnessConstraints)
    let pathToSCC : CTLResult VI :=
        sccs.foldl 
            (fun p scc =>
                match p with
                | .Satisfies => match GraphUtilities.findPath filteredGraph s scc with
                            | .none => .Satisfies
                            | .some p => .CounterExamplePathToSCC p scc -- include the SCC in the counterexample path
                | _ => p)
            .Satisfies
    match pathToSCC with
    | .Satisfies => do
        -- couldn't find a path to a SCC
        -- the other option is to find a path to a node that satisfies (¬a ∧ ¬b) in the fair Set
        let tENF := fromCTLtoENF (.And (.Not a) (.Not b))
        let termSet ← genSubFormulae tENF ts getAPSet
        match GraphUtilities.findPathPredicate filteredGraph s (fun v => termSet.contains v && fairSat.contains v) with
        | .some p => pure <| .CounterExamplePath p
        | .none => pure <| .Error ("Error, CTL expression " ++ toString (CTLFormula.Until a b) ++ " failed but no counterexample found")
    | _ => pure pathToSCC

-- finds a path refuting path formula `p` starting at the given state `s` with given fairness constraints.
-- This is a counterexample for the formula `∀p`.
def genCounterExampleFairAt {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [ToString A] [Inhabited VL]
        (p : CTLFormula A .Path)
        (fairnessConstraints : List (CTLFairness (List VI)))
        (fairSat : List VI)
        (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) (s : VI) 
        : StateM (CTLSubFormulae A (List VI)) (CTLResult VI) :=
    match p with
    | .Next e => do
        -- find a successor to s in the fair set which does not satisfy e
        let eENF := fromCTLtoENF e
        let satSet ← genSubFormulaeFair eENF ts fairnessConstraints fairSat getAPSet
        let succs := ts.edgesFor s
        match succs.el.find? (fun ⟨v,_⟩ => fairSat.contains v && not (satSet.contains v)) with
        | .none => pure <| .Error ("Error, CTLfair expression " ++ toString p ++ " failed but no counterexample found")
        | .some ⟨s',_⟩ => pure <| .CounterExamplePath [s,s']
    | .Always e => do
        -- we need to find a path to a node that is `not e` and in the fairSat set
        -- also we only allow edges from e states
        let eENF := fromCTLtoENF e
        let satSet ← genSubFormulaeFair eENF ts fairnessConstraints fairSat getAPSet
        let reducedGraph := GraphUtilities.filterEdges ts.toLabeledGraph (fun v₁ l v₂ => satSet.contains v₁)
        -- the goal state is not in e and in the fair set
        match GraphUtilities.findPathPredicate reducedGraph s (fun v => fairSat.contains v && not (satSet.contains v)) with
        | .none => pure <| .Error ("Error, CTLfair expression " ++ toString p ++ " failed but no counterexample found")
        | .some s' => pure <| .CounterExamplePath s'
    | .Until a b => genUntilCounterExampleFairAt a b fairnessConstraints fairSat ts getAPSet s
    | .Eventually e => genUntilCounterExampleFairAt .True e fairnessConstraints fairSat ts getAPSet s

-- Checks CTL satifiability at a given state s with fairness assumptions.
-- If s doesn't satisfy the formula it attempts to generate a counterexample/witness for the given formula starting at state s. Runs in the same
-- state monad as genSubFormulae.
def checkCTLFairAt {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [ToString A] [Inhabited VL]
        (c : CTLFormula A .State) 
        (fairness : (List (CTLFairness (CTLFormula A .State))))
        (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) (s : VI) 
        : StateM (CTLSubFormulae A (List VI)) (CTLResult VI) :=
  do
    let cENF := fromCTLtoENF c
    let constraints ← compileFairness ts fairness getAPSet
    let fairSat := satExistsAlwaysFair ts constraints (List.map ts.vertexFor (listRangeFin ts.vertexCount))
    let satSet ← genSubFormulaeFair cENF ts constraints fairSat getAPSet
    if satSet.contains s
    then pure .Satisfies
    else do
        -- look for a counterexample for ∀
        -- for ∃ we just say there isn't a witness
        -- anything else just returns the initial state
        match c with
        | .ForallPaths p => genCounterExampleFairAt p constraints fairSat ts getAPSet s
        | .ExistsPath p => pure .NoWitness
        | _ => pure <| .CounterExamplePath [s]


-- check a transition system with CTL under fairness, if it fails then a counterexample is returned (if the expression is a ∀).
def ctlCounterExampleFair {A VI VL EL : Type} [BEq A] [ToString A] [BEq VI] [Ord VI] [Inhabited VL]
        (c : CTLFormula A .State)
        (fairness : (List (CTLFairness (CTLFormula A .State))))
        (ts : FSM VI VL EL) 
        (getAPSet : FSM VI VL EL → A → List VI)
        : CTLResult VI :=
    --let ⟨satSet, subFormulae⟩ := satCTLSub c ts getAPSet
    let initialStore : CTLSubFormulae A (List VI) := {stateF := [], pathF := []}
    --let cENF := fromCTLtoENF c
    let checkStartState :=
        fun res s =>
            -- if there was already an error/counterexample found, just return that one
            match res with
            | .Satisfies => do
                checkCTLFairAt c fairness ts getAPSet s
            | _ => pure res
    -- check all start states to see if any fail and (if a ∀) generate a counterexample
    Id.run <| StateT.run' (ts.startStates.foldlM checkStartState .Satisfies)  initialStore

end CTLFair
